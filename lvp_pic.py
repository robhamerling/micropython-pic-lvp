"""
lvp_pic.py

Micropython class for programming Microchip PICs in LVP mode

Version: 5.0

Author: Rob Hamerling; Copyright (c) 2019..2024. All rights reserved.
Micropython base class for programming Microchip PICs with
ESP32, Pyboard, Raspberry Pico or any other MicroPython board.

License: MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


Description:

Collection of classes for programming PICs.
Hierarchy of the class structure can be found in README.md

Because the ESP32, PyBoard, RP2 PICO, etc. are running at a 3.3V level,
also the PIC to be programmed should be powered with 3.3V.
Using  higher voltage may damage the programmer!

See for functionality and a summary of changes: README.md.

"""

import os
from time import sleep_us
from machine import Pin, SPI, SoftSPI
from micropython import const

picpgmcompactinfo = 'picpgmcompactinfo.py'      # file with PIC attributes

# =============================================
class PIC(object):
    """ C L A S S  -  base for all 8 bits PIC families """
    # class variables/constants
    lvp_pic_version = '5.1'                     # library version
    lvp_magic = const(0x4D434850)               # magic pattern to enter LVP mode ('MCHP')
    # format strings for common error messages:
    fmt_verify   = '==> ERR Write verify error: addr=0x{:06X}, goal=0x{:04X}, result=0x{:04X}'
    fmt_address  = '==> ERR Memory address 0x{:06X} is outside any implemented range!'
    fmt_erase    = '==> ERR Bulk erase failed: word at 0x{:06X} : 0x{:04X}'
    rmt_rowalign = '==> WNG 0x{:06X} not on row boundary 0x{:06X}, forced!'

    def __init__(self, mclr, led=None):
        self.spi = None                         # default: no SPI
        self.mclr = mclr if isinstance(mclr, Pin) else Pin(mclr)  # Pin instance
        self.mclr.init(Pin.OUT)
        self.mclr.on()                          # start high
        self.led = led
        if self.led is not None:                # led is optional
            self.led = led if isinstance(led, Pin) else Pin(led)  # Pin instance
            self.led.init(Pin.OUT)
            self.led.off()                      # LED off
        print(f'LVP library version {self.lvp_pic_version}')
        print(f'Selected class: {self.__class__.__qualname__}')

        # Specification of PIC properties
        # Variables may be modified by an inheriting class and
        # even more after the actual target has been identified.
        # devid and revid (when present) are always 1 word (no end address needed)

        if self.__class__.__qualname__.startswith('PIC18'):     # selected PIC class
            # Memory region boundaries in byte addresses the <advanced> midrange PICs (18Fs)
            self.pgm_start    = 0x000000
            self.pgm_end      = 0x020000        # 128K Bytes
            self.id_start     = 0x200000
            self.id_end       = 0x200010
            self.cfg_start    = 0x300000
            self.cfg_end      = 0x300008
            self.eeprom_start = 0x310000
            self.eeprom_end   = 0x310400        # 1KB
            self.revid_start  = 0x3FFFFC
            self.devid_start  = 0x3FFFFE

            self.memaddr_width = 1              # byte-addressing for 18F
            self.empty = const(0xFFFF)          # 'empty' word
            self.filler = b'\xFF\xFF'           # 'empty' word in byte array
            self.pgm_rowsize = 1                # default no pgmg per row
            self.rowboundary = 256 * 256 * 256 - (self.pgm_rowsize * 2)  # mask for row boundary
            self.cfg_wordsize = 2               # config pgmg per word (not byte)

        else:
            # Memory region boundaries in word addresses for midrange PICs
            self.pgm_start    = 0x0000
            self.pgm_end      = 0x8000          # 32K words
            self.id_start     = 0x8000
            self.id_end       = 0x8004
            self.revid_start  = 0x8005
            self.devid_start  = 0x8006
            self.cfg_start    = 0x8007
            self.cfg_end      = 0x800C
            self.eeprom_start = 0xF000
            self.eeprom_end   = 0xF000          # No EEPROM!

            self.memaddr_width = 2              # word-addressing midrange PICs
            self.empty = const(0x3FFF)          # 'empty' word
            self.filler = b'\xFF\x3F'           # 'empty' word in byte array
            self.pgm_rowsize = 1                # default no pgmg per row
            self.rowboundary = 256 * 256 - self.pgm_rowsize  # mask for row boundary
            self.cfg_wordsize = 2               # config pgmg per word (not byte)

        self.lvpbit_mask = 0x0000               # placeholder  (no LVP bit)
        self.lvpbit_offset = 0
        self.revmask = 0xFFE0                   # isolate revision bits of devid (when applicable)
        self.bulk_erase_by_mask = False			# default: address specification
        self.cfg_impl = 'FFFF'                  # pattern of implemented config bits

        # Programming timing values (defaults and placeholders)
        # Values may be updated by inheriting classes (after target detection).
        self.Tdly   = 1      # delay between command and data (all PICs)
        self.Tenth  = 0      # programming mode entry hold time
        self.Tents  = 1      # programming mode entry setup time (all PICs)
        self.Terab  = 0      # bulk erase cycle time
        self.Thzd   = 1      # icspdat to output (all PICs)
        self.Tlzd   = 1      # iscpdat to input (all PICs)
        self.Tpint  = 0      # internally timed programmming delay
        self.Tpdfm  = 0      # EEPROM/config memory programming time

    def __print_datalist(self, title, addr, data):
        # Only used for debugging: show address and list of words in hex
        print(f'{title}  0x{addr:06X}\n      ', end='')
        for i in range(0, len(data), 8):        # max 8 words per line
            print(', '.join([f'{data[i + j]:04X}' for j in range(min(8, len(data) - i))]), '\n      ', end='')
        print()

    def __lvp_start(self):
        # initiate LVP mode (magic pattern to be sent by caller)
        self.mclr.on()
        sleep_us(self.Tents)                    # MCLR must be high some time
        self.mclr.off()
        sleep_us(self.Tenth)                    # MCLR low before first activity

    def __check_and_set_lvp_bit(self, cfgaddr, cfgdata):
        # Give warning if config contains LVP-bit 0 (LVP DISABLED)
        # Hardware will prevent it being set to 0, so force 1 to prevent verify error.
        # <cfgaddr> is the address of the current word being written
        # <cfgdata> is the data being written
        # This check may be done for every address, but has only effect
        # when the address matches (self.cfg_start + self.lvpbit_offset)
        if not self.lvpbit_mask == 0x0000:                      # lvp bit mask present
            if cfgaddr == self.cfg_start + self.lvpbit_offset:  # config word with lvp bit
                if not (cfgdata & self.lvpbit_mask) == self.lvpbit_mask:   # check lvp disabled
                    print("==> WNG:  LVP cannot be disabled in LVP-mode!")
                    cfgdata |= self.lvpbit_mask                 # set lvp bit enabled
        return cfgdata                                          # possibly modified data

    def __find_target(self):
        # find target PIC type in picpgmcompactinfo
        # search relies on the structure of the file with properties!
        # Note: devids are not really unique, some
        #       devid values are used for different PIC types.
        tgtlist = []                            # list of possible PICs
        with open(picpgmcompactinfo, 'r') as fp:
            while (ln := fp.readline()) != '':
                if ln.startswith('class PIC'):
                    tgt = ln[len('class PIC') : ln.rfind('(')]
                elif ln.startswith('    devid ='):
                    dev = int(ln[ln.find('=') + 2 : ], 16)
                    # print(f'{dev=:04X} {tgt=}')
                    if dev == self.devid:       # matching devid
                        tgtlist.append(tgt)     # add to list
                        # print(f'{tgt=} {dev=:04X}')
        tgt = None                              # real target not (yet) found
        if len(tgtlist) > 0:                    # one or more in list
            if len(tgtlist) > 1:
                print(f'WNG: multiple targets with devid {self.devid:04X}: {tgtlist}')
            tgt = tgtlist[0]                    # default: first or only
            for t in tgtlist:                   # search matching type
                if t.startswith('18') and self.__class__.__qualname__.startswith('PIC18'):
                    tgt = t                     # probably the right target
                    break
        print(f'Target presumably: {tgt}')
        return tgt

    def __adjust_attributes(self):
        # collect PIC specific properties
        # search relies on the structure of the file with properties!
        # First search the pgmspec of this PIC
        pgmspec = None
        with open(picpgmcompactinfo, 'r') as fp:
            while (ln := fp.readline()) != '':
                if ln.startswith(f'class PIC{self.target}'):
                    pgmspec = ln[ln.find('(') + 1 : ln.find(')')]   # class of pgmspec
                    break
            else:
                print(f'Did not find pgmspec of {self.target} in {picpgmcompactinfo}')
                return
        # Collect PIC specific programming attributes from picpgmcompactinfo
        print(f'Supplying {self.target} specific attributes')
        with open(picpgmcompactinfo, 'r') as fp:
            # search the pgmspec class
            while (ln := fp.readline()) != '':
                if ln.startswith('class ' + pgmspec):
                   break                    # found pgmspec
            else:
                print('pgmspec', pgmspec, 'not found')
                return
            setattr(self, 'pgmspec', int(pgmspec[2:]))  # remember pgmspec (number)
            # obtain the attributes of the pgmspec
            while (ln := fp.readline().strip()) != '':
                ll = ln.split(' ')
                if ll[0] == 'cfg_impl':
                    setattr(self, ll[0], ll[2].strip('"'))  # string
                else:
                    setattr(self, ll[0], eval(ll[2]))    # integer
            # search the specific PIC class
            while (ln := fp.readline()) != '':
                if ln.startswith('class PIC' + self.target):
                    break                               # found PIC
            # obtain PIC specific attributes
            while (ln := fp.readline().strip()) != '':
                ll = ln.split(' ')
                if ll[0] == 'cfg_impl':
                    setattr(self, ll[0], ll[2].strip('"'))  # string
                else:
                    setattr(self, ll[0], eval(ll[2]))    # integer
        if self.__class__.__qualname__.startswith('PIC18'):
            self.rowboundary = 256 * 256 * 256 - (self.pgm_rowsize * 2)  # (18F: byte addressing)
        else:
            self.rowboundary = 256 * 256 - self.pgm_rowsize  # (16F: word addressing)

    def __determine_target(self):
        # determine actual target PIC type from devid
        # and adjust target specific attributes for this specific PIC
        # Note: some PICs have the same devid, wrong type might be selected!
        if self.revid in (0x3FFF, 0xFFFF, 0x0000, None):    # probably no separate revid present
            self.revid = self.devid & ~self.revmask         # isolate revid
            self.devid = self.devid & self.revmask          # remove revision code
        self.target = self.__find_target()                  # scan picpgmcompactinfo
        if self.target:
            # print(f'{self.target} (device-ID 0x{self.devid:04X}, revision 0x{self.revid:04X})')
            # Replace generic attribute values with PIC specific values
            self.__adjust_attributes()
        else:
            print(f'device-ID 0x{self.devid:04X}')
            if self.devid == 0x0000:
                print("==> ERR: Target probably not connected, not powered,\n",
                      "        has LVP disabled or is an unsupported type")
            elif self.devid in (0x3FFF, 0xFFFF):
                print("==> ERR: Target probably not properly connected")
            else:
                print(f'==> ERR: Target is probably not of family {self.__class__.__qualname__}')
        return self.target

    # generator function (cache of hexfile)
    def __hexfile_cache(self, hexfile) -> bool:
        # Convert intelhex file into bytearrays for PIC programming
        # - Length of data and checksum-byte in data records are ignored.
        # - Returned format for all memory regions: (memory-address, bytearray)
        # - For program memory the data is collected from probably several
        #   lines of the Intel hex file into a bytearray to fill a row
        #   of program memory.
        #   Hexfile may have non-contiguous program memory,
        #   this is accounted for by inserting 'empty' words.
        # - For EEPROM, user-id and config: no caching
        # - All addresses and address-calculations are here in bytes.
        #   Calling functions may need to convert the returned address to
        #   word-address when appropriate for the type of PIC to be programmed.

        def hexchk(ln) -> bool:
            # check for valid Intel Hex record (start, length, checksum)
            if not ln[0] == ':':
                print(ln, '\n   Invalid Intel Hex record (should start with ":")')
                return False
            # convert pairs of hex chars to integers
            bytelist = [int(ln[i:i+2],16) for i in range(1, len(ln), 2)]    # excl 1st char
            if not bytelist[0] == (len(bytelist) - 5):              # number of data bytes!
                print(ln, '\n   Invalid Intel Hex record length')
                return False
            chksum = ((sum(bytelist[:-1]) ^ 0xff) + 1) & 0xff       # excl. last byte
            if not chksum == bytelist[-1]:
                print(ln, f'\n   chksum={bytelist[-1]:02X}, expected {chksum:02X}!')
                return False
            return True

        print(f'Loading hex file: {hexfile}')
        segmentsize  = const(0x10000)                               # number of bytes in
                                                                    # a segment of hex file
        usba = 0                                                    # upper address bits (31..16)
        pgmaddr = self.pgm_start * self.memaddr_width               # initial values (byte values)
        eepromaddr = self.eeprom_start * self.memaddr_width
        idaddr = self.id_start * self.memaddr_width
        cfgaddr = self.cfg_start * self.memaddr_width
        pgmmem = bytearray([])                                      # empty pgmmem cache
        fp = open(hexfile)
        for ln in fp:
            ln = ln.strip()                                         # remove irrelevant stuff
            if not hexchk(ln):                                      # check record
                break
            datalen = int(ln[1:3],16)
            offset = int(ln[3:7], 16)                               # memory offset
            code = int(ln[7:9], 16)                                 # record type
            data = bytes([int(ln[i:i+2],16) for i in range(9,len(ln)-2,2)])     # hex data
            if code == 0:                                           # data record type
                newmemaddr = usba * segmentsize + offset            # position of new block
                if self.pgm_rowsize == 1:                           # word-by-word programming
                    yield (newmemaddr // self.memaddr_width), data  # release hex record
                    continue
                if self.pgm_start <= (newmemaddr // self.memaddr_width) < self.pgm_end:  # code mem
                    # collect (cache) program memory to build blocks of <self.pgm_rowsize> words
                    # With full rows return item pairs: (addr, data)
                    # With non-adjacent blocks data may be shorter than self.pgm_rowsize.
                    # Fill spaces between non-adjacent blocks with 'empty' bit patterns
                    if pgmaddr + len(pgmmem) == newmemaddr:
                        # adjacent block: simply append to pgmmem
                        pgmmem += data                              # append bytes to cache
                        # print('Cached pgmmem:', len(pgmmem))
                        while len(pgmmem) >= self.pgm_rowsize * 2:  # release full row(s)
                            yield pgmaddr, pgmmem[0 : self.pgm_rowsize * 2]
                            # print(f'released adjacent pgmmem row {pgmaddr=:06X}')
                            pgmmem = pgmmem[self.pgm_rowsize * 2 :]  # remove first part
                            pgmaddr += self.pgm_rowsize * 2         # next destination
                    else:
                        # block is not adjacent to cached pgmmem
                        # print(f'INFO: address gap {(pgmaddr+len(pgmmem)):06X} -> {newmemaddr=:06X}')
                        if newmemaddr >= pgmaddr + self.pgm_rowsize * 2:
                            # new block is beyond current row
                            # fill up cache with empty words to full row
                            pgmmem += self.filler * (self.pgm_rowsize - len(pgmmem) // 2)
                            # release currently cached pgmmem
                            yield pgmaddr, pgmmem[0 : self.pgm_rowsize * 2]
                            # print('released filled-up pgmmem row')
                            # print(f'{pgmaddr:06X} : {pgmmem}')
                            # fill first part of row with 'empty' bytes before data
                            pgmaddr = newmemaddr & self.rowboundary  # truncate to row boundary
                            # prepend new row 'empty' words (if necessary)
                            pgmmem = bytes(self.filler * ((newmemaddr - pgmaddr) // 2))
                            pgmmem += data                          # append new data
                            # print(f'1. cache: {pgmaddr=:06X} {pgmmem}')
                        else:
                            # new block fits (partly) in current row
                            # fill gap between cached pgmmem and new block
                            pgmmem += self.filler * ((newmemaddr - pgmaddr - len(pgmmem)) // 2)
                            pgmmem += data                          # append data
                            # print(f'2. cache: {pgmaddr=:06X} {pgmmem}')
                            while len(pgmmem) >= self.pgm_rowsize * 2:     # > whole row in cache
                                yield pgmaddr, pgmmem[0 : self.pgm_rowsize * 2]
                                # print(f'released {pgmaddr=:06X} {pgmmem}')
                                pgmmem = pgmmem[self.pgm_rowsize * 2 :]    # remove first part
                                pgmaddr += self.pgm_rowsize * 2
                else:
                    if len(pgmmem) > 0:                             # still some pgmmem (< row)
                        yield pgmaddr, pgmmem                       # remaining pgmmem
                        pgmaddr += len(pgmmem)
                        # print(f'released very last chunck of pgmmem: {len(pgmmem)}')
                        pgmmem = bytearray([])                      # prepare for more to come
                    if self.id_start <= (newmemaddr // self.memaddr_width) < self.id_end:
                        yield idaddr, data
                        idaddr += len(data)
                    elif self.eeprom_start <= (newmemaddr // self.memaddr_width) < self.eeprom_end:
                        yield eepromaddr, data
                        eepromaddr += len(data)
                    elif self.cfg_start <= (newmemaddr // self.memaddr_width) < self.cfg_end:
                        yield cfgaddr, data
                        cfgaddr += len(data)
                    else:
                        print(f'==> ERR: Unexpected destination address: 0x{newmemaddr:06X}')
                        break
            elif code in (1, 4):                                    # address or end of file record
                if len(pgmmem):                                     # still something in pgmmem cache
                    # print(f'{code=} still cached pgmmem {len(pgmmem)}')
                    yield pgmaddr, pgmmem                           # release last portion of pgmmem
                    pgmaddr += len(pgmmem)
                    pgmmem = bytearray([])                          # empty the pgmmem cache
                if code == 4:
                    usba = int.from_bytes(data, 'big')              # remember upper 16 bits of address
                else:
                    break                                           # terminate iteration
            elif code in (2,3,5):                                   # not applicable here
                pass                                                # ignore
            else:                                                   # other record type
                print(f'==> ERR: Unexpected IntelHex record type: 0x{code:02x}')
                break
        fp.close()


# =============================================
class PICSPI(PIC):
    """ C L A S S   for PICs using SPI protocol
        with 8-bits commands and MSb bit sequence
    """
    def __init__(self, spi=None, mclr=None, led=None):

        # <spi> should specify an SPI object, like e.g.:
        #     bus = SPI(0)      # hardware SPI module 0
        # <mclr> is mandatory and should specify a Pin object
        # led is optional, should specify a Pin object (indicates work-in-progress)

        # The ICSPDAT (PGD) pin of the PIC is bi-directional, but SPI has separate
        # output (MOSI) and input (MISO). This requires special wiring.
        # See the README.md and/or view the picture SPI_loopback.png for details.

        super().__init__(mclr, led)

        if isinstance(spi, (SPI, SoftSPI)):
            self.spi = spi                      # SPI instance
            if hasattr(self.spi, "deinit"):     # check if this SPI supports it
                self.spi.deinit()               # reset SPI module
            self.spi.init(baudrate=4000000, polarity=0, phase=1, firstbit=SPI.MSB)   # 4 Mbps
        else:
            print('<spi> should specify an instance of SPI or SoftSPI')

        # programming command codes
        # (supposedly valid for all PICs supporting programming with SPI protocol)
        self.CMD_LOAD_PC_ADDR   = const(0x80)
        self.CMD_BULK_ERASE     = const(0x18)
        self.CMD_LOAD_DATA      = const(0x00)
        self.CMD_LOAD_DATA_INC  = const(0x02)
        self.CMD_READ_DATA      = const(0xFC)
        self.CMD_READ_DATA_INC  = const(0xFE)
        self.CMD_INC_ADDRESS    = const(0xF8)
        self.CMD_BEGIN_INT_PROG = const(0xE0)
        # Specific PICs have other codes for CMD_LOAD_DATA(_INCR)

    def __transfer_bytes_out(self, num, data):
        # transfer data (1 integer)
        # <num> number of bytes to transfer (usually 1 or 3 bytes)
        buf = bytearray(data.to_bytes(num, "big"))          # format buffer, MSB first
        # print(f'__transfer_bytes_out', "".join([f'{buf[i]:02X}' for i in range(len(buf))]))
        try:
            self.spi.write(buf)
        except:
            print(f'Failed writing data to SPI object "{self.spi}"')
        sleep_us(self.Tdly)

    def __command_write(self, command, payload=None):
        # send a command with optionally a payload (integer)
        self.__transfer_bytes_out(1, command)
        # print(f'__command_write: {command:02X}')
        if payload is not None:
            self.__transfer_bytes_out(3, payload << 1)      # shift in stop-bit
            # print(f'__command_write payload: {payload<<1:06X}')

    def __word_read(self, addr=None, inc=True) -> int:
        # read 3 bytes / 24 bits from current or specified program counter address
        # and increment PC, unless inc is False
        # Return input as (14- or 16-bits) integer
        if addr is not None:
            self.__command_write(self.CMD_LOAD_PC_ADDR, addr)   # set address
        self.__command_write(self.CMD_READ_DATA_INC if inc else self.CMD_READ_DATA)   # command byte
        try:
            buffer3 = self.spi.read(3)
        except:
            print(f'Failed reading from SPI object "{self.spi}"')
            return 0
        # print(f'__word_read 0x{addr:06X}', "".join([f'{buffer3[i]:02X}' for i in range(3)]))
        return (int.from_bytes(buffer3, "big") >> 1) & self.empty  # shift out stop bit

    def __word_write(self, data, inc=True):
        # load a word at current PC address
        # increment PC unless inc False
        self.__command_write(self.CMD_LOAD_DATA_INC if inc else self.CMD_LOAD_DATA, data)

    def __words_write_verify(self, addr, data):
        # write verify a list of words beginning at specified address
        # There are dependencies of PIC types and memory regions
        # Returns True when all OK, False with the first verify error
        self.__command_write(self.CMD_LOAD_PC_ADDR, addr)       # start point
        # print(f'{addr=:06X} {data}')
        for i in range(len(data)):
            data[i] = self.__check_and_set_lvp_bit(addr + i, data[i])   # force LVP-bit 1
            # print(f'__words_write 0x{addr+i:06X} {data[i]:04X}')
            self.__word_write(data[i], inc=False)               # do not increment PC
            if self.__class__.__qualname__.startswith('PIC18FQ'):
                if (self.pgm_start <= addr < self.pgm_end or    # pgm mem
                    self.id_start <= addr < self.id_end):       # ID mem
                    sleep_us(self.Tpint)
                else:
                    sleep_us(self.Tpdfm)                        # eeprom, cfgmem
            else:
                self.__command_write(self.CMD_BEGIN_INT_PROG)
                sleep_us(self.Tpint)                            # word programming
            if (readback := self.__word_read()) != data[i]:     # verify, increment PC
                print(self.fmt_verify.format(addr+i, data[i], readback))
                return False
        return True

    def __row_write_verify(self, addr, data):
        # write and verify a row of program memory
        # the address should be on row boundary (forced here!)
        # Returns True when all OK, False with the first verify error
        # print(f'{addr=} mask: {self.rowboundary})
        if not (temp := (addr & self.rowboundary)) == addr:
            print(fmt_rowalign.format(addr, temp))
            addr = temp                                        # force row alignment
        # print('write at {:06X} : '.format(addr), end = '')
        # print(f'[{len(data)}]', " ".join("{:04X}".format(x) for x in data))
        self.__command_write(self.CMD_LOAD_PC_ADDR, addr)       # set to start of row
        for i in range(min(len(data), self.pgm_rowsize) - 1):   # all words but last
            self.__word_write(data[i])                          # with increment addr
        self.__word_write(data[-1], inc=False)                  # last word: no increment addr
        self.__command_write(self.CMD_BEGIN_INT_PROG)           # program the row
        sleep_us(self.Tpint)                                    # row programming
        self.__command_write(self.CMD_LOAD_PC_ADDR, addr)       # start of row
        for i in range(min(len(data), self.pgm_rowsize)):       # all words
            if (readback := self.__word_read()) != data[i]:     # verify
                print(self.fmt_verify.format(addr+i, data[i], readback))
                return False
        return True

    # = = = P U B L I C   M E T H O D S  = = =

    def lvp_start(self):
        # initiate LVP mode and send magic pattern
        self.__lvp_start()                              # control MCLR
        self.__transfer_bytes_out(4, self.lvp_magic)    # send magic pattern for LVP
        sleep_us(self.Tenth)

    def lvp_stop(self):
        # stop LVP mode
        # print("Leaving LVP mode")
        if isinstance(self.spi, (SPI, SoftSPI)):        # when using SPI
            if hasattr(self.spi, "deinit"):             # check if this SPI supports it
                self.spi.deinit()                       # reset SPI module
        self.mclr.on()                                  # reset PIC to normal operation
        if self.led is not None:
            self.led.off()                              # LED off

    def lvp_check_target(self):
        # obtain devid and revid to identify the actual target PIC
        # when target identified collect PIC specific programming
        # attributes (mostly timings) from picpgmcompactinfo.
        # return - Type (name) of detected PIC
        self.revid = self.__word_read(self.revid_start, inc=False)
        self.devid = self.__word_read(self.devid_start, inc=False)
        self.__determine_target()
        return self.target

    def lvp_bulk_erase(self):
        # bulk erase operation
        # two variants: by region-mask or per region-address
        if self.target is None:
            return False
        print(f'Bulk erase of {self.target}')
        if self.bulk_erase_by_mask:
            self.__command_write(self.CMD_BULK_ERASE, 0x0F)     # all memory regions
            # print('bulk erase by mask: 0x0F')
        else:
            self.__command_write(self.CMD_LOAD_PC_ADDR, self.id_start)  # pgm, id, config memory
            self.__command_write(self.CMD_BULK_ERASE)
            sleep_us(self.Terab)
            if self.eeprom_end - self.eeprom_start > 0:         # EEPROM present
                self.__command_write(self.CMD_LOAD_PC_ADDR, self.eeprom_start)
                self.__command_write(self.CMD_BULK_ERASE)
        sleep_us(self.Terab)
        # verify that program memory was erased (last and first words):
        print('Verifying result of bulk erase....')
        if not (temp := self.__word_read(self.pgm_end - 2)) == self.empty:
            print(self.fmt_erase.format(self.pgm_end - 2, temp))
            return False
        if not (temp := self.__word_read(self.pgm_start)) == self.empty:
            print(self.fmt_erase.format(self.pgm_start, temp))
            return False
        return True

    def lvp_load_hexfile(self, hexfile):
        # programming mainline
        # use generator function '__hexfile_cache()' as source
        # every next item from cache consists of (address, bytearray)
        # address is directly from hex file and must be converted to
        # word-address for midrange (18Fs use byte-address)
        # function terminates after first verify error
        if not self.lvp_bulk_erase():                               # all memory regions
            return False                                        # bulk erase failed
        cache = self.__hexfile_cache(hexfile)                   # initialize the cache
        while True:                                             # until end of hex file or pgmg error
            try:
                if self.led is not None:
                    self.led.value(self.led.value() ^ 1)        # toggle the LED
                addr, data = next(cache)                        # get next hexfile item
                # print(f'{addr=:06X} {data=}')
                addr //= self.memaddr_width                     # byte- to word-address
                # convert data to a list of words
                datalist = [(int.from_bytes(data[i:i+2], "little")) for i in range(0, len(data), 2)]
                # print(f'{addr:06x}', " ".join([f'{datalist[i]:04X}' for i in range(len(datalist))]))
                # select the proper memory region
                if self.pgm_start <= addr < self.pgm_end:
                    # Some PICs do not support row-programming for program memory
                    if self.pgm_rowsize > 1:
                        if not self.__row_write_verify(addr, datalist):
                            return False
                    else:
                        if not self.__words_write_verify(addr, datalist):
                            return False
                elif self.id_start <= addr < self.id_end:
                    if not self.__words_write_verify(addr, datalist):
                        return False
                elif self.eeprom_start <= addr < self.eeprom_end:
                    if self.memaddr_width == 1:                 # advanced midrange PIC (18F)
                        # Hexfile of 18Fs has EEPROM in bytes (16Fs: words)
                        datalist = [data[i] for i in range(len(data))]
                    if not self.__words_write_verify(addr, datalist):
                        return False
                elif self.cfg_start <= addr < self.cfg_end:
                    if self.cfg_wordsize == 1:
                        # config memory of PICs like 18FxxQxx is programmed per byte
                        datalist = [data[i] for i in range(len(data))]
                    if not self.__words_write_verify(addr, datalist):
                        return False
                else:
                    print(self.fmt_address.format(addr))
                    return False
            except StopIteration:
                break
        return True


# ================================================================
class PICBB4(PIC):
    """ C L A S S  using bit-banging
        with 4-bits commands and LSb-first or MSb-first data transfer
        Like (but not exclusive) for 18F2xJxx / 18F4xJxx
    """

    def __init__(self, mclr=None, icspclk=None, icspdat=None, lsb=True, led=None):
        # led is optional, used to indicate work-in-progress (blinking).
        # mclr, icspclk, icspdat and led should specify Pin objects.
        # mclr and iscpclk pins should be capable of output,
        # icspdat pin is used bi-directionally.

        super().__init__(mclr, led)

        # programming command codes (4-bits)
        self.CMD_CORE                 = const(0x00)
        self.CMD_SHIFTOUT_TABLAT      = const(0x02)
        self.CMD_TABLE_READ           = const(0x08)
        self.CMD_TABLE_READ_INCR      = const(0x09)
        self.CMD_TABLE_WRITE          = const(0x0C)
        self.CMD_TABLE_WRITE_INCR     = const(0x0D)
        # self.CMD_TABLE_WRITE_PGM_INCR = const(0x0E)
        self.CMD_TABLE_WRITE_PGM      = const(0x0F)

        # Programming timing values in microseconds
        self.Tdly1   = 1         # >= 50 ns
        self.Tdly1a  = 1         # >= 50 ns
        self.Tdly5   = 3400      # >= 3.4 ms
        self.Tdly5a  = 50        # xxx us
        self.Tdly6   = 54000     # >= 54 ms
        self.Tdly7   = 524000    # >= 524 ms
        self.Thld2   = 400       # >= 400 us
        self.Tset2   = 1         # >= 100 ns
        self.Tdly8   = 1         # >= 20 ns
        self.Thld3   = 3         # >= 3 us
        self.Tkey1   = 4000      # >= 4 ms

        self.icspclk = icspclk
        self.icspclk.init(Pin.OUT)
        self.icspclk.off()
        self.icspdat = icspdat
        self.icspdat.init(Pin.OUT)                      # initially (is bi-directional)
        self.icspdat.off()
        self.lsb = lsb
        self.led = led

    def __transfer_bits_out(self, bits, data, lsb=True):
        # Transfer number of bits of data
        # bits are presented before rising edge of clock signal
        # (which allows transfer on rising and falling edge of clock signal)
        # clock speed is uncontrolled (as fast as possible, micropython is slow enough)
        # default bit transfer sequence (LSb-first) may be overridden
        # icspdat is supposed to be in output mode
        mask = 1 if lsb else (1 << (bits - 1))          # LSb-first or MSb-first
        for _ in range(bits):
            self.icspdat.value((data & mask) == mask)
            self.icspclk.on()                           # data clocked at rising or falling edge!
            self.icspclk.off()
            if lsb:
                mask <<= 1
            else:
                mask >>= 1
        self.icspdat.off()                              # PGD low
        # sleep_us(self.Tdly1 + self.Tdly1a)            # (100 ns)

    def __transfer_bits_in(self, lsb=True) -> int:
        # Read byte from current memory address, return integer (8 bits)
        # bit value is determined after the falling edge of the clock signal
        # default bit transfer sequence (LSb-first) may be overridden
        self.__transfer_bits_out(8, 0x00, lsb=lsb)      # 8 clock cycles icspdat low
        self.icspdat.init(Pin.IN)                       # temporary to input mode
        data = 0
        mask = 0x01 if lsb else 0x80                    # LSb or MSb
        for _ in range(8):                              # 8 clock cycles
            self.icspclk.on()
            self.icspclk.off()
            if self.icspdat.value():                    # high
                data |= mask                            # store 1-bit
            if lsb:
                mask <<= 1
            else:
                mask >>= 1
        self.icspdat.init(Pin.OUT)                      # back to output mode
        self.icspdat.off()                              # PGD low
        return int(data)                                # raw data (8 bits)

    def __command_write(self, command, payload=None):
        # send a command with optionally a payload (16 bits integer)
        # print(f'{command:02X}')
        self.__transfer_bits_out(4, command, lsb=self.lsb)
        if payload is not None:
            # print(f'    {payload:04X}')
            self.__transfer_bits_out(16, payload, lsb=self.lsb)

    def __adjust_address(self, addr):
        # adjust address to desired value
        # print(f'new address: {addr:06X}')
        self.__command_write(self.CMD_CORE, 0x0E00 | ((addr >> 16) & 0x3F))
        self.__command_write(self.CMD_CORE, 0x6EF8)
        self.__command_write(self.CMD_CORE, 0x0E00 | ((addr >> 8) & 0xFF))
        self.__command_write(self.CMD_CORE, 0x6EF7)
        self.__command_write(self.CMD_CORE, 0x0E00 | (addr & 0xFF))
        self.__command_write(self.CMD_CORE, 0x6EF6)

    def __word_read(self, addr=None) -> int:
        # Read a word at current or specified programming address
        if addr is not None:
             self.__adjust_address(addr)                # set start address
        self.__command_write(self.CMD_TABLE_READ_INCR)
        data1 = self.__transfer_bits_in(lsb=self.lsb)   # LSB
        self.__command_write(self.CMD_TABLE_READ_INCR)
        data2 = self.__transfer_bits_in(lsb=self.lsb)   # MSB
        return (data2 << 8) | data1                     # word (little Endian)

    def __programming_delay(self, delay1, delay2):
        # NOP instruction with command-payload splitted with intermediate delay
        self.__transfer_bits_out(3, 0x00)               # 3 clock cycles of NOP
        self.icspclk.on()                               # long 4th clock cycle
        sleep_us(delay1)                                # first delay
        self.icspclk.off()                              # PGC low
        sleep_us(delay2)                                # second delay
        self.__transfer_bits_out(16, 0x0000)            # payload of NOP command

    def __words_write_verify(self, addr, datalist) -> bool:
        # write a list of words (full row) starting at specified address
        # the target must have been write-enabled before (e.g. in lvp_load_hexfile)
        # no row-alignment required (?)
        self.__adjust_address(addr)
        for i in range(len(datalist) - 1):              # all but last word
            self.__command_write(self.CMD_TABLE_WRITE_INCR, datalist[i])
        self.__command_write(self.CMD_TABLE_WRITE_PGM, datalist[-1])   # last word
        self.__programming_delay(self.Tdly5, self.Tdly1a)   # time for programming
        self.__adjust_address(addr)                     # back to start address
        for i in range(min(len(datalist), self.pgm_rowsize)):   # all programmed words
            readback = self.__word_read()               # incl increment addr
            if readback != datalist[i]:                 # verify
                print(self.fmt_verify.format(addr+i*self.cfg_wordsize, datalist[i], readback))
                return False
        return True

    def __row_write_verify(self, addr, data) -> bool:
        # write and verify a row of program memory
        # the address should be on row boundary (forced here!)
        # Returns True when all OK, False with the first verify error
        if not (temp := (addr & self.rowboundary)) == addr:
            print(fmt_rowalign.format(addr, temp))
            addr = temp
        # print("write at {:06X}: ".format(addr), end = "")
        # print("".join("{:02x}".format(x) for x in data))
        return self.__words_write_verify(addr, data)    # write row

    def __eeprom_write_verify(self, addr, datalist):
        # EEPROM programming
        # this method may be replaced by some specific PIC families
        self.__words_write_verify(addr, datalist)

    def __config_write_verify(self, addr, datalist):
        # prepare for writing a full row
        # this method may be replaced by some specific PIC families
        # prepend and append datalist with 'empty' words if necessary
        rowaddr = addr & self.rowboundary               # truncate to row alignment
        prepwords = (addr - rowaddr) * self.memaddr_width // 2   # number of words
        prepdata = [self.empty] * prepwords
        appwords = self.pgm_rowsize - prepwords - len(datalist)
        appdata = [self.empty] * appwords
        # modified datalist (full row):
        datalist = prepdata + datalist + appdata
        # self.__print_datalist('code', rowaddr, datalist)
        self.__row_write_verify(rowaddr, datalist)


    # ==== P U B L I C   M E T H O D S

    def lvp_start(self):
        # initiate LVP mode and send magic pattern (with MSb first!)
        # data bits should be presented at rising edge of clock signal
        self.mclr.off()
        # sleep_us(self.Tset2)                  # (100 ns)
        self.mclr.on()                          # unspecified in Pgmpec
        self.mclr.off()                         # 6-7 us pulse seems sufficiently long
        sleep_us(self.Tkey1)                    # (4 ms) MCLR-off -> 1st bit ICSPDAT
        # now send magic pattern
        self.__transfer_bits_out(32, self.lvp_magic, lsb=False)  # herewith MSb first!
        # sleep_us(self.Tkey2)                  # (50 ns)
        self.mclr.on()                          # ON with these PICs
        sleep_us(self.Thld2)                    # (400 us)

    def lvp_stop(self):
        # terminate LVP
        self.icspclk.off()
        self.icspdat.off()
        self.icspclk.init(Pin.IN)               # )
        self.icspdat.init(Pin.IN)               # ) high impedance
        # sleep_us(self.Tdly8)                  # (20 ns)
        self.mclr.off()                         # terminate LVP
        self.mclr.on()                          # start normal operation

    def lvp_check_target(self) -> str:
        # obtain devid and revid to identify the actual target PIC
        # return - Type (name) of detected PIC
        self.revid = self.__word_read(self.revid_start)
        self.devid = self.__word_read(self.devid_start)
        self.__determine_target()
        return self.target

    def lvp_bulk_erase(self):
        # bulk erase operation
        print(f'Bulk erase of {self.target}')
        self.__adjust_address(0x3C0005)
        self.__command_write(self.CMD_TABLE_WRITE, self.bulk_erase_word1)
        self.__adjust_address(0x3C0004)
        self.__command_write(self.CMD_TABLE_WRITE, self.bulk_erase_word2)
        self.__command_write(self.CMD_CORE, 0x0000)     # NOP
        self.__command_write(self.CMD_CORE)             # NOP command
        sleep_us(self.Tdly7 + self.Tdly6)
        self.__transfer_bits_out(16, 0x0000)            # payload of previous NOP
        # verify that program memory was erased (last and first word):
        print('Verifying result of bulk erase....')
        if not (temp := self.__word_read(self.pgm_end - 2)) == self.empty:
            print(self.fmt_erase.format(self.pgm_end - 2, temp))
            return False
        if not (temp := self.__word_read(self.pgm_start)) == self.empty:
            print(self.fmt_erase.format(self.pgm_start, temp))
            return False
        return True

    def lvp_load_hexfile(self, hexfile) -> bool:
        # programming mainline
        # use generator function '__hexfile_cache()' as source
        # every next item from cache consists of (address, bytearray)
        # address is directly from hex file (byte address)
        # print(f'{self.eeprom_start=:06X} {self.eeprom_end=:06X}')
        if not self.lvp_bulk_erase():                       # all memory regions
            return False                                    # bulk erase failed
        cache = self.__hexfile_cache(hexfile)               # initialize the cache
        self.__command_write(self.CMD_CORE, 0x84A6)         # write enable of target (code memory)
        while True:                                         # until end of hex file or pgmg error
            try:
                if self.led is not None:
                    self.led.value(self.led.value() ^ 1)    # toggle the LED
                addr, data = next(cache)                    # get next hexfile item
                # convert word-oriented bytearray into a list of words
                datalist = [(int.from_bytes(data[i:i+2], "little")) for i in range(0, len(data), 2)]
                # select the proper memory region
                # print(f'{addr=:06X} {datalist[0]=:04X}')
                if self.pgm_start <= addr < self.pgm_end:
                    # prepare for writing a full row
                    # prepend and append datalist with 'empty' words if necessary
                    rowaddr = addr & self.rowboundary           # truncate to row alignment
                    prepwords = (addr - rowaddr) * self.memaddr_width // 2   # number of words
                    prepdata = [self.empty] * prepwords
                    appwords = self.pgm_rowsize - prepwords - len(datalist)
                    appdata = [self.empty] * appwords
                    # modified datalist (full row):
                    datalist = prepdata + datalist + appdata
                    # self.__print_datalist('code', rowaddr, datalist)
                    if not self.__row_write_verify(rowaddr, datalist):
                        return False
                elif self.id_start <= addr < self.id_end:
                    if not self.__words_write_verify(addr, datalist):
                        return False
                elif self.eeprom_start <= addr < self.eeprom_end:
                    if not self.__eeprom_write_verify(addr, data):
                        return False
                elif self.cfg_start <= addr < self.cfg_end:
                    if not self.__config_write_verify(addr, datalist):
                        return False
                else:
                    print(self.fmt_address.format(addr))
                    return False
            except StopIteration:
                break
        return True

# =============================================
class PICBB6(PIC):
    """ C L A S S  for using bit-banging
        with 6-bits commands and LSb-first sequence
    """
    def __init__(self, mclr=None, icspclk=None, icspdat=None, led=None):
        # led is optional, used to indicate work-in-progress (blinking).
        # mclr, icspclk, icspdat and led should specify Pin objects.
        # mclr and iscpclk pins should be capable of output,
        # icspdat pin is used bi-directionally.

        super().__init__(mclr, led)

        # programming command codes
        self.CMD_LOAD_CONFIG     = const(0x00)
        self.CMD_LOAD_DATA_PGM   = const(0x02)
        self.CMD_LOAD_DATA_DATA  = const(0x03)
        self.CMD_READ_DATA_PGM   = const(0x04)
        self.CMD_READ_DATA_DATA  = const(0x05)
        self.CMD_INCR_ADDR       = const(0x06)
        self.CMD_RESET_ADDR      = const(0x16)
        self.CMD_BEGIN_INT_PGMG  = const(0x08)
        self.CMD_BULK_ERASE_PGM  = const(0x09)
        self.CMD_BULK_ERASE_DATA = const(0x11)

        self.icspclk = icspclk
        self.icspclk.init(Pin.OUT)
        self.icspclk.off()
        self.icspdat = icspdat
        self.icspdat.init(Pin.OUT)
        self.icspdat.off()

        self.current_addr = self.pgm_start                  # current programming address

    def __transfer_bits_out(self, bits, data):
        # Transfer number of bits of data
        # uncontrolled clock timing (micropython is slow enough)
        # iocspdat pin assumed to be in output mode
        sleep_us(self.Tlzd)
        mask = 1                                            # LSbit first
        for _ in range(bits):
            self.icspclk.on()
            self.icspdat.value((data & mask) == mask)
            self.icspclk.off()
            mask <<= 1
        sleep_us(self.Tdly)                                 # always required

    def __transfer_bits_in(self) -> int:
        # Read word from current memory address, return integer (16 bits)
        # 'automatic' (uncontrolled) clock timing
        self.icspdat.init(Pin.IN)                           # icspdat: input
        sleep_us(self.Thzd)
        data = 0                                            # init (16-bits) payload
        mask = 1                                            # LSbit first
        for _ in range(16):                                 # 16 clock cycles
            self.icspclk.on()
            self.icspclk.off()
            if self.icspdat.value():                        # 'on'
                data |= mask                                # store 1-bit
            mask <<= 1
        self.icspdat.init(Pin.OUT)                          # back to output mode
        self.icspdat.off()                                  # PGD low
        return data                                         # raw data (16 bits)

    def __command_write(self, command, payload=None):
        # send a command with optionally a payload (integer)
        self.__transfer_bits_out(6, command)
        if payload is not None:
            self.__transfer_bits_out(16, payload << 1)      # payload + stopbit

    def __adjust_address(self, addr):
        # adjust current_addr to desired value
        # print(f'__adjust_address {addr:06X} {self.current_addr=:06X}')
        if addr < self.id_start:                            # destination in program memory
            if (self.current_addr >= self.id_start or       # currently in config memory
                self.current_addr > addr):                  # or beyond <addr>\
                # print(f'cmd_reset_addr')
                self.__command_write(self.CMD_RESET_ADDR)   # to start of pgm memory
                self.current_addr = self.pgm_start
        else:                                               # destination in userid/config memory
            if (self.current_addr < self.id_start or        # currently in program memory
                    self.current_addr > addr):              # or beyond <addr>
                # print('cmd_load_config')
                self.__command_write(self.CMD_LOAD_CONFIG, self.empty)  # config memory
                self.current_addr = self.id_start           # to start of id_memory
        while self.current_addr < addr:                     # shift to destination
            # print(f'__adjust_address: 0x{self.current_addr:06X} -> 0x{addr:06X}')
            self.__command_write(self.CMD_INCR_ADDR)
            self.current_addr += 1

    def __word_read(self, addr=None):
        # Read a word at current or specified programming address
        if addr is not None:
            self.__adjust_address(addr)                     # set address
        self.__command_write(self.CMD_READ_DATA_PGM)        # command byte
        data = self.__transfer_bits_in()                    # obtain word
        return (data >> 1) & self.empty                     # remove stopbit, return 14-bits value

    def __words_write_verify(self, addr, data):
        # write verify a list of words starting at specified address
        # Returns True when all OK, False with the first verify error.
        self.__adjust_address(addr)                         # set begin address
        for i in range(len(data)):
            data[i] = self.__check_and_set_lvp_bit(addr + i, data[i])   # force LVP-bit 1
            self.__command_write(self.CMD_LOAD_DATA_PGM, data[i])
            self.__command_write(self.CMD_BEGIN_INT_PGMG)   # latched word to memory
            sleep_us(self.Tpint)                            # settle
            if (readback := self.__word_read()) != data[i]:
                print(self.fmt_verify.format(addr, data[i], readback))
                return False
            self.__command_write(self.CMD_INCR_ADDR)
            self.current_addr += 1
        return True

    # INFO: row-by-row programming is inefficient due to needed address changes


    # ==== P U B L I C   M E T H O D S

    def lvp_start(self):
        # initiate LVP mode and send magic pattern
        self.__lvp_start()                                      # MCLR low
        self.__transfer_bits_out(33, self.lvp_magic)            # magic LVP pattern + 1 extra clock cycle
        sleep_us(self.Tenth)                                    # (maybe not needed)

    def lvp_stop(self):
        # stop LVP mode
        # print("Leaving LVP mode")
        self.icspclk.off()
        self.icspdat.off()
        self.icspclk.init(Pin.IN)
        self.icspdat.init(Pin.IN)
        self.mclr.on()                                          # reset PIC to normal operation
        # Leave the LED in its latest state (may indicate success or failure)
        # if self.led is not None:
        #    self.led.off()                                      # LED off

    def lvp_check_target(self):
        # obtain devid and revid to identify the actual target PIC
        # return - Type (name) of detected PIC
        self.revid = self.__word_read(self.revid_start)
        self.devid = self.__word_read(self.devid_start)
        self.__determine_target()
        return self.target

    def lvp_bulk_erase(self):
        # bulk erase operation
        # EXCEPTION: no LVP bulk erase for 12/16F182x revision <= 6
        if (self.target.find('F182') >= 0) and (self.revid <= 6):
            print(f'No LVP bulk erase for {self.target} revision <= {self.revid}!')
            return False
        print(f'Bulk erase {self.target}')
        self.__adjust_address(self.id_start)                    # for all regions
        self.__command_write(self.CMD_BULK_ERASE_PGM)
        sleep_us(self.Terab)
        # verify that program memory was erased (last and first word):
        print('Verifying result of bulk erase....')
        if not (temp := self.__word_read(self.pgm_end - 2)) == self.empty:
            print(self.fmt_erase.format(self.pgm_end - 2, temp))
            return False
        if not (temp := self.__word_read(self.pgm_start)) == self.empty:
            print(self.fmt_erase.format(self.pgm_start, temp))
            return False
        return True

    def lvp_load_hexfile(self, hexfile):
        # programming mainline
        # use generator function '__hexfile_cache()' as source
        # every next item from cache consists of (address, bytearray)
        # address is directly from hex file and must be converted to
        # word-address for midrange
        # function terminates after first verify error
        if not self.lvp_bulk_erase():                           # all memory regions
            return False                                        # bulk erase failed
        cache = self.__hexfile_cache(hexfile)                   # initialize the cache
        while True:                                             # until end of hex file or pgmg error
            try:
                if self.led is not None:
                    self.led.value(self.led.value() ^ 1)        # toggle the LED
                addr, data = next(cache)                        # get next hexfile item
                addr //= self.memaddr_width                     # to word address for midrange!
                # convert byte array to a list of words
                datalist = [(int.from_bytes(data[i:i+2], "little")) for i in range(0, len(data), 2)]
                # select the proper memory region
                if self.pgm_start <= addr < self.pgm_end:
                    if not self.__words_write_verify(addr, datalist):
                        return False
                elif self.eeprom_start <= addr < self.eeprom_end:
                    if not self.__words_write_verify(addr, datalist):
                        return False
                elif self.id_start <= addr < self.id_end:
                    if not self.__words_write_verify(addr, datalist):
                        return False
                elif self.cfg_start <= addr < self.cfg_end:
                    if not self.__words_write_verify(addr, datalist):
                        return False
                else:
                    print(self.fmt_address.format(addr))
                    return False
            except StopIteration:
                break
        return True

# ===================================================================
class PIC16F145x(PICBB6):           # PgmSpec 40001620
    # using bit-banging with 6-bits commands
    def __init__(self, mclr=None, icspclk=None, icspdat=None, led=None):
        # see with 'class PICBB6' for requirements of mclr, icspdat/clk and led

        super().__init__(mclr, icspclk, icspdat, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 5000
        self.Tpint  = 5000

class PIC12F150x(PIC16F145x):   # PgmSpec 40001573
    pass
class PIC16F150x(PIC16F145x):   # PgmSpec 40001573
    pass
class PIC12F157x(PIC16F145x):   # PgmSpec 40001713
    pass
class PIC16F157x(PIC16F145x):   # PgmSpec 40001766
    pass
class PIC12F161x(PIC16F145x):   # Pgmspec 40001720
    pass
class PIC16F161x(PIC16F145x):   # Pgmspec 40001720
    pass
class PIC16F17xx(PIC16F145x):   # Pgmspec 40001457, 40001683, 40001754, 40001782
    pass
class PIC12F18xx(PIC16F145x):   # PgmSpec 40001390
    pass
class PIC16F18xx(PIC16F145x):   # PgmSpec 40001439
    pass

# ===================================================================
class PIC16F131xx(PICSPI):      # PgmSpec 40002500
    # C L A S S  for PIC16F131xx family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 20000
        self.Tpint  = 12000

        # programming properties
        self.bulk_erase_by_mask = True

# ===================================================================
class PIC16F152xx(PICSPI):      # PgmSpec 40002149
    # C L A S S  for PIC16F152xx family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 13000
        self.Tpint  = 5600

# ===================================================================
class PIC16F153xx(PICSPI):      #  PgmSpec 40001838
    # C L A S S  for PIC16F153xx family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 8400
        self.Tpint  = 5600

# ===================================================================
class PIC16F171xx(PICSPI):          # PgmSpec 40002266
    # C L A S S  for PIC16F171xx family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 16800             # pgmspec 8.4 ms!
        self.Tpint  = 5600

        # programming properties
        self.bulk_erase_by_mask = True

# ===================================================================
class PIC16F180xx(PICSPI):          # PgmSpec 40002317
    # C L A S S  for PIC16F180xx family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 13000
        self.Tpint  = 5600

        # programming properties
        self.bulk_erase_by_mask = True

# ===================================================================
class PIC16F181xx(PICSPI):           # PgmSpec 40002276
    # C L A S S  for PIC16F181xx family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 8400
        self.Tpint  = 5600

        # programming properties
        self.bulk_erase_by_mask = True

# ===================================================================
class PIC16F184xx(PICSPI):          # PgmSpec 40001970
    # C L A S S  for PIC16F184xx/8xx families
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 14000
        self.Tpint  = 5600

class PIC16F188xx(PIC16F184xx):     # PgmSpec 40001753
    pass

# ===================================================================
class PIC16F191xx(PICSPI):          #  PgmSpec 40001880
    # C L A S S  for PIC16F191xx family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth = 250
        self.Terab = 8400
        self.Tpint = 5600

# ===================================================================
class PIC18F2xxx(PICBB4):
    # C L A S S  for PIC18F2xxx/4xxx family, pgmspec 30009622
    # (note: exclusive 18F1/2/4220, pgmspec 30009592)
    def __init__(self, mclr=None, icspclk=None, icspdat=None, lsb=True, led=None, pgm=None):
        # see with 'class PICBB4' for requirements of mclr, icspdat/clk and led
        # pgm is only for this PIC class (entering LVP mode by PGM pin)

        super().__init__(mclr, icspclk, icspdat, lsb, led)

        if pgm is None:
            print('pgm pin must be specified!')
            return
        self.pgm = pgm if isinstance(pgm, Pin) else Pin(pgm)  # Pin instance
        self.pgm.init(Pin.OUT)
        self.pgm.off()                           # start low

        # Programming timing values in microseconds
        self.Tdly5 = 1000           # P9
        self.Tdly6 = 100            # P10
        self.Tdly7 = 5000           # P11
        self.Tdly8 = 1              # P16
        self.Thld2 = 2              # P12
        self.Thld4 = 1              # P18
        self.Tset3 = 3              # P15

        # programming properties
        self.bulk_erase_word1 = 0x3F3F
        self.bulk_erase_word2 = 0x8F8F
        self.cfg_wordsize = 1

    def lvp_start(self):
        self.mclr.off()                         # should be low initially
        # initiate LVP mode by rising pgm pin
        self.pgm.on()                           # pgm pin high
        sleep_us(self.Tset3)
        self.mclr.on()                          # mclr high
        sleep_us(self.Thld2)

    def lvp_stop(self):
        # terminate LVP
        self.icspclk.off()
        self.icspdat.off()
        self.icspclk.init(Pin.IN)               # )
        self.icspdat.init(Pin.IN)               # ) high impedance
        # sleep_us(self.Tdly8)
        self.mclr.off()
        self.pgm.off()                          # pgm pin low
        self.mclr.on()                          # start normal operation

    def __eeprom_write_verify(self, addr, data):
        # alternate method for this class than in PICBB4
        # print('Programming EEPROM (PIC18F2xxx specific)')
        # EEPROM is programmed per byte
        datalist = [data[i] for i in range(len(data))]          # make list of bytes
        self.__command_write(self.CMD_CORE, 0x9EA6)             # clear EECON1.EEPGD
        self.__command_write(self.CMD_CORE, 0x9CA6)             # clear EECON1.CFGS
        for i in range(len(datalist)):
            self.__command_write(self.CMD_CORE, 0x0E00 + ((addr + i) & 0xFF))   # LSB addr
            self.__command_write(self.CMD_CORE, 0x6EA9)
            self.__command_write(self.CMD_CORE, 0x0E00 + (((addr + i) >> 8) & 0xFF))  # MSB of addr
            self.__command_write(self.CMD_CORE, 0x6EAA)
            self.__command_write(self.CMD_CORE, 0x0E00 + (datalist[i] & 0xFF))  # data byte
            self.__command_write(self.CMD_CORE, 0x6EA8)
            self.__command_write(self.CMD_CORE, 0x84A6)         # set EECON1.WREN
            self.__command_write(self.CMD_CORE, 0x82A6)         # set WR bit: initiate write
            WRbit = False
            while WRbit is False:
                self.__command_write(self.CMD_CORE, 0x50A6)     # EECON1 -> w
                self.__command_write(self.CMD_CORE, 0x6EF5)     # w -> TABLAT
                self.__command_write(self.CMD_CORE)             # NOP (payload later)
                self.__command_write(self.CMD_SHIFTOUT_TABLAT)  # shift out the data
                eecon1 = self.__transfer_bits_in()              # read EECON1
                WRbit = (eecon1 & 0x02) == 0x00
            sleep_us(self.Tdly6)                                # P10 / Tdly6
            self.__transfer_bits_out(16, 0x00)                  # payload of last NOP command
            self.__command_write(self.CMD_CORE, 0x94A6)         # disable EEPROM writes
            # verify EEPROM byte
            self.__command_write(self.CMD_CORE, 0x80A6)         # enable EEPROM read
            self.__command_write(self.CMD_CORE, 0x50A8)         # EEDATA -> w
            self.__command_write(self.CMD_CORE, 0x6EF5)         # w -> TABLAT
            self.__command_write(self.CMD_CORE, 0x0000)         # NOP
            self.__command_write(self.CMD_SHIFTOUT_TABLAT)      # shift out
            eedata = self.__transfer_bits_in()                  # read EECON1
            # print(f'EEPROM {i} out {datalist[i]:02X} in {eedata:02X}')
            if not eedata == data[i]:
                print(self.fmt_verify.format(addr, data[i], eedata))
                return False
        return True

    def __config_write_verify(self, addr, datalist):
        # alternate method for this class than in PICBB4
        # print(f'Programming config words (PIC18F2xxx specific)')
        self.__command_write(self.CMD_CORE, 0x8EA6)             # set EEPGD
        self.__command_write(self.CMD_CORE, 0x8CA6)             # set CFGS
        for i in range(len(datalist)):
            cfgword = datalist[i]
            self.__adjust_address(addr + 2 * i)                         # set addr
            self.__command_write(self.CMD_TABLE_WRITE_PGM, cfgword)     # LSB (MSB ignored)
            self.__programming_delay(self.Tdly5, self.Tdly6)            # P9 + P10
            self.__adjust_address(addr + 2 * i + 1 )                    # next byte
            self.__command_write(self.CMD_TABLE_WRITE_PGM, cfgword)     # MSB (LSB ignored)
            self.__programming_delay(self.Tdly5, self.Tdly6)
            # readback
            self.__adjust_address(addr + 2 * i)                         # set addr
            self.__command_write(self.CMD_TABLE_READ_INCR)
            lsb = self.__transfer_bits_in()                             # LSB
            self.__command_write(self.CMD_TABLE_READ_INCR)
            msb = self.__transfer_bits_in()                             # MSB
            # verify config bytes (limited to the implemented bits)
            x = (addr + i - self.cfg_start) * 4                 # offset of mask in cfg_impl
            lsb_fix = ( cfgword       & 0xff) & int(self.cfg_impl[x  :x+2], 16)  # anded with impl mask
            msb_fix = ((cfgword >> 8) & 0xff) & int(self.cfg_impl[x+2:x+4], 16)
            if not (lsb == lsb_fix) and (msb == msb_fix):
                # display bytes as words in hex (little endian notation)
                print(self.fmt_verify.format(addr + i*2, msb + lsb*256, msb_fix + lsb_fix*256))
                return False
        return True

class PIC18F4xxx(PIC18F2xxx):
    pass

class PIC18F2685(PIC18F2xxx):
    pass



# ===================================================================
class PIC18FJ5x(PICBB4):
    # C L A S S  for PIC18F2xJ5x/4xJ5x family, PgmSpec 30009687
    def __init__(self, mclr=None, icspclk=None, icspdat=None, lsb=True, led=None):
        # see with 'class PICBB4' for requirements of mclr, icspdat/clk and led

        super().__init__(mclr, icspclk, icspdat, lsb, led)

        # programming properties
        self.bulk_erase_word1 = 0x0101
        self.bulk_erase_word2 = 0x8080

class PIC18FJ10(PIC18FJ5x):
    pass
class PIC18FJ11(PIC18FJ5x):
    pass
class PIC18FJ13(PIC18FJ5x):
    pass
class PIC18FJ50(PIC18FJ5x):
    pass
class PIC18FJ53(PIC18FJ5x):
    pass

# ===================================================================
class PIC18FK22(PICBB4):
    # C L A S S  for PIC18F2x/4xK22 families
    # 18F13/14K22 : 40001357
    # 18F2x/4xK22 : 40001398
    def __init__(self, mclr=None, icspclk=None, icspdat=None, lsb=True, led=None):
        # see with 'class PICBB4' for requirements of mclr, icspdat/clk and led

        super().__init__(mclr, icspclk, icspdat, lsb, led)

        # Programming timing values in microseconds
        self.Tenth  = 2
        self.Tdly5  = 1000
        self.Tdly5a = 5000
        self.Tdly6  = 200
        self.Tdly7  = 15000

        # programming properties
        self.bulk_erase_word1 = 0x0F0F
        self.bulk_erase_word2 = 0x8F8F          # Tables 3-1 , 3-2 !

    def __eeprom_write_verify(self, addr, data):
        # alternate procedure for this class than in PICBB4
        # print('Programming EEPROM (PIC18FK22 specific)')
        # EEPROM is programmed per byte
        datalist = [data[i] for i in range(len(data))]          # make list of bytes
        self.__command_write(self.CMD_CORE, 0x9EA6)             # clear EECON1.EEPGD
        self.__command_write(self.CMD_CORE, 0x9CA6)             # clear EECON1.CFGS
        for i in range(len(datalist)):
            self.__command_write(self.CMD_CORE, 0x0E00 + ((addr + i) & 0xFF))   # LSB addr
            self.__command_write(self.CMD_CORE, 0x6EA9)
            self.__command_write(self.CMD_CORE, 0x0E00 + (((addr + i) >> 8) & 0xFF))  # MSB of addr
            self.__command_write(self.CMD_CORE, 0x6EAA)
            self.__command_write(self.CMD_CORE, 0x0E00 + (datalist[i] & 0xFF))  # data byte
            self.__command_write(self.CMD_CORE, 0x6EA8)
            self.__command_write(self.CMD_CORE, 0x84A6)         # set EECON1.WREN
            self.__command_write(self.CMD_CORE, 0x82A6)         # set WR bit: initiate write
            self.__command_write(self.CMD_CORE, 0x0000)         # NOP
            self.__transfer_bits_out(4, 0x00)                   # NOP commandbyte
            WRbit = False
            while WRbit is False:
                self.__command_write(self.CMD_CORE, 0x50A6)     # EECON1 -> w
                self.__command_write(self.CMD_CORE, 0x6EF5)     # w -> TABLAT
                self.__command_write(self.CMD_CORE, 0x0000)     # NOP
                self.__command_write(self.CMD_SHIFTOUT_TABLAT)
                eecon1 = self.__transfer_bits_in()              # read EECON1
                WRbit = (eecon1 & 0x02) == 0x00
            sleep_us(self.Tdly6)                                # P10 / Tdly6
            self.__transfer_bits_out(16, 0x00)                  # payload of last NOP command
            self.__command_write(self.CMD_CORE, 0x94A6)         # disable EEPROM writes
            # verify EEPROM byte
            self.__command_write(self.CMD_CORE, 0x80A6)         # enable EEPROM read
            self.__command_write(self.CMD_CORE, 0x50A8)         # EEDATA -> w
            self.__command_write(self.CMD_CORE, 0x6EF5)         # w -> TABLAT
            self.__command_write(self.CMD_CORE, 0x0000)         # NOP
            self.__command_write(self.CMD_SHIFTOUT_TABLAT)      # shift out
            eedata = self.__transfer_bits_in()                  # read EECON1
            # print(f'EEPROM {i} out {datalist[i]:02X} in {eedata:02X}')
            if not eedata == data[i]:
                print(self.fmt_verify.format(addr, data[i], eedata))
                return False
        return True

    def __config_write_verify(self, addr, datalist):
        # alternate procedure for this class than in PICBB4
        # print(f'Programming config words (PIC18FK22 specific)')
        # print(f'{self.cfg_impl=}')
        self.__command_write(self.CMD_CORE, 0x8EA6)             # set EEPGD
        self.__command_write(self.CMD_CORE, 0x8CA6)             # set CFGS
        self.__command_write(self.CMD_CORE, 0x84A6)             # set WREN
        for i in range(len(datalist)):
            cfgword = datalist[i]
            self.__adjust_address(addr + 2 * i)                         # set address
            self.__command_write(self.CMD_TABLE_WRITE_PGM, cfgword)     # LSB
            self.__programming_delay(self.Tdly5a, self.Tdly6)           # delay
            self.__adjust_address(addr + 2 * i + 1 )                    # next addr
            self.__command_write(self.CMD_TABLE_WRITE_PGM, cfgword)     # MSB
            self.__programming_delay(self.Tdly5a, self.Tdly6)
            # read back 2 bytes
            self.__adjust_address(addr + 2 * i)                 # set addr
            self.__command_write(self.CMD_TABLE_READ_INCR)
            lsb = self.__transfer_bits_in()                     # LSB
            self.__command_write(self.CMD_TABLE_READ_INCR)
            msb = self.__transfer_bits_in()                     # MSB
            # verify config bytes (limited to the implemented bits)
            x = (addr + i - self.cfg_start) * 4                 # offset of mask in cfg_impl
            lsb_fix = ( cfgword       & 0xff) & int(self.cfg_impl[x  :x+2], 16)  # anded with impl mask
            msb_fix = ((cfgword >> 8) & 0xff) & int(self.cfg_impl[x+2:x+4], 16)
            if not (lsb == lsb_fix) and (msb == msb_fix):
                # display bytes as words in hex (little endian notation)
                print(self.fmt_verify.format(addr + i*2, msb + lsb*256, msb_fix + lsb_fix*256))
                return False
        return True


# ===================================================================
class PIC18FK4x(PICSPI):
    # C L A S S  for PIC18FxxK4x families
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 250
        self.Terab  = 25200
        self.Tpint  = 5600

class PIC18FK40(PIC18FK4x):     # PgmSpec 40001772 40001822
    pass
class PIC18FK42(PIC18FK4x):     # PgmSpec 40001836 40001886
    pass

# ===================================================================
class PIC18FQ1x(PICSPI):
    # C L A S S  for PIC18F2x/4xQ1x family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 1000
        self.Terab  = 75000
        self.Tpdfm  = 11000
        self.Tpint  = 65

        # deviations from default commands:
        self.CMD_LOAD_DATA     = const(0xC0)
        self.CMD_LOAD_DATA_INC = const(0xE0)

class PIC18FQ10(PIC18FQ1x):      # PgmSpec 40001874
    pass

# ===================================================================
class PIC18FQ2x(PICSPI):
    # C L A S S  for PIC18FxxQ20/24 families
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 1500
        self.Terab  = 11000
        self.Tpdfm  = 11000
        self.Tpint  = 75

        # deviations from default commands:
        self.CMD_LOAD_DATA     = const(0xC0)
        self.CMD_LOAD_DATA_INC = const(0xE0)

        # programming properties
        self.bulk_erase_by_mask = True
        self.cfg_wordsize = 1       # config pgmg per byte

class PIC18FQ20(PIC18FQ2x):         # PgmSpec 40002327
    pass
class PIC18FQ24(PIC18FQ2x):         # PgmSpec 40002414
    pass

# ===================================================================
class PIC18FQ4x(PICSPI):
    # C L A S S  for PIC18FxxQ40/41/43 families
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 1000
        self.Terab  = 11000
        self.Tpdfm  = 11000
        self.Tpint  = 75

        # deviations from default commands:
        self.CMD_LOAD_DATA     = const(0xC0)
        self.CMD_LOAD_DATA_INC = const(0xE0)

        # programming properties
        self.bulk_erase_by_mask = True
        self.cfg_wordsize = 1       # config pgmg per byte

class PIC18FQ40(PIC18FQ4x):         # PgmSpec 40002479
    pass
class PIC18FQ41(PIC18FQ4x):         # PgmSpec 40002185
    pass
class PIC18FQ43(PIC18FQ4x):         # PgmSpec 40002143
    pass

# ===================================================================
class PIC18FQ7x(PICSPI):
    # C L A S S   for PIC18FxxQ7x family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 1500
        self.Terab  = 11000
        self.Tpdfm  = 11000
        self.Tpint  = 75

        # deviations from default commands:
        self.CMD_LOAD_DATA     = const(0xC0)
        self.CMD_LOAD_DATA_INC = const(0xE0)

        # programming properties
        self.bulk_erase_by_mask = True
        self.cfg_wordsize = 1       # config pgmg per byte

class PIC18FQ71(PIC18FQ7x):         # PgmSpec 40002306
    pass

# ===================================================================
class PIC18FQ8x(PICSPI):
    # C L A S S   for PIC18FxxQ83/84 family
    def __init__(self, spi=None, mclr=None, led=None):
        # see with 'class PICSPI' for requirements of spi, mclr and led

        super().__init__(spi, mclr, led)

        # Programming timing values in microseconds
        self.Tenth  = 1000
        self.Terab  = 11000
        self.Tpdfm  = 11000
        self.Tpint  = 75

        # deviations from default commands:
        self.CMD_LOAD_DATA     = const(0xC0)
        self.CMD_LOAD_DATA_INC = const(0xE0)

        # programming properties
        self.bulk_erase_by_mask = True
        self.cfg_wordsize = 1       # config pgmg per byte

class PIC18FQ83(PIC18FQ8x):         # PgmSpec 40002137
    pass
class PIC18FQ84(PIC18FQ8x):         # PgmSpec 40002137
    pass


#
