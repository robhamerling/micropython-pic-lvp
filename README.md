<h1>Micropython scripts for programming Microchip PICs</h1>

<h3>in Low Voltage Programming mode</h3>

<h3>Micropython on PyBoard, ESP32, Raspberry Pico, etc.</h3>

Author: Rob Hamerling. Copyrights (c) 2019..2024. All rights reserved.

Version: 5.1

License: MIT

<h2>Summary</h2>

Micropython library for programming some specific groups of
Microchip midrange PICs with a PyBoard, ESP32 or Raspberry Pico.
Will probably work but has not been tested with other Micropython devices.
Support for other families of PICs might be added in future.
See the examples for currently supported PIC families.



<h2>Class Libraries</h2>

<h3>Classes for specific PIC families</h3>

* **PIC16F145x**     16F145x
* **PIC16F131xx**    16F131xx
* **PIC12F150x**     12F1501
* **PIC16F150x**     16F150x
* **PIC16F152xx**    16F152xx
* **PIC16F153xx**    16F153xx
* **PIC12F157x**     16F1571/2
* **PIC12F161x**     12F1612
* **PIC16F161x**     16F161x
* **PIC16F17xx**     16F17xx
* **PIC16F17xx**     16F17xx
* **PIC12F18xx**     12F182x/4x
* **PIC16F18xx**     16F182x/4x
* **PIC16F180xx**    16F180xx
* **PIC16F181xx**    16F181xx
* **PIC16F184xx**    16F184xx
* **PIC16F188xx**    16F188xx
* **PIC16F191xx**    16F191xx
* **PIC18F2xxx**     18F2xxx
* **PIC18F4xxx**     18F4xxx
* **PIC18FJ5x**      18F2xJ1/5x/4xJ1/5X
* **PIC18FJ10**      18FxxJ10
* **PIC18FJ11**      18FxxJ11
* **PIC18FJ13**      18FxxJ13
* **PIC18FJ50**      18FxxJ50
* **PIC18FJ53**      18FxxJ53
* **PIC18FK22**      18FxxK22
* **PIC18FK4x**      18FxxK4x
* **PIC18FK40**      18FxxK40
* **PIC18FK42**      18FxxK42
* **PIC18FQ1x**      18FxxQ1x
* **PIC18FQ10**      18FxxQ10
* **PIC18FQ2x**      18FxxQ2x
* **PIC18FQ20**      18FxxQ20
* **PIC18FQ24**      18FxxQ24
* **PIC18FQ4x**      18FxxQ4x
* **PIC18FQ40**      18FxxQ40
* **PIC18FQ41**      18FxxQ41
* **PIC18FQ44**      18FxxQ44
* **PIC18FQ7x**      18FxxQ7x
* **PIC18FQ71**      18FxxQ71
* **PIC18FQ8x**      18FxxQ8x
* **PIC18FQ83**      18FxxQ83
* **PIC18FQ84**      18FxxQ84

Note: the 'x' in class names is lower case!


<h3>Parent classes</h3>

**PIC**

    Base class for LVP (Low Voltage Programming) of
    Microchip PICs via PyBoard, ESP32 or Raspberry Pico

**PICBB4**

    Class for LVP of the Microchip midrange families
    using 4-bits programming commands.

**PICBB6**

    Class for LVP of the Microchip midrange families
    using 6-bits programming commands.

**PICSPI**

    Class for LVP of the Microchip midrange families
    using 8-bits programming commands, which can be
    programmed using (hardware) SPI.


<h2>Sample user scripts</h2>

Herewith some examples of Micropython scripts programming
a PIC with a specific hex file.
See further below for the lists of the provided hex files.
These samples can easily be modified to fit your specific
hardware and use your own hex files.

Below a list of example scripts to (re-)programdifferent
PIC types with Raspberry Pico with a blink-a-led program.
Any other Micropython device should work as well!

* lvp_1455_blink.py
* lvp_27k40_blink.py


<h2>Wiring</h2>

The data pin on the PIC-side (ICSPDAT pin) is bidirectional.
When using a bit-banging technique - like for the 16F1455 - one pin is used
on the micropython side (PyBoard, ESP32 or Raspberri Pico).
This pin is switched between output-mode and input-mode by the software.

With SPI - for most other PICs - separate pins are used for output (MOSI)
and input (MISO). This requires a special wiring:

* connect MOSI via a signal diode to ICSPDAT,
* connect MISO directly to ICSPDAT
* connect ICSPDAT with a 1K resistor to ground.

See an example for this wiring (in which PGD is ICSPDAT):

![Schematics](SPI_loopback.png)

Below a Pickit 28-Pin demo board with 16F18857 connected to a PyBoard.
On the PICture the diode and pull down resistor are hidden under the blue PCB!

![test setup with PyBoard](PyBoard_PIC_16f18857.jpg)

The same type of wiring is required when using SoftSPI,
and it requires pin specification of icspdat\_out and icspdat\_in.


<h2>Sample JalV2 sources and hex files</h2>

Some simple blink-a-led programs with JalV2 source and hex file are
provided to check the proper working of the lvp scripts:

*  **16f1455_blink_intosc.jal**
*  **16f1455_blink_intosc.hex**
*  **18f27k40_blink_intosc.jal**
*  **18f27k40_blink_intosc.hex**

**Note:**
The target PIC must have LVP enabled (the LVP bit in configuration words
set to 1, this is default with new PICs) otherwise LVP won't work.
Once LVP is disabled in the PIC it can only be enabled with HVP
(High Voltage Programming), e.g. with a Pickit 3, 4 or a similar programmer.
When the hexfile to be loaded contains LVP DISABLED (the LVP bit cleared)
in the configuration words the bit will be set to 1 to prevent a
verification error: LVP cannot be disabled by LVP!

For more information see also the comments in the scripts.

<h2>Background information</h2>

There are many programming protocols for PICs.
Below some the most major differences for the currently supported PICs:
* 16Fs
```
   * Programming in rows of words for program memory,
   * ID memory, config memory per word, EEPROM per byte
```
* 18Fs
```
   * Programming per word for program memory and ID-memory.
     EEPROM memory is programed per byte.
   * Programming of config memory is PIC specific
   * 18FxxK40: Config memory is programmed per word
   * 18FxxQ4x: Config memory is programmed per byte.
```


<h2>Summary of changes</h2>

```
  version
  up to 1.8:    Dedicated programs for programming and bulk erase,
                resulting in much redundant code.
  2.0:          Splitted program into a class library and program(s)
                Class library contains all functionally, programs
                need only to specify some parameters and call the
                required public functions.
  2.1:          Pin objects have to be specified (was pin numbers),
                is safer and more flexible for different boards.
                All defaults are now 'None'.
                <led> is optional (can be left 'None').
  2.2           Combined lvp_145x.py and lvp_188xx.py class libraries
                to a single library 'lvp_pic16.py' to reduce code redundancy.
                Within this library there are still the old classes, but
                now as subclasses of the lvp_pic16 class, to be imported like:
                    from lvp_pic16 import lvp_188xx
  2.3           Some minor refinements and improved/corrected comments.
  2.4           Some variables moved from class to instance for future
                enhancements.
                Renamed the script to lvp_pic16.py since a version for
                the 18F PICs is available (lvp_pic18).
  2.5           -
  2.6           Made spi=1 as default for SPI classes.
                -------------------------------------------------------
  3.0           Splitted classes in separate files for memory-optimisation
                and future enhancements.
  3.1           Restructured class/file organization.
  3.2           Several small corrections
                Continued improvement of structure for flexibility,
                reduction of redundant code, memory optimization.
                Merge of SPI-code of lvp_spi16 and lvp_spi18.
  3.3           Fixed bug with writing EEPROM of 18Fs (a word per byte!).
                The 18F family is called 'advanced' PIC by Microchip.
                The newer midrange PICs (mainly 16F) are called 'enhanced'.
  3.4           Some minor (mostly cosmetic) changes.
  3.5           Added support for PIC16F18xxQ43 family.
                Removed row-erase with program memory: no need after bulk erase.
  3.6           Added support for PIC16F180xx family
                Several minor code and comment improvements.
  3.7           Added variable 'family' to facilitate methods of
                inheritated classes behave selectively.
                Adapted some parent classes to fit class 18FxxQ43 in
                the original class structure.
                --------------------------------------------------------
  4.0           All classes combined into a single file.
                Naming of classes simplified (removed 'lvp_' prefixes)
                Applications should use 'from lvp_pic import <class>'
                <class> is one of pic180xx, pic188xx, pic145x, pick40, picq43.
                for example:
                    from lvp_pic import pic180xx
  4.1           Added support for 18FxxQ83/84 families (class picq8x).
                Added support and examples for Raspberry Pico (RP2040).
                Code change: programming timings now specified per family.
                Some minor code cleanups.
  4.2           Corrected some read/write timings
                Added 18FxxQ40/41 families (renamed class picq43 to picq4x).
                Added 16F152xx, 16F153xx and 16F181xx families.
                Some code changes:
                    - combined individual bulk_erase code.
                    - default rowsize 32 words, maybe overwritten
                      for individual PICs.
  4.3           Added 18FxxQ71 family.
                Most class names changed and all now in capitals
                (except for 'variable' x) and example scripts adapted
                accordingly. Thus imports must be changed, e.g.:
                    from lvp_pic import PIC16F180xx
  4.4           Fixed a major bug related to class hierarchy.
                Fixed some other minor bugs.
                Added 12F182x and 16F184x families
                --------------------------------------------------------
  5.0           Changes in class structure and of class names.
                Added following PIC families
                - 18F2XJ5x/18f4Xj5x
                - 16F131xx, 16F171xx, 16F191xx
                - 18FxxQ10, 18FxxQ20, 18FxxQ24
                - 18F2x/4xK22
                Added external script 'devid2pictype.py'
                to determine the pictype (name) with its devid.
                Added external script 'picpgmcompactinfo.py'
                with some pictype specific attribute values.
                Removed builtin SPI hardware settings. With
                PICs using SPI-compatible protocol the user
                must specify an SPI or SoftSPI object, which is
                appropriately (re-)initialised by the libary.
                Most samples are removed. Two blink-a-led PIC
                programs and two Micropython scripts are left.
                These scripts are for the Raspberry Pi PICO,
                but should run on any device with Micropython.
  5.1           File picpgmcompactinfo.py not imported
                anymore but read when needed.
                File devid2pictype.py not used anymore,
                info is obtained from picpgmcompactinfo.py



#
