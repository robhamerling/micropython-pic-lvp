#!/usr/bin/env python3
"""
Title: Create file with PIC programming attributes

Author: Rob Hamerling, Copyright (c) 2024..2024. All rights reserved.

Description:
     - collect programming attributes from MPLABX XML files as
       selected with the generation of Jallib device files
     - build a class per common Program Specifications
       with all attributes and default value of its PICs
     - build a sub-class per PIC with attributes which
       are unique for this PIC like 'devid', or of which the
       value deviates from the pgmspec class (default) value
     - Only attributes required by the 'lvp_pic' script are
       collected: selection by programming specification
       and attribute name (as used in picpgmdataclass.py)
     - Some attributes are used pgmspec wide, regardless if
       a specific pic has a different value (esp. timings!)

"""

user_guide = ("""
# User guide:
# The pic programming information file can be loaded as follows:
#     import picpgmcompactinfo
# This will import a class for every PIC.
# Obtain access to the attributes of a target PIC with e.g.:
#     target = picpgmcompact.PIC16F18857
# When pictype is a variable like "16F18857" you may need to specify:
#     target = eval("picpgmcompactinfo.PIC" + pictype)
# The programming attributes of the target are then available
# by name like for example:
#     devid = target.devid
#     code_end = target.pgm_end
# or with:
#     devid = getattr(target, "devid")
#     code_end = getattr(target, "pgm_end")


""")

"""
    Notes
        1. This script uses fuctions of the 'picpgmdataclass' script
        2. Only the attributes which are absolutely needed
           by the LVP_PIC script are collected!
        3. For PICs without DeviceID a dummy value 0x0000 is provided
        4. Rowsize is expressed in words for all PIC types
        5. When an attribute has no value (might be an error in MPLABX):
           Rowsize (pgm_rowsize) value will be 1


"""

import os
import time
from shutil import copy2
import xml.etree.ElementTree as et
from concurrent import futures

import picpgmdataclass              # module with required basic functions
import pgmspec_family               # dictionary {pgmspec : PIC-family}

mplabxversion = "6.20"                          # current MPLABX version
base = os.path.join('/', 'media', 'rob', 'XSA', 'picdevices.'+mplabxversion)  # MPLABX extracted data

# Selection of PgmSpecs (of PICs supported by the LVP script)
pgmspec_selection = (30009622,
                     30009687,
                     40001357,
                     40001390,
                     40001398,
                     40001439,
                     40001457,
                     40001573,
                     40001620,
                     40001683,
                     40001713,
                     40001720,
                     40001753,
                     40001754,
                     40001766,
                     40001772,
                     40001792,
                     40001822,
                     40001836,
                     40001838,
                     40001874,
                     40001880,
                     40001886,
                     40001970,
                     40002079,
                     40002137,
                     40002143,
                     40002149,
                     40002185,
                     40002266,
                     40002276,
                     40002306,
                     40002317,
                     40002327,
                     40002414,
                     40002500,
                     )


def build_picpgmcompact(picfmtdict, picpgmcompactinfo):
    """ build picpgmcompact.py with a class for each PgmSpec
        and a subclass for each of the PICs in this PgmSpec.
    """
    # selection of attributes required by the LVP-script and
    # table to translate names in picpgmdataclass to names used by LVP script
    attrib_selection = {'devid'                        : 'devid',
                    #   'CodeSector_beginaddr'         : 'pgm_start',       # fixed 0x0!
                        'CodeSector_endaddr'           : 'pgm_end',
                        'UserIDSector_beginaddr'       : 'id_start',
                        'UserIDSector_endaddr'         : 'id_end',
                        'ConfigFuseSector_beginaddr'   : 'cfg_start',
                        'ConfigFuseSector_endaddr'     : 'cfg_end',
                        'WORMHoleSector_beginaddr'     : 'cfg_start',
                        'WORMHoleSector_endaddr'       : 'cfg_end',
                        'EEDataSector_beginaddr'       : 'eeprom_start',
                        'EEDataSector_endaddr'         : 'eeprom_end',
                    #   'RevisionIDSector_beginaddr'   : 'revid_start',     # fixed 0x8005 / 0x3FFFFC
                    #   'DeviceIDSector_beginaddr'     : 'devid_start',     # fixed 0x8006 / 0x3FFFFE
                        'Latches_pgm'                  : 'pgm_rowsize',     # in words!
                        'ProgrammingWaitTime_erase'    : 'Terab',
                        'ProgrammingWaitTime_pgm'      : 'Tpint',
                        'ProgrammingWaitTime_cfg'      : 'Tpdfm',
                        'fuses_impl'                   : 'cfg_impl',
                        'lvpbit_mask'                  : 'lvpbit_mask',
                        'lvpbit_offset'                : 'lvpbit_offset',
                        }

    # dictionary with corrections for specific pgmspecs and attributes
    # mostly because the values in MPLABX files differ from those in the PgmSpec
    parmcorrections = {40002317 : {'Terab' : '13000',
                                   'Tpint' : '5600'},
                       40002500 : {'Terab' : '20000',
                                   'Tpint' : '12000'},
                      }


    pgmspec2picname = picpgmdataclass.invert_dict(picfmtdict)       # inverted dictionary: {pgmspec : [picname,]}

    with open(picpgmcompactinfo, 'w') as fp:
        fp.write('#\n'
                 '# PIC programming attributes\n'
                 '#  - most attribute values are presented as numeric value\n'
                 '#  - times are in microseconds\n'
                 '#  - voltages are in millivolts\n'
                 '#  - rowsize_pgm is number of words\n\n'
                f'# Data collected from MPLABX v{mplabxversion} dd {time.asctime()}\n')
        fp.write(user_guide)                                        # for text: see top of script
        for pgmspec in pgmspec_selection:                           # selected PgmSpecs
            # collect attributes of all PICs in this PgmSpec
            attrdict = {}                                           # {attr : [list of PICs]}
            for pic in sorted(pgmspec2picname[str(pgmspec)]):
                for attr in picfmtdict[pic]:
                    attrdict.setdefault(attr, []).append(pic)       # list PICs with this attribute
            # create class for the PgmSpec with keywords for all PICs of the PgmSpec
            fp.write(f'class PS{pgmspec}:       # family: {pgmspec_family.pgmspec_family[pgmspec]}\n')
            pgmspecdefault = {}                                     # {key:value} defaults
            for attr in attrib_selection:                           # selected attributes
                if attr == 'devid':                                 # only in pic class
                   continue
                if attr not in attrdict:                            # not in xml file
                    # print(f'{pic} : attribute {attr} not in attrdict')
                    continue
                # select most frequently used value with this pgmspec
                val = picpgmdataclass.mostfrequent(attr, attrdict[attr], picfmtdict)
                if val is None:
                    val = '0'
                elif attr == 'Latches_pgm':
                    if pic.startswith('18') and (int(val) > 1):
                        # print(f'rowsize in words: {pgmspec} {pic[0:2]}Fs {attr}')
                        val = f'{int(val) // 2}'                    # rowsize_pgm always in words
                xlate = attrib_selection.get(attr)                  # picpgmcompactinfo name
                # some possible corrections (of errors in MPLABX)
                if pgmspec in parmcorrections:
                    for parmkey, parmvalue in parmcorrections[pgmspec].items():
                        if xlate == parmkey:
                            print(f'Corr: {pgmspec} {xlate} {parmvalue} (was {val})')
                            val = parmvalue
                # fp.write(f'    {xlate} = {val}\n')
                if picpgmdataclass.isnumeric(val):
                    fp.write(f'    {xlate} = {val}\n')
                else:
                    fp.write(f'    {xlate} = "{val}"\n')
                pgmspecdefault[attr] = val                          # to dict with pgmspec setti
            fp.write('\n')
            # create class for every PIC of this PgmSpec
            for pic in sorted(pgmspec2picname[str(pgmspec)]):       # all PICs of this pgmspec
                fp.write(f'class PIC{pic}(PS{pgmspec}):\n')         # inherit pgmspec class
                # fp.write(f'    name = "{pic}"\n')                   # specify PIC type
                if not 'devid' in attrdict:                         # no devid in attributes
                    fp.write(f'    devid = 0x0000\n' )              # add dummy
                for attr in sorted(attrib_selection):
                    xlate = attrib_selection.get(attr)              # picpgmcompactinfo name
                    if attr == 'devid':                             # devid: with every PIC
                        fp.write(f'    {xlate} = {picfmtdict[pic].get(attr, 0x0000)}\n')
                    elif attr not in attrdict:                           # not in xml file
                        # print(f'{pic} {pgmspec} : attribute {attr} not in attrdict')
                        continue
                    elif xlate in ('Terab', 'Tpint'):               # pgmspec level parms
                        continue                                    # not in PIC classes
                    else:
                        val = picfmtdict[pic].get(attr, '0')
                        if attr == 'Latches_pgm':                   # might need bytes -> words
                            if pic.startswith('18') and (int(val) > 1):
                                # print(f'{pic} rowsize in words: {pgmspec} {pic[0:2]}Fs {attr}')
                                val = str(int(val) // 2)            # rowsize_pgm in words
                        if not val == pgmspecdefault[attr]:         # different from pgmspec
                            if picpgmdataclass.isnumeric(val):
                                fp.write(f'    {xlate} = {val}\n')
                            else:
                                fp.write(f'    {xlate} = "{val}"\n')
                fp.write('\n')
            fp.write('\n')


# ================ mainline =======================

if __name__ == "__main__":
    print(f'Collecting attributes for LVP PIC programming script ...')

    picdir = os.path.join(base, 'mplabx')       # extracted PIC XML files
    if not os.path.exists(picdir):              # directory not found
        print(f'Could not find {picdir}')
        exit(1)
    if not len(os.listdir(picdir)):             # (only xml files expected)
        print(f'No files found in {picdir}')
        exit(1)

    # using functions of picpgmdataclass script:
    picpgmdict = picpgmdataclass.collect_pgminfo(picdir)
    picfmtdict = picpgmdataclass.build_format_dict(picpgmdict)


    picpgmcompactfile = 'picpgmcompactinfo.py'
    print(f'Building compact programming attribute version: {picpgmcompactfile} ...')
    build_picpgmcompact(picfmtdict, picpgmcompactfile)

    print(f'Copying {picpgmcompactfile} to ~/upy/lvp directory')
    copy2(picpgmcompactfile, os.path.join('..', '..', 'upy', 'lvp'))

#
