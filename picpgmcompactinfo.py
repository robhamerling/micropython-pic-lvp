#
# PIC programming attributes
#  - most attribute values are presented as numeric value
#  - times are in microseconds
#  - voltages are in millivolts
#  - rowsize_pgm is number of words

# Data collected from MPLABX v6.20 dd Mon Nov 11 21:08:54 2024

# User guide:
# The pic programming information file can be loaded as follows:
#     import picpgmcompactinfo
# This will import a class for every PIC.
# Obtain access to the attributes of a target PIC with e.g.:
#     target = picpgmcompact.PIC16F18857
# When pictype is a variable like "16F18857" you may need to specify:
#     target = eval("picpgmcompactinfo.PIC" + pictype)
# The programming attributes of the target are then available
# by name like for example:
#     devid = target.devid
#     code_end = target.pgm_end
# or with:
#     devid = getattr(target, "devid")
#     code_end = getattr(target, "pgm_end")


class PS30009622:       # family: PIC18F2XXX/4XXX
    pgm_end = 0x8000
    id_start = 0x200000
    id_end = 0x200008
    cfg_start = 0x300000
    cfg_end = 0x30000e
    eeprom_start = 0xf00000
    eeprom_end = 0xf00100
    pgm_rowsize = 16
    Terab = 10000
    Tpint = 1000
    Tpdfm = 5000
    cfg_impl = "00CF1F1F0087C5000FC00FE00F40"
    lvpbit_mask = 0x0004
    lvpbit_offset = 6

class PIC18F2221(PS30009622):
    pgm_end = 0x1000
    pgm_rowsize = 4
    devid = 0x2160
    cfg_impl = "00CF1F1F0087FD0003C003E00340"

class PIC18F2321(PS30009622):
    pgm_end = 0x2000
    pgm_rowsize = 4
    devid = 0x2120
    cfg_impl = "00CF1F1F0087FD0003C003E00340"

class PIC18F2410(PS30009622):
    pgm_end = 0x4000
    eeprom_start = 0
    eeprom_end = 0
    devid = 0x1160

class PIC18F2420(PS30009622):
    pgm_end = 0x4000
    devid = 0x1140
    cfg_impl = "00CF1F1F0087C50003C003E00340"

class PIC18F2423(PS30009622):
    pgm_end = 0x4000
    devid = 0x1150
    cfg_impl = "00CF1F1F0087C50003C003E00340"

class PIC18F2450(PS30009622):
    pgm_end = 0x4000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 8
    devid = 0x2420
    cfg_impl = "3FCF3F1F0086ED00034003600340"

class PIC18F2455(PS30009622):
    pgm_end = 0x6000
    devid = 0x1260
    cfg_impl = "3FCF3F1F0087E50007C007E00740"

class PIC18F2458(PS30009622):
    pgm_end = 0x6000
    devid = 0x2A60
    cfg_impl = "3FCF3F1F0087E50007C007E00740"

class PIC18F2480(PS30009622):
    pgm_end = 0x4000
    devid = 0x1AE0
    cfg_impl = "00CF1F1F0086D50003C003E00340"

class PIC18F2510(PS30009622):
    eeprom_start = 0
    eeprom_end = 0
    devid = 0x1120

class PIC18F2515(PS30009622):
    pgm_end = 0xc000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 32
    devid = 0x0CE0

class PIC18F2520(PS30009622):
    devid = 0x1100

class PIC18F2523(PS30009622):
    devid = 0x1110

class PIC18F2525(PS30009622):
    pgm_end = 0xc000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0CC0
    cfg_impl = "00CF1F1F0087C50007C007E00740"

class PIC18F2550(PS30009622):
    devid = 0x1240
    cfg_impl = "3FCF3F1F0087E5000FC00FE00F40"

class PIC18F2553(PS30009622):
    devid = 0x2A40
    cfg_impl = "3FCF3F1F0087E5000FC00FE00F40"

class PIC18F2580(PS30009622):
    devid = 0x1AC0
    cfg_impl = "00CF1F1F0086D5000FC00FE00F40"

class PIC18F2585(PS30009622):
    pgm_end = 0xc000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0EE0
    cfg_impl = "00CF1F1F0086F5000FC00FE00F40"

class PIC18F2610(PS30009622):
    pgm_end = 0x10000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 32
    devid = 0x0CA0

class PIC18F2620(PS30009622):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0C80

class PIC18F2680(PS30009622):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0EC0
    cfg_impl = "00CF1F1F0086F5000FC00FE00F40"

class PIC18F2682(PS30009622):
    pgm_end = 0x14000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x2700
    cfg_impl = "00CF1F1F0086F5003FC03FE03F40"

class PIC18F2685(PS30009622):
    pgm_end = 0x18000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x2720
    cfg_impl = "00CF1F1F0086F5003FC03FE03F40"

class PIC18F4221(PS30009622):
    pgm_end = 0x1000
    pgm_rowsize = 4
    devid = 0x2140
    cfg_impl = "00CF1F1F0087FD0003C003E00340"

class PIC18F4321(PS30009622):
    pgm_end = 0x2000
    pgm_rowsize = 4
    devid = 0x2100
    cfg_impl = "00CF1F1F0087FD0003C003E00340"

class PIC18F4410(PS30009622):
    pgm_end = 0x4000
    eeprom_start = 0
    eeprom_end = 0
    devid = 0x10E0

class PIC18F4420(PS30009622):
    pgm_end = 0x4000
    devid = 0x10C0
    cfg_impl = "00CF1F1F0087C50003C003E00340"

class PIC18F4423(PS30009622):
    pgm_end = 0x4000
    devid = 0x10D0
    cfg_impl = "00CF1F1F0087C50003C003E00340"

class PIC18F4450(PS30009622):
    pgm_end = 0x4000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 8
    devid = 0x2400
    cfg_impl = "3FCF3F1F0086ED00034003600340"

class PIC18F4455(PS30009622):
    pgm_end = 0x6000
    devid = 0x1220
    cfg_impl = "3FCF3F1F0087E50007C007E00740"

class PIC18F4458(PS30009622):
    pgm_end = 0x6000
    devid = 0x2A20
    cfg_impl = "3FCF3F1F0087E50007C007E00740"

class PIC18F4480(PS30009622):
    pgm_end = 0x4000
    devid = 0x1AA0
    cfg_impl = "00CF1F1F0086D50003C003E00340"

class PIC18F4510(PS30009622):
    eeprom_start = 0
    eeprom_end = 0
    devid = 0x10A0

class PIC18F4515(PS30009622):
    pgm_end = 0xc000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 32
    devid = 0x0C60

class PIC18F4520(PS30009622):
    devid = 0x1080

class PIC18F4523(PS30009622):
    devid = 0x1090

class PIC18F4525(PS30009622):
    pgm_end = 0xc000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0C40
    cfg_impl = "00CF1F1F0087C50007C007E00740"

class PIC18F4550(PS30009622):
    devid = 0x1200
    cfg_impl = "3FCF3F1F0087E5000FC00FE00F40"

class PIC18F4553(PS30009622):
    devid = 0x2A00
    cfg_impl = "3FCF3F1F0087E5000FC00FE00F40"

class PIC18F4580(PS30009622):
    devid = 0x1A80
    cfg_impl = "00CF1F1F0086D5000FC00FE00F40"

class PIC18F4585(PS30009622):
    pgm_end = 0xc000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0EA0
    cfg_impl = "00CF1F1F0086F5000FC00FE00F40"

class PIC18F4610(PS30009622):
    pgm_end = 0x10000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 32
    devid = 0x0C20

class PIC18F4620(PS30009622):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0C00

class PIC18F4680(PS30009622):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0E80
    cfg_impl = "00CF1F1F0086F5000FC00FE00F40"

class PIC18F4682(PS30009622):
    pgm_end = 0x14000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x2740
    cfg_impl = "00CF1F1F0086F5003FC03FE03F40"

class PIC18F4685(PS30009622):
    pgm_end = 0x18000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x2760
    cfg_impl = "00CF1F1F0086F5003FC03FE03F40"

class PIC18LF2221(PS30009622):
    pgm_end = 0x1000
    pgm_rowsize = 4
    devid = 0x2160
    cfg_impl = "00CF1F1F0087FD0003C003E00340"

class PIC18LF2321(PS30009622):
    pgm_end = 0x2000
    pgm_rowsize = 4
    devid = 0x2120
    cfg_impl = "00CF1F1F0087FD0003C003E00340"

class PIC18LF2410(PS30009622):
    pgm_end = 0x4000
    eeprom_start = 0
    eeprom_end = 0
    devid = 0x1160

class PIC18LF2420(PS30009622):
    pgm_end = 0x4000
    devid = 0x1140
    cfg_impl = "00CF1F1F0087C50003C003E00340"

class PIC18LF2423(PS30009622):
    pgm_end = 0x4000
    devid = 0x1150
    cfg_impl = "00CF1F1F0087C50003C003E00340"

class PIC18LF2450(PS30009622):
    pgm_end = 0x4000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 8
    devid = 0x2420
    cfg_impl = "3FCF3F1F0086ED00034003600340"

class PIC18LF2455(PS30009622):
    pgm_end = 0x6000
    devid = 0x1260
    cfg_impl = "3FCF3F1F0087E50007C007E00740"

class PIC18LF2458(PS30009622):
    pgm_end = 0x6000
    devid = 0x2A60
    cfg_impl = "3FCF3F1F0087E50007C007E00740"

class PIC18LF2480(PS30009622):
    pgm_end = 0x4000
    devid = 0x1AE0
    cfg_impl = "00CF1F1F0086D50003C003E00340"

class PIC18LF2510(PS30009622):
    eeprom_start = 0
    eeprom_end = 0
    devid = 0x1120

class PIC18LF2515(PS30009622):
    pgm_end = 0xc000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 32
    devid = 0x0CE0

class PIC18LF2520(PS30009622):
    devid = 0x1100

class PIC18LF2523(PS30009622):
    devid = 0x1110

class PIC18LF2525(PS30009622):
    pgm_end = 0xc000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0CC0
    cfg_impl = "00CF1F1F0087C50007C007E00740"

class PIC18LF2550(PS30009622):
    devid = 0x1240
    cfg_impl = "3FCF3F1F0087E5000FC00FE00F40"

class PIC18LF2553(PS30009622):
    devid = 0x2A40
    cfg_impl = "3FCF3F1F0087E5000FC00FE00F40"

class PIC18LF2580(PS30009622):
    devid = 0x1AC0
    cfg_impl = "00CF1F1F0086D5000FC00FE00F40"

class PIC18LF2585(PS30009622):
    pgm_end = 0xc000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0EE0
    cfg_impl = "00CF1F1F0086F5000FC00FE00F40"

class PIC18LF2610(PS30009622):
    pgm_end = 0x10000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 32
    devid = 0x0CA0

class PIC18LF2620(PS30009622):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0C80

class PIC18LF2680(PS30009622):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0EC0
    cfg_impl = "00CF1F1F0086F5000FC00FE00F40"

class PIC18LF2682(PS30009622):
    pgm_end = 0x14000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x2700
    cfg_impl = "00CF1F1F0086F5003FC03FE03F40"

class PIC18LF2685(PS30009622):
    pgm_end = 0x18000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x2720
    cfg_impl = "00CF1F1F0086F5003FC03FE03F40"

class PIC18LF4221(PS30009622):
    pgm_end = 0x1000
    pgm_rowsize = 4
    devid = 0x2140
    cfg_impl = "00CF1F1F0087FD0003C003E00340"

class PIC18LF4321(PS30009622):
    pgm_end = 0x2000
    pgm_rowsize = 4
    devid = 0x2100
    cfg_impl = "00CF1F1F0087FD0003C003E00340"

class PIC18LF4410(PS30009622):
    pgm_end = 0x4000
    eeprom_start = 0
    eeprom_end = 0
    devid = 0x10E0

class PIC18LF4420(PS30009622):
    pgm_end = 0x4000
    devid = 0x10C0
    cfg_impl = "00CF1F1F0087C50003C003E00340"

class PIC18LF4423(PS30009622):
    pgm_end = 0x4000
    devid = 0x10D0
    cfg_impl = "00CF1F1F0087C50003C003E00340"

class PIC18LF4450(PS30009622):
    pgm_end = 0x4000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 8
    devid = 0x2400
    cfg_impl = "3FCF3F1F0086ED00034003600340"

class PIC18LF4455(PS30009622):
    pgm_end = 0x6000
    devid = 0x1220
    cfg_impl = "3FCF3F1F0087E50007C007E00740"

class PIC18LF4458(PS30009622):
    pgm_end = 0x6000
    devid = 0x2A20
    cfg_impl = "3FCF3F1F0087E50007C007E00740"

class PIC18LF4480(PS30009622):
    pgm_end = 0x4000
    devid = 0x1AA0
    cfg_impl = "00CF1F1F0086D50003C003E00340"

class PIC18LF4510(PS30009622):
    eeprom_start = 0
    eeprom_end = 0
    devid = 0x10A0

class PIC18LF4515(PS30009622):
    pgm_end = 0xc000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 32
    devid = 0x0C60

class PIC18LF4520(PS30009622):
    devid = 0x1080

class PIC18LF4523(PS30009622):
    devid = 0x1090

class PIC18LF4525(PS30009622):
    pgm_end = 0xc000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0C40
    cfg_impl = "00CF1F1F0087C50007C007E00740"

class PIC18LF4550(PS30009622):
    devid = 0x1200
    cfg_impl = "3FCF3F1F0087E5000FC00FE00F40"

class PIC18LF4553(PS30009622):
    devid = 0x2A00
    cfg_impl = "3FCF3F1F0087E5000FC00FE00F40"

class PIC18LF4580(PS30009622):
    devid = 0x1A80
    cfg_impl = "00CF1F1F0086D5000FC00FE00F40"

class PIC18LF4585(PS30009622):
    pgm_end = 0xc000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0EA0
    cfg_impl = "00CF1F1F0086F5000FC00FE00F40"

class PIC18LF4610(PS30009622):
    pgm_end = 0x10000
    eeprom_start = 0
    eeprom_end = 0
    pgm_rowsize = 32
    devid = 0x0C20

class PIC18LF4620(PS30009622):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0C00

class PIC18LF4680(PS30009622):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x0E80
    cfg_impl = "00CF1F1F0086F5000FC00FE00F40"

class PIC18LF4682(PS30009622):
    pgm_end = 0x14000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x2740
    cfg_impl = "00CF1F1F0086F5003FC03FE03F40"

class PIC18LF4685(PS30009622):
    pgm_end = 0x18000
    eeprom_end = 0xf00400
    pgm_rowsize = 32
    devid = 0x2760
    cfg_impl = "00CF1F1F0086F5003FC03FE03F40"


class PS30009687:       # family: PIC18F2XJXX/4XJXX
    pgm_end = 0xfff8
    cfg_start = 0xfff8
    cfg_end = 0x10000
    pgm_rowsize = 32
    Terab = 600
    Tpint = 1000
    Tpdfm = 5000
    cfg_impl = "EF0FDF0FFFF9FF01"

class PIC18F24J10(PS30009687):
    pgm_end = 0x3ff8
    Tpdfm = 10000
    cfg_start = 0x3ff8
    cfg_end = 0x3ffe
    devid = 0x1D00
    cfg_impl = "E1FCC7FF00F1"

class PIC18F24J11(PS30009687):
    pgm_end = 0x3ff8
    cfg_start = 0x3ff8
    cfg_end = 0x4000
    devid = 0x4D80
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18F24J50(PS30009687):
    pgm_end = 0x3ff8
    cfg_start = 0x3ff8
    cfg_end = 0x4000
    devid = 0x4C00

class PIC18F25J10(PS30009687):
    pgm_end = 0x7ff8
    Tpdfm = 10000
    cfg_start = 0x7ff8
    cfg_end = 0x7ffe
    devid = 0x1C00
    cfg_impl = "E1FCC7FF00F1"

class PIC18F25J11(PS30009687):
    pgm_end = 0x7ff8
    cfg_start = 0x7ff8
    cfg_end = 0x8000
    devid = 0x4DA0
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18F25J50(PS30009687):
    pgm_end = 0x7ff8
    cfg_start = 0x7ff8
    cfg_end = 0x8000
    devid = 0x4C20

class PIC18F26J11(PS30009687):
    devid = 0x4DC0
    cfg_impl = "E104DF0FFFF9FF01"

class PIC18F26J13(PS30009687):
    devid = 0x5920
    cfg_impl = "FF0CFF0FFF0FFF03"

class PIC18F26J50(PS30009687):
    devid = 0x4C40

class PIC18F26J53(PS30009687):
    devid = 0x5820
    cfg_impl = "FF0FFF0FFF0FFF0B"

class PIC18F27J13(PS30009687):
    pgm_end = 0x1fff8
    cfg_start = 0x1fff8
    cfg_end = 0x20000
    devid = 0x5960
    cfg_impl = "FF0CFF0FFF0FFF03"

class PIC18F27J53(PS30009687):
    pgm_end = 0x1fff8
    cfg_start = 0x1fff8
    cfg_end = 0x20000
    devid = 0x5860
    cfg_impl = "FF0FFF0FFF0FFF0B"

class PIC18F44J10(PS30009687):
    pgm_end = 0x3ff8
    Tpdfm = 10000
    cfg_start = 0x3ff8
    cfg_end = 0x3ffe
    devid = 0x1D20
    cfg_impl = "E1FCC7FF00F1"

class PIC18F44J11(PS30009687):
    pgm_end = 0x3ff8
    cfg_start = 0x3ff8
    cfg_end = 0x4000
    devid = 0x4DE0
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18F44J50(PS30009687):
    pgm_end = 0x3ff8
    cfg_start = 0x3ff8
    cfg_end = 0x4000
    devid = 0x4C60

class PIC18F45J10(PS30009687):
    pgm_end = 0x7ff8
    Tpdfm = 10000
    cfg_start = 0x7ff8
    cfg_end = 0x7ffe
    devid = 0x1C20
    cfg_impl = "E1FCC7FF00F1"

class PIC18F45J11(PS30009687):
    pgm_end = 0x7ff8
    cfg_start = 0x7ff8
    cfg_end = 0x8000
    devid = 0x4E00
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18F45J50(PS30009687):
    pgm_end = 0x7ff8
    cfg_start = 0x7ff8
    cfg_end = 0x8000
    devid = 0x4C80

class PIC18F46J11(PS30009687):
    devid = 0x4E20
    cfg_impl = "E104DF0FFFF9FF01"

class PIC18F46J13(PS30009687):
    devid = 0x59A0
    cfg_impl = "FF0CFF0FFF0FFF03"

class PIC18F46J50(PS30009687):
    devid = 0x4CA0

class PIC18F46J53(PS30009687):
    devid = 0x58A0
    cfg_impl = "FF0FFF0FFF0FFF0B"

class PIC18F47J13(PS30009687):
    pgm_end = 0x1fff8
    cfg_start = 0x1fff8
    cfg_end = 0x20000
    devid = 0x59E0
    cfg_impl = "FF0CFF0FFF0FFF03"

class PIC18F47J53(PS30009687):
    pgm_end = 0x1fff8
    cfg_start = 0x1fff8
    cfg_end = 0x20000
    devid = 0x58E0
    cfg_impl = "FF0FFF0FFF0FFF0B"

class PIC18LF24J10(PS30009687):
    pgm_end = 0x3ff8
    Tpdfm = 10000
    cfg_start = 0x3ff8
    cfg_end = 0x3ffe
    devid = 0x1D40
    cfg_impl = "E1FCC7FF00F1"

class PIC18LF24J11(PS30009687):
    pgm_end = 0x3ff8
    cfg_start = 0x3ff8
    cfg_end = 0x4000
    devid = 0x4E40
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18LF24J50(PS30009687):
    pgm_end = 0x3ff8
    cfg_start = 0x3ff8
    cfg_end = 0x4000
    devid = 0x4CC0

class PIC18LF25J10(PS30009687):
    pgm_end = 0x7ff8
    Tpdfm = 10000
    cfg_start = 0x7ff8
    cfg_end = 0x7ffe
    devid = 0x1C40
    cfg_impl = "E1FCC7FF00F1"

class PIC18LF25J11(PS30009687):
    pgm_end = 0x7ff8
    cfg_start = 0x7ff8
    cfg_end = 0x8000
    devid = 0x4E60
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18LF25J50(PS30009687):
    pgm_end = 0x7ff8
    cfg_start = 0x7ff8
    cfg_end = 0x8000
    devid = 0x4CE0

class PIC18LF26J11(PS30009687):
    devid = 0x4E80
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18LF26J13(PS30009687):
    devid = 0x5B20
    cfg_impl = "FF0CFF0FFF0FFF03"

class PIC18LF26J50(PS30009687):
    devid = 0x4D00

class PIC18LF26J53(PS30009687):
    devid = 0x5A20
    cfg_impl = "FF0FFF0FFF0FFF0B"

class PIC18LF27J13(PS30009687):
    pgm_end = 0x1fff8
    cfg_start = 0x1fff8
    cfg_end = 0x20000
    devid = 0x5B60
    cfg_impl = "FF0CFF0FFF0FFF03"

class PIC18LF27J53(PS30009687):
    pgm_end = 0x1fff8
    cfg_start = 0x1fff8
    cfg_end = 0x20000
    devid = 0x5A60
    cfg_impl = "FF0FFF0FFF0FFF0B"

class PIC18LF44J10(PS30009687):
    pgm_end = 0x3ff8
    Tpdfm = 10000
    cfg_start = 0x3ff8
    cfg_end = 0x3ffe
    devid = 0x1D60
    cfg_impl = "E1FCC7FF00F1"

class PIC18LF44J11(PS30009687):
    pgm_end = 0x3ff8
    cfg_start = 0x3ff8
    cfg_end = 0x4000
    devid = 0x4EA0
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18LF44J50(PS30009687):
    pgm_end = 0x3ff8
    cfg_start = 0x3ff8
    cfg_end = 0x4000
    devid = 0x4D20

class PIC18LF45J10(PS30009687):
    pgm_end = 0x7ff8
    Tpdfm = 10000
    cfg_start = 0x7ff8
    cfg_end = 0x7ffe
    devid = 0x1C60
    cfg_impl = "E1FCC7FF00F1"

class PIC18LF45J11(PS30009687):
    pgm_end = 0x7ff8
    cfg_start = 0x7ff8
    cfg_end = 0x8000
    devid = 0x4EC0
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18LF45J50(PS30009687):
    pgm_end = 0x7ff8
    cfg_start = 0x7ff8
    cfg_end = 0x8000
    devid = 0x4D40

class PIC18LF46J11(PS30009687):
    devid = 0x4EE0
    cfg_impl = "E10CDF0FFFF9FF01"

class PIC18LF46J13(PS30009687):
    devid = 0x5BA0
    cfg_impl = "FF0CFF0FFF0FFF03"

class PIC18LF46J50(PS30009687):
    devid = 0x4D60

class PIC18LF46J53(PS30009687):
    devid = 0x5AA0
    cfg_impl = "FF0FFF0FFF0FFF0B"

class PIC18LF47J13(PS30009687):
    pgm_end = 0x1fff8
    cfg_start = 0x1fff8
    cfg_end = 0x20000
    devid = 0x5BE0
    cfg_impl = "FF0CFF0FFF0FFF03"

class PIC18LF47J53(PS30009687):
    pgm_end = 0x1fff8
    cfg_start = 0x1fff8
    cfg_end = 0x20000
    devid = 0x5AE0
    cfg_impl = "FF0FFF0FFF0FFF0B"


class PS40001357:       # family: PIC18F1XK22/LF1XK22
    pgm_end = 0x2000
    id_start = 0x200000
    id_end = 0x200008
    cfg_start = 0x300000
    cfg_end = 0x30000e
    eeprom_start = 0xf00000
    eeprom_end = 0xf00100
    pgm_rowsize = 4
    Terab = 10000
    Tpint = 1000
    Tpdfm = 5000
    cfg_impl = "00FF1F1F0088CD0003C003E00340"
    lvpbit_mask = 0x0004
    lvpbit_offset = 6

class PIC18F13K22(PS40001357):
    devid = 0x4F40

class PIC18F14K22(PS40001357):
    pgm_end = 0x4000
    pgm_rowsize = 8
    devid = 0x4F20

class PIC18LF13K22(PS40001357):
    devid = 0x4F80

class PIC18LF14K22(PS40001357):
    pgm_end = 0x4000
    pgm_rowsize = 8
    devid = 0x4F60


class PS40001390:       # family: PIC12(L)F1822/PIC16(L)F182X
    pgm_end = 0x800
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    eeprom_start = 0xf000
    eeprom_end = 0xf100
    pgm_rowsize = 32
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "3FFF3713"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC12F1822(PS40001390):
    pgm_rowsize = 16
    devid = 0x2700

class PIC12LF1822(PS40001390):
    pgm_rowsize = 16
    devid = 0x2800

class PIC16F1823(PS40001390):
    pgm_rowsize = 16
    devid = 0x2720

class PIC16F1824(PS40001390):
    pgm_end = 0x1000
    devid = 0x2740

class PIC16F1825(PS40001390):
    pgm_end = 0x2000
    devid = 0x2760

class PIC16F1826(PS40001390):
    pgm_rowsize = 8
    devid = 0x2780

class PIC16F1827(PS40001390):
    pgm_end = 0x1000
    pgm_rowsize = 8
    devid = 0x27A0

class PIC16F1828(PS40001390):
    pgm_end = 0x1000
    devid = 0x27C0

class PIC16F1829(PS40001390):
    pgm_end = 0x2000
    devid = 0x27E0

class PIC16F1829LIN(PS40001390):
    pgm_end = 0x2000
    devid = 0x27E0

class PIC16LF1823(PS40001390):
    pgm_rowsize = 16
    devid = 0x2820

class PIC16LF1824(PS40001390):
    pgm_end = 0x1000
    devid = 0x2840

class PIC16LF1825(PS40001390):
    pgm_end = 0x2000
    devid = 0x2860

class PIC16LF1826(PS40001390):
    pgm_rowsize = 8
    devid = 0x2880
    cfg_impl = "3FFF3703"

class PIC16LF1827(PS40001390):
    pgm_end = 0x1000
    pgm_rowsize = 8
    devid = 0x28A0
    cfg_impl = "3FFF3703"

class PIC16LF1828(PS40001390):
    pgm_end = 0x1000
    devid = 0x28C0

class PIC16LF1829(PS40001390):
    pgm_end = 0x2000
    devid = 0x28E0


class PS40001398:       # family: PIC18(L)F2XK22/4XK22
    pgm_end = 0x2000
    id_start = 0x200000
    id_end = 0x200008
    cfg_start = 0x300000
    cfg_end = 0x30000e
    eeprom_start = 0xf00000
    eeprom_end = 0xf00100
    pgm_rowsize = 32
    Terab = 12000
    Tpint = 1000
    Tpdfm = 5000
    cfg_impl = "00FF1F3F00BFC50003C003E00340"
    lvpbit_mask = 0x0004
    lvpbit_offset = 6

class PIC18F23K22(PS40001398):
    devid = 0x5740

class PIC18F24K22(PS40001398):
    pgm_end = 0x4000
    devid = 0x5640

class PIC18F25K22(PS40001398):
    pgm_end = 0x8000
    devid = 0x5540
    cfg_impl = "00FF1F3F00BFC5000FC00FE00F40"

class PIC18F26K22(PS40001398):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    devid = 0x5440
    cfg_impl = "00FF1F3F00BFC5000FC00FE00F40"

class PIC18F43K22(PS40001398):
    devid = 0x5700

class PIC18F44K22(PS40001398):
    pgm_end = 0x4000
    devid = 0x5600

class PIC18F45K22(PS40001398):
    pgm_end = 0x8000
    devid = 0x5500
    cfg_impl = "00FF1F3F00BFC5000FC00FE00F40"

class PIC18F46K22(PS40001398):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    devid = 0x5400
    cfg_impl = "00FF1F3F00BFC5000FC00FE00F40"

class PIC18LF23K22(PS40001398):
    devid = 0x5760

class PIC18LF24K22(PS40001398):
    pgm_end = 0x4000
    devid = 0x5660

class PIC18LF25K22(PS40001398):
    pgm_end = 0x8000
    devid = 0x5560
    cfg_impl = "00FF1F3F00BFC5000FC00FE00F40"

class PIC18LF26K22(PS40001398):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    devid = 0x5460
    cfg_impl = "00FF1F3F00BFC5000FC00FE00F40"

class PIC18LF43K22(PS40001398):
    devid = 0x5720

class PIC18LF44K22(PS40001398):
    pgm_end = 0x4000
    devid = 0x5620

class PIC18LF45K22(PS40001398):
    pgm_end = 0x8000
    devid = 0x5520
    cfg_impl = "00FF1F3F00BFC5000FC00FE00F40"

class PIC18LF46K22(PS40001398):
    pgm_end = 0x10000
    eeprom_end = 0xf00400
    devid = 0x5420
    cfg_impl = "00FF1F3F00BFC5000FC00FE00F40"


class PS40001439:       # family: PIC16F/LF1847/PIC12F/LF1840
    pgm_end = 0x1000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    eeprom_start = 0xf000
    eeprom_end = 0xf100
    pgm_rowsize = 32
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "3FFF3713"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC12F1840(PS40001439):
    devid = 0x1B80

class PIC12LF1840(PS40001439):
    devid = 0x1BC0

class PIC16F1847(PS40001439):
    pgm_end = 0x2000
    devid = 0x1480

class PIC16LF1847(PS40001439):
    pgm_end = 0x2000
    devid = 0x14A0


class PS40001457:       # family: PIC16(L)F178X
    pgm_end = 0x1000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    eeprom_start = 0xf000
    eeprom_end = 0xf100
    pgm_rowsize = 32
    Terab = 5000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "3FFF3F23"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC16F1782(PS40001457):
    pgm_end = 0x800
    devid = 0x2A00

class PIC16F1783(PS40001457):
    devid = 0x2A20

class PIC16F1784(PS40001457):
    devid = 0x2A40

class PIC16F1786(PS40001457):
    pgm_end = 0x2000
    devid = 0x2A60

class PIC16F1787(PS40001457):
    pgm_end = 0x2000
    devid = 0x2A80

class PIC16F1788(PS40001457):
    pgm_end = 0x4000
    devid = 0x302B

class PIC16F1789(PS40001457):
    pgm_end = 0x4000
    devid = 0x302A

class PIC16LF1782(PS40001457):
    pgm_end = 0x800
    devid = 0x2AA0
    cfg_impl = "3FFF3F03"

class PIC16LF1783(PS40001457):
    devid = 0x2AC0
    cfg_impl = "3FFF3F03"

class PIC16LF1784(PS40001457):
    devid = 0x2AE0
    cfg_impl = "3FFF3F03"

class PIC16LF1786(PS40001457):
    pgm_end = 0x2000
    devid = 0x2B00
    cfg_impl = "3FFF3F03"

class PIC16LF1787(PS40001457):
    pgm_end = 0x2000
    devid = 0x2B20
    cfg_impl = "3FFF3F03"

class PIC16LF1788(PS40001457):
    pgm_end = 0x4000
    devid = 0x302D
    cfg_impl = "3FFF3F03"

class PIC16LF1789(PS40001457):
    pgm_end = 0x4000
    devid = 0x302C
    cfg_impl = "3FFF3F03"


class PS40001573:       # family: PIC12(L)F1501/PIC16(L)F150X
    pgm_end = 0x800
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    pgm_rowsize = 16
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "0EFB2E03"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC12F1501(PS40001573):
    pgm_end = 0x400
    devid = 0x2CC0

class PIC12LF1501(PS40001573):
    pgm_end = 0x400
    devid = 0x2D80

class PIC16F1503(PS40001573):
    devid = 0x2CE0

class PIC16F1507(PS40001573):
    devid = 0x2D00

class PIC16F1508(PS40001573):
    pgm_end = 0x1000
    pgm_rowsize = 32
    devid = 0x2D20
    cfg_impl = "3EFF3E03"

class PIC16F1509(PS40001573):
    pgm_end = 0x2000
    pgm_rowsize = 32
    devid = 0x2D40
    cfg_impl = "3EFF3E03"

class PIC16LF1503(PS40001573):
    devid = 0x2DA0

class PIC16LF1507(PS40001573):
    devid = 0x2DC0

class PIC16LF1508(PS40001573):
    pgm_end = 0x1000
    pgm_rowsize = 32
    devid = 0x2DE0
    cfg_impl = "3EFF3E03"

class PIC16LF1509(PS40001573):
    pgm_end = 0x2000
    pgm_rowsize = 32
    devid = 0x2E00
    cfg_impl = "3EFF3E03"


class PS40001620:       # family: PIC16(L)F145X
    pgm_end = 0x2000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    pgm_rowsize = 32
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "3EFF3FF3"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC16F1454(PS40001620):
    devid = 0x3020

class PIC16F1455(PS40001620):
    devid = 0x3021

class PIC16F1459(PS40001620):
    devid = 0x3023

class PIC16LF1454(PS40001620):
    devid = 0x3024

class PIC16LF1455(PS40001620):
    devid = 0x3025

class PIC16LF1459(PS40001620):
    devid = 0x3027


class PS40001683:       # family: PIC16(L)F170X
    pgm_end = 0x800
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    pgm_rowsize = 32
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "3EFF3F87"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC16F1703(PS40001683):
    pgm_rowsize = 16
    devid = 0x3061
    cfg_impl = "0EFB3F87"

class PIC16F1704(PS40001683):
    pgm_end = 0x1000
    devid = 0x3043

class PIC16F1705(PS40001683):
    pgm_end = 0x2000
    devid = 0x3055

class PIC16F1707(PS40001683):
    pgm_rowsize = 16
    devid = 0x3060
    cfg_impl = "0EFB3F87"

class PIC16F1708(PS40001683):
    pgm_end = 0x1000
    devid = 0x3042

class PIC16F1709(PS40001683):
    pgm_end = 0x2000
    devid = 0x3054

class PIC16LF1703(PS40001683):
    pgm_rowsize = 16
    devid = 0x3063
    cfg_impl = "0EFB3F87"

class PIC16LF1704(PS40001683):
    pgm_end = 0x1000
    devid = 0x3045

class PIC16LF1705(PS40001683):
    pgm_end = 0x2000
    devid = 0x3057

class PIC16LF1707(PS40001683):
    pgm_rowsize = 16
    devid = 0x3062
    cfg_impl = "0EFB3F87"

class PIC16LF1708(PS40001683):
    pgm_end = 0x1000
    devid = 0x3044

class PIC16LF1709(PS40001683):
    pgm_end = 0x2000
    devid = 0x3056


class PS40001713:       # family: PIC12(L)F1571/2
    pgm_end = 0x400
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    pgm_rowsize = 16
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "0EFB3F03"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC12F1571(PS40001713):
    devid = 0x3051

class PIC12F1572(PS40001713):
    pgm_end = 0x800
    devid = 0x3050

class PIC12LF1571(PS40001713):
    devid = 0x3053

class PIC12LF1572(PS40001713):
    pgm_end = 0x800
    devid = 0x3052


class PS40001720:       # family: PIC12(L)F1612/16(L)F161X
    pgm_end = 0x800
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800a
    pgm_rowsize = 32
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "0EE33F833F7F"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC12F1612(PS40001720):
    pgm_rowsize = 16
    devid = 0x3058

class PIC12LF1612(PS40001720):
    pgm_rowsize = 16
    devid = 0x3059

class PIC16F1613(PS40001720):
    pgm_rowsize = 16
    devid = 0x304C

class PIC16F1614(PS40001720):
    pgm_end = 0x1000
    devid = 0x3078
    cfg_impl = "0EE33F873F7F"

class PIC16F1615(PS40001720):
    pgm_end = 0x2000
    devid = 0x307C
    cfg_impl = "3EE73F873F7F"

class PIC16F1618(PS40001720):
    pgm_end = 0x1000
    devid = 0x3079
    cfg_impl = "0EE33F873F7F"

class PIC16F1619(PS40001720):
    pgm_end = 0x2000
    devid = 0x307D
    cfg_impl = "3EE73F873F7F"

class PIC16LF1613(PS40001720):
    pgm_rowsize = 16
    devid = 0x304D

class PIC16LF1614(PS40001720):
    pgm_end = 0x1000
    devid = 0x307A
    cfg_impl = "0EE33F873F7F"

class PIC16LF1615(PS40001720):
    pgm_end = 0x2000
    devid = 0x307E
    cfg_impl = "3EE73F873F7F"

class PIC16LF1618(PS40001720):
    pgm_end = 0x1000
    devid = 0x307B
    cfg_impl = "0EE33F873F7F"

class PIC16LF1619(PS40001720):
    pgm_end = 0x2000
    devid = 0x307F
    cfg_impl = "3EE73F873F7F"


class PS40001753:       # family: PIC16(L)F188XX
    pgm_end = 0x2000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    eeprom_start = 0xf000
    eeprom_end = 0xf100
    pgm_rowsize = 32
    Terab = 5600
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "29773EE33F7F30030003"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F18854(PS40001753):
    pgm_end = 0x1000
    devid = 0x306A

class PIC16F18855(PS40001753):
    devid = 0x306C

class PIC16F18856(PS40001753):
    pgm_end = 0x4000
    devid = 0x3070

class PIC16F18857(PS40001753):
    pgm_end = 0x8000
    devid = 0x3074

class PIC16F18875(PS40001753):
    devid = 0x306D

class PIC16F18876(PS40001753):
    pgm_end = 0x4000
    devid = 0x3071

class PIC16F18877(PS40001753):
    pgm_end = 0x8000
    devid = 0x3075

class PIC16LF18854(PS40001753):
    pgm_end = 0x1000
    devid = 0x306B

class PIC16LF18855(PS40001753):
    devid = 0x306E

class PIC16LF18856(PS40001753):
    pgm_end = 0x4000
    devid = 0x3072

class PIC16LF18857(PS40001753):
    pgm_end = 0x8000
    devid = 0x3076

class PIC16LF18875(PS40001753):
    devid = 0x306F

class PIC16LF18876(PS40001753):
    pgm_end = 0x4000
    devid = 0x3073

class PIC16LF18877(PS40001753):
    pgm_end = 0x8000
    devid = 0x3077


class PS40001754:       # family: PIC16(L)F176X
    pgm_end = 0x1000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    pgm_rowsize = 32
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "3EFF3F87"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC16F1764(PS40001754):
    devid = 0x3080

class PIC16F1765(PS40001754):
    pgm_end = 0x2000
    devid = 0x3081

class PIC16F1768(PS40001754):
    devid = 0x3084

class PIC16F1769(PS40001754):
    pgm_end = 0x2000
    devid = 0x3085

class PIC16LF1764(PS40001754):
    devid = 0x3082

class PIC16LF1765(PS40001754):
    pgm_end = 0x2000
    devid = 0x3083

class PIC16LF1768(PS40001754):
    devid = 0x3086

class PIC16LF1769(PS40001754):
    pgm_end = 0x2000
    devid = 0x3087


class PS40001766:       # family: PIC16(L)F157X
    pgm_end = 0x1000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    pgm_rowsize = 32
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "0EFB3F07"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC16F1574(PS40001766):
    devid = 0x3000

class PIC16F1575(PS40001766):
    pgm_end = 0x2000
    devid = 0x3001

class PIC16F1578(PS40001766):
    devid = 0x3002

class PIC16F1579(PS40001766):
    pgm_end = 0x2000
    devid = 0x3003

class PIC16LF1574(PS40001766):
    devid = 0x3004

class PIC16LF1575(PS40001766):
    pgm_end = 0x2000
    devid = 0x3005

class PIC16LF1578(PS40001766):
    devid = 0x3006

class PIC16LF1579(PS40001766):
    pgm_end = 0x2000
    devid = 0x3007


class PS40001772:       # family: PIC18(L)F2X/4XK40
    pgm_end = 0x8000
    id_start = 0x200000
    id_end = 0x200010
    cfg_start = 0x300000
    cfg_end = 0x30000c
    eeprom_start = 0x310000
    eeprom_end = 0x310400
    pgm_rowsize = 32
    Terab = 25200
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "7729E3BF7F3F0F3703000F02"
    lvpbit_mask = 0x0020
    lvpbit_offset = 7

class PIC18F24K40(PS40001772):
    pgm_end = 0x4000
    eeprom_end = 0x310100
    devid = 0x69C0
    cfg_impl = "7729E3BF7F3F033703000302"

class PIC18F25K40(PS40001772):
    eeprom_end = 0x310100
    devid = 0x69A0

class PIC18F26K40(PS40001772):
    pgm_end = 0x10000
    devid = 0x6980

class PIC18F27K40(PS40001772):
    pgm_end = 0x20000
    pgm_rowsize = 64
    devid = 0x6960
    cfg_impl = "7729E3BF7F3FFF370300FF02"

class PIC18F45K40(PS40001772):
    eeprom_end = 0x310100
    devid = 0x6940

class PIC18F46K40(PS40001772):
    pgm_end = 0x10000
    devid = 0x6920

class PIC18F47K40(PS40001772):
    pgm_end = 0x20000
    pgm_rowsize = 64
    devid = 0x6900
    cfg_impl = "7729E3BF7F3FFF370300FF02"

class PIC18LF24K40(PS40001772):
    pgm_end = 0x4000
    eeprom_end = 0x310100
    devid = 0x6AA0
    cfg_impl = "7729E3BF7F3F033703000302"

class PIC18LF25K40(PS40001772):
    eeprom_end = 0x310100
    devid = 0x6A80

class PIC18LF26K40(PS40001772):
    pgm_end = 0x10000
    devid = 0x6A60

class PIC18LF27K40(PS40001772):
    pgm_end = 0x20000
    pgm_rowsize = 64
    devid = 0x6A40
    cfg_impl = "7729E3BF7F3FFF370300FF02"

class PIC18LF45K40(PS40001772):
    eeprom_end = 0x310100
    devid = 0x6A20

class PIC18LF46K40(PS40001772):
    pgm_end = 0x10000
    devid = 0x6A00

class PIC18LF47K40(PS40001772):
    pgm_end = 0x20000
    pgm_rowsize = 64
    devid = 0x69E0
    cfg_impl = "7729E3BF7F3FFF370300FF02"


class PS40001792:       # family: PIC16(L)F177X
    pgm_end = 0x2000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x8009
    pgm_rowsize = 32
    Terab = 6000
    Tpint = 2500
    Tpdfm = 5000
    cfg_impl = "3EFF3F87"
    lvpbit_mask = 0x2000
    lvpbit_offset = 1

class PIC16F1773(PS40001792):
    pgm_end = 0x1000
    devid = 0x308A

class PIC16F1776(PS40001792):
    devid = 0x308B

class PIC16F1777(PS40001792):
    devid = 0x308E

class PIC16F1778(PS40001792):
    pgm_end = 0x4000
    devid = 0x308F

class PIC16F1779(PS40001792):
    pgm_end = 0x4000
    devid = 0x3090

class PIC16LF1773(PS40001792):
    pgm_end = 0x1000
    devid = 0x308C

class PIC16LF1776(PS40001792):
    devid = 0x308D

class PIC16LF1777(PS40001792):
    devid = 0x3091

class PIC16LF1778(PS40001792):
    pgm_end = 0x4000
    devid = 0x3092

class PIC16LF1779(PS40001792):
    pgm_end = 0x4000
    devid = 0x3093


class PS40001822:       # family: PIC18(L)F6XK40
    pgm_end = 0x8000
    id_start = 0x200000
    id_end = 0x200010
    cfg_start = 0x300000
    cfg_end = 0x30000c
    eeprom_start = 0x310000
    eeprom_end = 0x310400
    pgm_rowsize = 32
    Terab = 25200
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "7729E3BF7F3F0F3703000F02"
    lvpbit_mask = 0x0020
    lvpbit_offset = 7

class PIC18F65K40(PS40001822):
    devid = 0x6B00

class PIC18F66K40(PS40001822):
    pgm_end = 0x10000
    devid = 0x6AE0

class PIC18F67K40(PS40001822):
    pgm_end = 0x20000
    pgm_rowsize = 64
    devid = 0x6AC0
    cfg_impl = "7729E3BF7F3FFF370300FF02"

class PIC18LF65K40(PS40001822):
    devid = 0x6B60

class PIC18LF66K40(PS40001822):
    pgm_end = 0x10000
    devid = 0x6B40

class PIC18LF67K40(PS40001822):
    pgm_end = 0x20000
    pgm_rowsize = 64
    devid = 0x6B20
    cfg_impl = "7729E3BF7F3FFF370300FF02"


class PS40001836:       # family: PIC18(L)F24/25K42
    pgm_end = 0x4000
    id_start = 0x200000
    id_end = 0x200010
    cfg_start = 0x300000
    cfg_end = 0x30000a
    eeprom_start = 0x310000
    eeprom_end = 0x310100
    pgm_rowsize = 32
    Terab = 8400
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "772BFFBF7F3F9F2F0100"
    lvpbit_mask = 0x0020
    lvpbit_offset = 7

class PIC18F24K42(PS40001836):
    devid = 0x6CA0

class PIC18F25K42(PS40001836):
    pgm_end = 0x8000
    devid = 0x6C80

class PIC18LF24K42(PS40001836):
    devid = 0x6DE0

class PIC18LF25K42(PS40001836):
    pgm_end = 0x8000
    devid = 0x6DC0


class PS40001838:       # family: PIC16(L)F153XX
    pgm_end = 0x2000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    pgm_rowsize = 32
    Terab = 8400
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "29773EE33F7F2B9F0001"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F15313(PS40001838):
    pgm_end = 0x800
    devid = 0x30BE

class PIC16F15323(PS40001838):
    pgm_end = 0x800
    devid = 0x30C0

class PIC16F15324(PS40001838):
    pgm_end = 0x1000
    devid = 0x30C2

class PIC16F15325(PS40001838):
    devid = 0x30C6

class PIC16F15344(PS40001838):
    pgm_end = 0x1000
    devid = 0x30C4

class PIC16F15345(PS40001838):
    devid = 0x30C8

class PIC16F15354(PS40001838):
    pgm_end = 0x1000
    devid = 0x30AC

class PIC16F15355(PS40001838):
    devid = 0x30AE

class PIC16F15356(PS40001838):
    pgm_end = 0x4000
    devid = 0x30B0

class PIC16F15375(PS40001838):
    devid = 0x30B2

class PIC16F15376(PS40001838):
    pgm_end = 0x4000
    devid = 0x30B4

class PIC16F15385(PS40001838):
    devid = 0x30B6

class PIC16F15386(PS40001838):
    pgm_end = 0x4000
    devid = 0x30B8

class PIC16LF15313(PS40001838):
    pgm_end = 0x800
    devid = 0x30BF

class PIC16LF15323(PS40001838):
    pgm_end = 0x800
    devid = 0x30C1

class PIC16LF15324(PS40001838):
    pgm_end = 0x1000
    devid = 0x30C3

class PIC16LF15325(PS40001838):
    devid = 0x30C7

class PIC16LF15344(PS40001838):
    pgm_end = 0x1000
    devid = 0x30C5

class PIC16LF15345(PS40001838):
    devid = 0x30C9

class PIC16LF15354(PS40001838):
    pgm_end = 0x1000
    devid = 0x30AD

class PIC16LF15355(PS40001838):
    devid = 0x30AF

class PIC16LF15356(PS40001838):
    pgm_end = 0x4000
    devid = 0x30B1

class PIC16LF15375(PS40001838):
    devid = 0x30B3

class PIC16LF15376(PS40001838):
    pgm_end = 0x4000
    devid = 0x30B5

class PIC16LF15385(PS40001838):
    devid = 0x30B7

class PIC16LF15386(PS40001838):
    pgm_end = 0x4000
    devid = 0x30B9


class PS40001874:       # family: PIC18F2X/4XQ10
    pgm_end = 0x8000
    id_start = 0x200000
    id_end = 0x200100
    cfg_start = 0x300000
    cfg_end = 0x30000c
    eeprom_start = 0x310000
    eeprom_end = 0x310400
    pgm_rowsize = 1
    Terab = 65535
    Tpint = 65
    Tpdfm = 65
    cfg_impl = "7729E3BF7F3F033703000302"
    lvpbit_mask = 0x0020
    lvpbit_offset = 7

class PIC18F24Q10(PS40001874):
    pgm_end = 0x4000
    eeprom_end = 0x310100
    devid = 0x71C0

class PIC18F25Q10(PS40001874):
    eeprom_end = 0x310100
    devid = 0x71A0

class PIC18F26Q10(PS40001874):
    pgm_end = 0x10000
    devid = 0x7180

class PIC18F27Q10(PS40001874):
    pgm_end = 0x20000
    devid = 0x7100
    cfg_impl = "7729E3BF7F3FF3370300F302"

class PIC18F45Q10(PS40001874):
    eeprom_end = 0x310100
    devid = 0x7140

class PIC18F46Q10(PS40001874):
    pgm_end = 0x10000
    devid = 0x7120

class PIC18F47Q10(PS40001874):
    pgm_end = 0x20000
    devid = 0x70E0
    cfg_impl = "7729E3BF7F3FF3370300F302"


class PS40001880:       # family: PIC16(L)F191XX
    pgm_end = 0x2000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    eeprom_start = 0xf000
    eeprom_end = 0xf100
    pgm_rowsize = 32
    Terab = 14000
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "29773EE13F7F2F9F0001"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F19155(PS40001880):
    devid = 0x3096

class PIC16F19156(PS40001880):
    pgm_end = 0x4000
    devid = 0x3098

class PIC16F19175(PS40001880):
    devid = 0x309A

class PIC16F19176(PS40001880):
    pgm_end = 0x4000
    devid = 0x309C

class PIC16F19185(PS40001880):
    devid = 0x30BA

class PIC16F19186(PS40001880):
    pgm_end = 0x4000
    devid = 0x30BC

class PIC16LF19155(PS40001880):
    devid = 0x3097

class PIC16LF19156(PS40001880):
    pgm_end = 0x4000
    devid = 0x3099

class PIC16LF19175(PS40001880):
    devid = 0x309B

class PIC16LF19176(PS40001880):
    pgm_end = 0x4000
    devid = 0x309D

class PIC16LF19185(PS40001880):
    devid = 0x30BB

class PIC16LF19186(PS40001880):
    pgm_end = 0x4000
    devid = 0x30BD


class PS40001886:       # family: PIC18(L)F26/27/45/46/47/55/56/57K42
    pgm_end = 0x10000
    id_start = 0x200000
    id_end = 0x200010
    cfg_start = 0x300000
    cfg_end = 0x30000a
    eeprom_start = 0x310000
    eeprom_end = 0x310400
    pgm_rowsize = 64
    Terab = 8400
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "772BFFBF7F3F9F2F0100"
    lvpbit_mask = 0x0020
    lvpbit_offset = 7

class PIC18F26K42(PS40001886):
    devid = 0x6C60

class PIC18F27K42(PS40001886):
    pgm_end = 0x20000
    devid = 0x6C40

class PIC18F45K42(PS40001886):
    pgm_end = 0x8000
    eeprom_end = 0x310100
    devid = 0x6C20

class PIC18F46K42(PS40001886):
    devid = 0x6C00

class PIC18F47K42(PS40001886):
    pgm_end = 0x20000
    devid = 0x6BE0

class PIC18F55K42(PS40001886):
    pgm_end = 0x8000
    eeprom_end = 0x310100
    devid = 0x6BC0

class PIC18F56K42(PS40001886):
    devid = 0x6BA0

class PIC18F57K42(PS40001886):
    pgm_end = 0x20000
    devid = 0x6B80

class PIC18LF26K42(PS40001886):
    devid = 0x6DA0

class PIC18LF27K42(PS40001886):
    pgm_end = 0x20000
    devid = 0x6D80

class PIC18LF45K42(PS40001886):
    pgm_end = 0x8000
    eeprom_end = 0x310100
    devid = 0x6D60

class PIC18LF46K42(PS40001886):
    devid = 0x6D40

class PIC18LF47K42(PS40001886):
    pgm_end = 0x20000
    devid = 0x6D20

class PIC18LF55K42(PS40001886):
    pgm_end = 0x8000
    eeprom_end = 0x310100
    devid = 0x6D00

class PIC18LF56K42(PS40001886):
    devid = 0x6CE0

class PIC18LF57K42(PS40001886):
    pgm_end = 0x20000
    devid = 0x6CC0


class PS40001970:       # family: PIC16(L)F184XX
    pgm_end = 0x2000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    eeprom_start = 0xf000
    eeprom_end = 0xf100
    pgm_rowsize = 32
    Terab = 8400
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "29773EE73F7F2F9F0001"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F18424(PS40001970):
    pgm_end = 0x1000
    devid = 0x30CA

class PIC16F18425(PS40001970):
    devid = 0x30CC

class PIC16F18426(PS40001970):
    pgm_end = 0x4000
    devid = 0x30D2

class PIC16F18444(PS40001970):
    pgm_end = 0x1000
    devid = 0x30CE

class PIC16F18445(PS40001970):
    devid = 0x30D0

class PIC16F18446(PS40001970):
    pgm_end = 0x4000
    devid = 0x30D4

class PIC16F18455(PS40001970):
    devid = 0x30D7

class PIC16F18456(PS40001970):
    pgm_end = 0x4000
    devid = 0x30D9

class PIC16LF18424(PS40001970):
    pgm_end = 0x1000
    devid = 0x30CB

class PIC16LF18425(PS40001970):
    devid = 0x30CD

class PIC16LF18426(PS40001970):
    pgm_end = 0x4000
    devid = 0x30D3

class PIC16LF18444(PS40001970):
    pgm_end = 0x1000
    devid = 0x30CF

class PIC16LF18445(PS40001970):
    devid = 0x30D1

class PIC16LF18446(PS40001970):
    pgm_end = 0x4000
    devid = 0x30D5

class PIC16LF18455(PS40001970):
    devid = 0x30D8

class PIC16LF18456(PS40001970):
    pgm_end = 0x4000
    devid = 0x30DA


class PS40002079:       # family: PIC18-Q43
    pgm_end = 0x8000
    id_start = 0x200000
    id_end = 0x200040
    cfg_start = 0x300000
    cfg_end = 0x300012
    eeprom_start = 0x380000
    eeprom_end = 0x380400
    pgm_rowsize = 1
    Terab = 11000
    Tpint = 75
    Tpdfm = 11000
    cfg_impl = "772BFFBF7F3F3F8F0001"
    lvpbit_mask = 0x0020
    lvpbit_offset = 3

class PIC18F25Q43(PS40002079):
    devid = 0x73C0

class PIC18F26Q43(PS40002079):
    pgm_end = 0x10000
    devid = 0x7420

class PIC18F27Q43(PS40002079):
    pgm_end = 0x20000
    devid = 0x7480

class PIC18F45Q43(PS40002079):
    devid = 0x73E0

class PIC18F46Q43(PS40002079):
    pgm_end = 0x10000
    devid = 0x7440

class PIC18F47Q43(PS40002079):
    pgm_end = 0x20000
    devid = 0x74A0

class PIC18F55Q43(PS40002079):
    devid = 0x7400

class PIC18F56Q43(PS40002079):
    pgm_end = 0x10000
    devid = 0x7460

class PIC18F57Q43(PS40002079):
    pgm_end = 0x20000
    devid = 0x74C0


class PS40002137:       # family: PIC18-Q83/84
    pgm_end = 0x10000
    id_start = 0x200000
    id_end = 0x200040
    cfg_start = 0x300000
    cfg_end = 0x300024
    eeprom_start = 0x380000
    eeprom_end = 0x380400
    pgm_rowsize = 1
    Terab = 16800
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "77FBFFBF7F3F1F0F3301FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    lvpbit_mask = 0x0020
    lvpbit_offset = 3

class PIC18F26Q83(PS40002137):
    devid = 0xA306

class PIC18F26Q84(PS40002137):
    devid = 0xA300

class PIC18F27Q83(PS40002137):
    pgm_end = 0x20000
    devid = 0x9909

class PIC18F27Q84(PS40002137):
    pgm_end = 0x20000
    devid = 0x9903

class PIC18F46Q83(PS40002137):
    devid = 0xA307

class PIC18F46Q84(PS40002137):
    devid = 0xA301

class PIC18F47Q83(PS40002137):
    pgm_end = 0x20000
    devid = 0x990A

class PIC18F47Q84(PS40002137):
    pgm_end = 0x20000
    devid = 0x9904

class PIC18F56Q83(PS40002137):
    devid = 0xA308

class PIC18F56Q84(PS40002137):
    devid = 0xA302

class PIC18F57Q83(PS40002137):
    pgm_end = 0x20000
    devid = 0x990B

class PIC18F57Q84(PS40002137):
    pgm_end = 0x20000
    devid = 0x9905


class PS40002143:       # family: PIC18-Q41
    pgm_end = 0x4000
    id_start = 0x200000
    id_end = 0x200040
    cfg_start = 0x300000
    cfg_end = 0x300012
    eeprom_start = 0x380000
    eeprom_end = 0x380200
    pgm_rowsize = 1
    Terab = 11000
    Tpint = 75
    Tpdfm = 11000
    cfg_impl = "77EBFFBF7F3F1F0F01"
    lvpbit_mask = 0x0020
    lvpbit_offset = 3

class PIC18F04Q41(PS40002143):
    devid = 0x7540

class PIC18F05Q41(PS40002143):
    pgm_end = 0x8000
    devid = 0x7500

class PIC18F06Q41(PS40002143):
    pgm_end = 0x10000
    devid = 0x7580

class PIC18F14Q41(PS40002143):
    devid = 0x7520

class PIC18F15Q41(PS40002143):
    pgm_end = 0x8000
    devid = 0x74E0

class PIC18F16Q41(PS40002143):
    pgm_end = 0x10000
    devid = 0x7560


class PS40002149:       # family: PIC16F152XX
    pgm_end = 0x1000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    pgm_rowsize = 32
    Terab = 8400
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "11333ADF00002B9F0001"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F15213(PS40002149):
    pgm_end = 0x800
    devid = 0x30E3

class PIC16F15214(PS40002149):
    devid = 0x30E6

class PIC16F15223(PS40002149):
    pgm_end = 0x800
    devid = 0x30E4

class PIC16F15224(PS40002149):
    devid = 0x30E7

class PIC16F15225(PS40002149):
    pgm_end = 0x2000
    devid = 0x30E9

class PIC16F15243(PS40002149):
    pgm_end = 0x800
    devid = 0x30E5

class PIC16F15244(PS40002149):
    devid = 0x30E8

class PIC16F15245(PS40002149):
    pgm_end = 0x2000
    devid = 0x30EA

class PIC16F15254(PS40002149):
    devid = 0x30F0

class PIC16F15255(PS40002149):
    pgm_end = 0x2000
    devid = 0x30EF

class PIC16F15256(PS40002149):
    pgm_end = 0x4000
    devid = 0x30EB

class PIC16F15274(PS40002149):
    devid = 0x30EE

class PIC16F15275(PS40002149):
    pgm_end = 0x2000
    devid = 0x30ED

class PIC16F15276(PS40002149):
    pgm_end = 0x4000
    devid = 0x30EC


class PS40002185:       # family: PIC18-Q40
    pgm_end = 0x4000
    id_start = 0x200000
    id_end = 0x200040
    cfg_start = 0x300000
    cfg_end = 0x300012
    eeprom_start = 0x380000
    eeprom_end = 0x380200
    pgm_rowsize = 1
    Terab = 11000
    Tpint = 75
    Tpdfm = 11000
    cfg_impl = "77EBFFBF7F3F1F0F01"
    lvpbit_mask = 0x0020
    lvpbit_offset = 3

class PIC18F04Q40(PS40002185):
    devid = 0x7640

class PIC18F05Q40(PS40002185):
    pgm_end = 0x8000
    devid = 0x7600

class PIC18F06Q40(PS40002185):
    pgm_end = 0x10000
    devid = 0x75C0

class PIC18F14Q40(PS40002185):
    devid = 0x7620

class PIC18F15Q40(PS40002185):
    pgm_end = 0x8000
    devid = 0x75E0

class PIC18F16Q40(PS40002185):
    pgm_end = 0x10000
    devid = 0x75A0


class PS40002266:       # family: PIC16F171XX
    pgm_end = 0x1000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    eeprom_start = 0xf000
    eeprom_end = 0xf080
    pgm_rowsize = 32
    Terab = 8400
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "39773FE73F7F2F9F0003"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F17114(PS40002266):
    devid = 0x30DB

class PIC16F17115(PS40002266):
    pgm_end = 0x2000
    devid = 0x30E2

class PIC16F17124(PS40002266):
    devid = 0x30DC

class PIC16F17125(PS40002266):
    pgm_end = 0x2000
    devid = 0x30DE

class PIC16F17126(PS40002266):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x30E0

class PIC16F17144(PS40002266):
    devid = 0x30DD

class PIC16F17145(PS40002266):
    pgm_end = 0x2000
    devid = 0x30DF

class PIC16F17146(PS40002266):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x30E1

class PIC16F17154(PS40002266):
    devid = 0x3101

class PIC16F17155(PS40002266):
    pgm_end = 0x2000
    devid = 0x3103

class PIC16F17156(PS40002266):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x3105

class PIC16F17174(PS40002266):
    devid = 0x3102

class PIC16F17175(PS40002266):
    pgm_end = 0x2000
    devid = 0x3104

class PIC16F17176(PS40002266):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x3106


class PS40002276:       # family: PIC16F181XX
    pgm_end = 0x1000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    eeprom_start = 0xf000
    eeprom_end = 0xf080
    pgm_rowsize = 32
    Terab = 8400
    Tpint = 2800
    Tpdfm = 5600
    cfg_impl = "39773FE73F7F2F9F0003"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F18114(PS40002276):
    devid = 0x3107

class PIC16F18115(PS40002276):
    pgm_end = 0x2000
    devid = 0x310C

class PIC16F18124(PS40002276):
    devid = 0x3108

class PIC16F18125(PS40002276):
    pgm_end = 0x2000
    devid = 0x310D

class PIC16F18126(PS40002276):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x3111

class PIC16F18144(PS40002276):
    devid = 0x3109

class PIC16F18145(PS40002276):
    pgm_end = 0x2000
    devid = 0x310E

class PIC16F18146(PS40002276):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x3112

class PIC16F18154(PS40002276):
    devid = 0x310A

class PIC16F18155(PS40002276):
    pgm_end = 0x2000
    devid = 0x310F

class PIC16F18156(PS40002276):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x3113

class PIC16F18174(PS40002276):
    devid = 0x310B

class PIC16F18175(PS40002276):
    pgm_end = 0x2000
    devid = 0x3110

class PIC16F18176(PS40002276):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x3114


class PS40002306:       # family: PIC18-Q71
    pgm_end = 0x4000
    id_start = 0x200000
    id_end = 0x200040
    cfg_start = 0x300000
    cfg_end = 0x30000b
    eeprom_start = 0x380000
    eeprom_end = 0x380100
    pgm_rowsize = 1
    Terab = 11000
    Tpint = 75
    Tpdfm = 11000
    cfg_impl = "77EBFFBF7F3FFFFF8F0101"
    lvpbit_mask = 0x0020
    lvpbit_offset = 3

class PIC18F24Q71(PS40002306):
    devid = 0x7660

class PIC18F25Q71(PS40002306):
    pgm_end = 0x8000
    devid = 0x76A0

class PIC18F26Q71(PS40002306):
    pgm_end = 0x10000
    devid = 0x76E0

class PIC18F44Q71(PS40002306):
    devid = 0x77A0

class PIC18F45Q71(PS40002306):
    pgm_end = 0x8000
    devid = 0x77E0

class PIC18F46Q71(PS40002306):
    pgm_end = 0x10000
    devid = 0x7720

class PIC18F54Q71(PS40002306):
    devid = 0x7820

class PIC18F55Q71(PS40002306):
    pgm_end = 0x8000
    devid = 0x7860

class PIC18F56Q71(PS40002306):
    pgm_end = 0x10000
    devid = 0x7760


class PS40002317:       # family: PIC16F180XX
    pgm_end = 0x1000
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    eeprom_start = 0xf000
    eeprom_end = 0xf080
    pgm_rowsize = 32
    Terab = 13000
    Tpint = 5600
    Tpdfm = 5600
    cfg_impl = "11733FDF00002F9F0003"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F18013(PS40002317):
    pgm_end = 0x800
    devid = 0x30F1

class PIC16F18014(PS40002317):
    devid = 0x30F2

class PIC16F18015(PS40002317):
    pgm_end = 0x2000
    devid = 0x30F5

class PIC16F18023(PS40002317):
    pgm_end = 0x800
    devid = 0x30F3

class PIC16F18024(PS40002317):
    devid = 0x30F4

class PIC16F18025(PS40002317):
    pgm_end = 0x2000
    devid = 0x30F6

class PIC16F18026(PS40002317):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x30F9

class PIC16F18044(PS40002317):
    devid = 0x30F7

class PIC16F18045(PS40002317):
    pgm_end = 0x2000
    devid = 0x30F8

class PIC16F18046(PS40002317):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x30FA

class PIC16F18054(PS40002317):
    devid = 0x30FB

class PIC16F18055(PS40002317):
    pgm_end = 0x2000
    devid = 0x30FC

class PIC16F18056(PS40002317):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x30FF

class PIC16F18074(PS40002317):
    devid = 0x30FD

class PIC16F18075(PS40002317):
    pgm_end = 0x2000
    devid = 0x30FE

class PIC16F18076(PS40002317):
    pgm_end = 0x4000
    eeprom_end = 0xf100
    devid = 0x3100


class PS40002327:       # family: PIC18-Q20
    pgm_end = 0x10000
    id_start = 0x200000
    id_end = 0x200040
    cfg_start = 0x300000
    cfg_end = 0x30001a
    eeprom_start = 0x380000
    eeprom_end = 0x380100
    pgm_rowsize = 1
    Terab = 11000
    Tpint = 75
    Tpdfm = 11000
    cfg_impl = "77EFFFFB7F3F01FF8F01010000000000000000000000000001FF"
    lvpbit_mask = 0x0020
    lvpbit_offset = 3

class PIC18F06Q20(PS40002327):
    devid = 0x7A60

class PIC18F16Q20(PS40002327):
    devid = 0x7A40
    cfg_impl = "77EFFFFB7F3F03FF8F01010000000000000000000000000001FF"


class PS40002414:       # family: PIC18-Q24
    pgm_end = 0x8000
    id_start = 0x200000
    id_end = 0x200040
    cfg_start = 0x300000
    cfg_end = 0x30001d
    eeprom_start = 0x380000
    eeprom_end = 0x380200
    pgm_rowsize = 1
    Terab = 11000
    Tpint = 75
    Tpdfm = 11000
    cfg_impl = "77FBFFBF7F3FFF008F00010000000000000000000000000001FF000001"
    lvpbit_mask = 0x0020
    lvpbit_offset = 3

class PIC18F24Q24(PS40002414):
    devid = 0x78A0

class PIC18F25Q24(PS40002414):
    devid = 0x78C0

class PIC18F26Q24(PS40002414):
    pgm_end = 0x10000
    devid = 0x78E0

class PIC18F45Q24(PS40002414):
    devid = 0x7940

class PIC18F46Q24(PS40002414):
    pgm_end = 0x10000
    devid = 0x7900

class PIC18F55Q24(PS40002414):
    devid = 0x7960

class PIC18F56Q24(PS40002414):
    pgm_end = 0x10000
    devid = 0x7920


class PS40002500:       # family: PIC16F13145
    pgm_end = 0x800
    id_start = 0x8000
    id_end = 0x8004
    cfg_start = 0x8007
    cfg_end = 0x800c
    pgm_rowsize = 32
    Terab = 20000
    Tpint = 12000
    Tpdfm = 5600
    cfg_impl = "39773BE73F7F2B9F0001"
    lvpbit_mask = 0x2000
    lvpbit_offset = 3

class PIC16F13113(PS40002500):
    devid = 0x3121

class PIC16F13114(PS40002500):
    pgm_end = 0x1000
    devid = 0x3124

class PIC16F13115(PS40002500):
    pgm_end = 0x2000
    devid = 0x3127

class PIC16F13123(PS40002500):
    devid = 0x3122

class PIC16F13124(PS40002500):
    pgm_end = 0x1000
    devid = 0x3125

class PIC16F13125(PS40002500):
    pgm_end = 0x2000
    devid = 0x3128

class PIC16F13143(PS40002500):
    devid = 0x3123

class PIC16F13144(PS40002500):
    pgm_end = 0x1000
    devid = 0x3126

class PIC16F13145(PS40002500):
    pgm_end = 0x2000
    devid = 0x3129


