"""
lvp_1455_rp2_blink.py

Micropython script for programming PIC16F1455 in LVP mode with RP2040

Author: Rob Hamerling; Copyright (C). All rights reserved 2019..2023.

License: MIT License
"""

from os import stat
from sys import exit
from machine import Pin
from time import ticks_ms, ticks_diff

from lvp_pic import PIC16F145x    # class for this family

hexfile = "16f1455_blink_intosc.hex"
# Check if specified hexfile is present
try:
    stat(hexfile)
except:
    print('==> ERR: IntelHex file', hexfile, 'not found')
    exit(1)

start_time = ticks_ms()

# with bit banging one pin is used for input and output
pic = PIC16F145x(mclr=Pin(22), icspclk=Pin(18),
                 icspdat=Pin(16), led=Pin(25))
print('Programming 16F1455 PIC in LVP-mode')

# ----- Start the actual programming -----
# set PIC in LVP-mode, check if target supported, transfer hex file
pic.lvp_start()                                 # init LVP
target = pic.lvp_check_target()
if target is not None:
    if pic.lvp_load_hexfile(hexfile):           # transfer contents to PIC
        print("Programming finished successfully")
        print("Execution time {:.2f} seconds".format(ticks_diff(ticks_ms(), start_time) / 1000))
        pic.led.off()                           # LED off
    else:
        print("Programming failed, see previous message(s)")
        pic.led.on()                            # Signal that programming failed
pic.lvp_stop()                                  # de-init LVP

#
