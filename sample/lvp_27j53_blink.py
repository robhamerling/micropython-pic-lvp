"""
lvp_27j53_blink.py

Micropython script for programming PIC18F27J53 in LVP mode with RP2040

Author: Rob Hamerling; Copyright (C). All rights reserved 2019..2024.

License: MIT License
"""
from os import stat
from sys import exit
from machine import Pin
from time import ticks_ms, ticks_diff

from lvp_pic import PIC18FJ5x    # class for this family

hexfile = "18f27j53_blink_intosc.hex"
# Check if specified hexfile is present
try:
    stat(hexfile)
except:
    print("==> ERR: IntelHex file", hexfile, "not found")
    exit(1)

start_time = ticks_ms()

# with bit banging one pin is used for input and output
pic = PIC18FJ5x(mclr=Pin(22), icspclk=Pin(18), icspdat=Pin(16), led=Pin(25))

print('Programming 18F27j53 PIC in LVP-mode')

# ----- Start the actual programming -----
# set PIC in LVP-mode, check if target supported, transfer hex file
pic.lvp_start()                                 # init LVP
if not pic.lvp_check_target() is None:
    if pic.lvp_load_hexfile(hexfile):           # transfer contents to PIC
        print("Programming finished successfully")
        print("Execution time {:.2f} seconds".format(ticks_diff(ticks_ms(), start_time) / 1000))
        pic.led.off()                           # LED off
    else:
        print("Programming failed, see previous message(s)")
        pic.led.on()                            # Signal that programming failed

pic.lvp_stop()                                  # de-init LVP

#
