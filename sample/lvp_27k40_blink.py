"""
lvp_27k40_rp2_blink.py

Micropython script for programming PIC18F27K40 in LVP mode with RP2040

Author: Rob Hamerling; Copyright (C). All rights reserved 2019..2024.

License: MIT License
"""

from os import stat
from sys import exit
from machine import Pin, SPI
from time import ticks_ms, ticks_diff, sleep

from lvp_pic import PIC18FK40    # class for this family


hexfile = "18f27k40_blink_intosc.hex"
# Check if specified hexfile is present
try:
    stat(hexfile)                       # Check if specified hexfile is present
except:
    print('==> ERR: IntelHex file', hexfile, 'not found')
    exit(1)

start_time = ticks_ms()

# with SPI module 0 of RP2 Pico defaults: CLK=18, MOSI=19, MISO=16
bus = SPI(0)                                    # hardware SPI module 0
bus.init(baudrate=4000000, polarity=0, phase=1, firstbit=SPI.MSB)    # 4 Mbps

pic = PIC18FK40(bus, mclr=Pin(22), led=Pin(25))

print("Programming 18F27K40 PIC in LVP-mode with hardware SPI")

# ----- Start the actual programming -----
# set PIC in LVP-mode, check if target supported, transfer hex file
pic.lvp_start()                                 # init LVP
target = pic.lvp_check_target()
if not target is None:
    if pic.lvp_load_hexfile(hexfile):           # transfer contents to PIC
        print("Programming finished successfully")
        print("Execution time {:.2f} seconds".format(ticks_diff(ticks_ms(), start_time) / 1000))
        pic.led.off()                           # LED off
    else:
        print("Programming failed, see previous message(s)")
        pic.led.on()                            # Signal that programming failed
pic.lvp_stop()                                  # de-init LVP

#
