#!/usr/bin/python3

# pgmspec_fam.py
# Analyse PIC pgmspecs for programming algoritm:
# - Convert PgmSpec from pdf to txt
# - Scan text file for programming algoritm (algoritm temporary not used)
# - Collect PIC family (pgmspec title)
# generate dictionary with (pgmspec : pgmspec-title)


import os
import sys
import json
from shutil import copy2

import picpgmdataclass              # module with required basic functions
import picpgmcompact

def do_convert(pdf,txt):
    cmd = f"pdftotext -layout -enc 'UTF-8' {pdf} {txt}"
    output = os.popen(cmd)                  # issue command
    loglist = output.readlines()            # list of (stdout) lines
    rc = output.close()
    print("".join(loglist))                 # show output
    return rc

def check_pgmspec(txt):
    fp = open(txt, 'r')
    for i in range(5):
        ln = fp.readline()
        if (ln.find('Programming Specification') >= 0 or
            ln.find('Memory Programming') >= 0):
            fp.close()
            break
    else:
        fp.close()
        os.unlink(txt)
        print(os.path.split(txt)[1], 'not a pgmspec, removed')

def find_alg(txt):
    # search for programming algorithms
    # if not txt.endswith('1766'):
    #     return False
    alg = '?'
    Terab = 0
    pgmspec = int(os.path.split(txt)[-1])
    with open(txt, 'r') as fp:
        lines = fp.readlines()
        title = lines[0].strip().split()
        family = title[1] if len(title[0]) <= 4 else title[0]
        for ln in lines:
            ln.strip()
            if alg == '?' and ln.find('4-bit Command') >= 0:
                alg = 4
            if alg == '?' and ln.find('six bits in') >= 0:
                alg = 6
            elif alg == '?' and ln.find('eight bits in') >= 0:
                alg = 8
            if ((ln.find('Bulk Erase Cycle') >= 0) or
                 ln.startswith('TERAB')):
                ll = ln.split()
                try:
                    xms = ll.index('ms')
                    Terab = int(eval(ll[xms-1]) * 1000)
                except ValueError:
                    pass

    # print(f' {alg}  {pgmspec} : "{family:s}"', end='')
    # print('   ' if Terab == 0 else f'   {Terab=}')
    return pgmspec, family


if __name__ == "__main__":
    src = os.path.join('/', 'media', 'rob', 'XSA', 'picdatasheets')
    dst = os.path.join('/', 'home', 'rob', 'py', 'picpgm', 'pgmspectext')
    lst = os.listdir(src)

    # create txt files when not present
    if len(os.listdir(dst)) < 80:
        for f in os.listdir(src):
            if not f.startswith(('3000', '4000')):
                continue
            txt = os.path.join(dst, os.path.splitext(f)[0][:-1])
            if not os.path.exists(txt):
                pdf = os.path.join(src, f)
                print(pdf, '-->', txt)
                rc = do_convert(pdf, txt)
                if rc:
                    print('returncode', rc)
                else:
                    check_pgmspec(txt)

    # load list with pgmspecs from picdatasheets
    jsonfile = os.path.join(src, 'pgmspec.json')
    with open(jsonfile, "r") as fp:
        pgmspecs = json.load(fp)                     # obtain pdf info of PICs

    txtlist = os.listdir(dst)

    print(f'pgmspec in {dst}, but not in pgmspec.json:')
    for p in sorted(txtlist):
        if not p in pgmspecs:
            print(f'\t{p}')
    print(f'pgmspec in pgmspec.json, but not in {dst}')
    for p in pgmspecs:
        if not p in txtlist:
            print(f'\t{p}')

    # generate dictionary with (pgmspec : pgmspec title (PIC family)
    filename = "pgmspec_family.py"
    with open(filename, "w") as fp:
        fp.write('pgmspec_family = {\n')
        for p in sorted(txtlist):
            pgmspec, family = find_alg(os.path.join(dst,p))
            fp.write(f'    {pgmspec} : "{family}",\n')
        fp.write('                 }\n')
    print(f'Copying {filename} to lvp directory!')
    copy2(filename, os.path.join('..', '..', 'upy', 'lvp'))


#
