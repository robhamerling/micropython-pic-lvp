#
# PIC programming attributes
#  - all attribute values are presented as string
#  - times are in microseconds
#  - voltages are in millivolts

# Data collected from MPLABX v6.20 dd Mon Nov 11 21:02:27 2024

# User guide:
# The pic programming information file can be loaded as follows:
#     import picpgmdataclassinfo
# This will import a dataclass for every PIC.
# Obtain access to the attributes of a target PIC with e.g.:
#     target = picpgmdataclassinfo.PIC16F18857
# When pictype is a variable like "16F18857" you may need to specify:
#     target = eval("picpgmdataclassinfo.PIC" + pictype)
# The programming attributes of the target are then available
# by name like for example the device ID with:
#     devid = target.devid
# or with:
#     devid = getattr(target, "devid")

from dataclasses import dataclass

@dataclass
class PS30000034:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_unimplval: str = "0"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2180"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "8000"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "5000"
    ProgrammingWaitTime_pgm: str = "8000"
    ProgrammingWaitTime_userid: str = "8000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "12750"
    Wait_cfg: str = "8000"
    Wait_eedata: str = "8000"
    Wait_erase: str = "5000"
    Wait_pgm: str = "8000"
    Wait_userid: str = "8000"
    architecture: str = "16xxxx"
    erasealgo: str = "9"
    fuses_impl: str = "3DFF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0080"
    lvpbit_offset: str = "0"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "30000034"
    tries: str = "1"

@dataclass
class PIC16F627(PS30000034):
    name: str = "16F627"
    devid: str = "0x07A0"

@dataclass
class PIC16F628(PS30000034):
    name: str = "16F628"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_unimplval: str = "0"
    devid: str = "0x07C0"

@dataclass
class PIC16LF627(PS30000034):
    name: str = "16LF627"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x07A0"

@dataclass
class PIC16LF628(PS30000034):
    name: str = "16LF628"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_unimplval: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x07C0"


@dataclass
class PS30000262:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2140"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_eedata: str = "10000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_pgm: str = "10000"
    ProgrammingWaitTime_userid: str = "10000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "6000"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "14000"
    VPP_minvoltage: str = "12000"
    Wait_cfg: str = "10000"
    Wait_eedata: str = "10000"
    Wait_erase: str = "10000"
    Wait_pgm: str = "10000"
    Wait_userid: str = "10000"
    architecture: str = "16xxxx"
    erasealgo: str = "8"
    fuses_impl: str = "3FFF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    lvpthresh: str = "4500"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pgmspec: str = "30000262"
    tries: str = "1"

@dataclass
class PIC16F83(PS30000262):
    name: str = "16F83"
    CodeSector_endaddr: str = "0x200"
    DeviceIDSector_beginaddr: str = "0"
    DeviceIDSector_endaddr: str = "0"
    devid: str = "0x0000"

@dataclass
class PIC16F84(PS30000262):
    name: str = "16F84"
    DeviceIDSector_beginaddr: str = "0"
    DeviceIDSector_endaddr: str = "0"
    devid: str = "0x0000"

@dataclass
class PIC16F84A(PS30000262):
    name: str = "16F84A"
    ProgrammingWaitTime_cfg: str = "4000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "4000"
    ProgrammingWaitTime_pgm: str = "4000"
    ProgrammingWaitTime_userid: str = "4000"
    VDD_maxvoltage: str = "5500"
    Wait_cfg: str = "4000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "4000"
    Wait_pgm: str = "4000"
    Wait_userid: str = "4000"
    devid: str = "0x0560"
    mask: str = "0x3FE0"

@dataclass
class PIC16LF83(PS30000262):
    name: str = "16LF83"
    CodeSector_endaddr: str = "0x200"
    DeviceIDSector_beginaddr: str = "0"
    DeviceIDSector_endaddr: str = "0"
    VDD_maxdefaultvoltage: str = "6000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0000"

@dataclass
class PIC16LF84(PS30000262):
    name: str = "16LF84"
    DeviceIDSector_beginaddr: str = "0"
    DeviceIDSector_endaddr: str = "0"
    VDD_maxdefaultvoltage: str = "6000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0000"

@dataclass
class PIC16LF84A(PS30000262):
    name: str = "16LF84A"
    ProgrammingWaitTime_cfg: str = "4000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "4000"
    ProgrammingWaitTime_pgm: str = "4000"
    ProgrammingWaitTime_userid: str = "4000"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    Wait_cfg: str = "4000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "4000"
    Wait_pgm: str = "4000"
    Wait_userid: str = "4000"
    devid: str = "0x0560"
    mask: str = "0x3FE0"


@dataclass
class PS30000324:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "2"
    Latches_userid: str = "2"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "2"
    ProgrammingRowSize_userid: str = "2"
    ProgrammingWaitTime_cfg: str = "1000"
    ProgrammingWaitTime_erase: str = "30000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "1000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "6000"
    VDD_mindefaultvoltage: str = "4000"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "12750"
    Wait_cfg: str = "1000"
    Wait_erase: str = "30000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "1000"
    architecture: str = "16xxxx"
    erasealgo: str = "1"
    fuses_impl: str = "005F"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "30000324"
    tries: str = "1"

@dataclass
class PIC16F73(PS30000324):
    name: str = "16F73"
    devid: str = "0x0600"

@dataclass
class PIC16F74(PS30000324):
    name: str = "16F74"
    devid: str = "0x0620"

@dataclass
class PIC16F76(PS30000324):
    name: str = "16F76"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x0640"

@dataclass
class PIC16F77(PS30000324):
    name: str = "16F77"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x0660"

@dataclass
class PIC16LF73(PS30000324):
    name: str = "16LF73"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2500"
    devid: str = "0x0600"

@dataclass
class PIC16LF74(PS30000324):
    name: str = "16LF74"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2500"
    devid: str = "0x0620"

@dataclass
class PIC16LF76(PS30000324):
    name: str = "16LF76"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2500"
    devid: str = "0x0640"

@dataclass
class PIC16LF77(PS30000324):
    name: str = "16LF77"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2500"
    devid: str = "0x0660"


@dataclass
class PS30000480:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x3000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "5000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "5000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00270F0F000185000FC00FE00F40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "8192"
    pgmspec: str = "30000480"
    tries: str = "1"

@dataclass
class PIC18F2439(PS30000480):
    name: str = "18F2439"
    devid: str = "0x0480"

@dataclass
class PIC18F2539(PS30000480):
    name: str = "18F2539"
    CodeSector_endaddr: str = "0x6000"
    devid: str = "0x0400"

@dataclass
class PIC18F4439(PS30000480):
    name: str = "18F4439"
    devid: str = "0x04A0"

@dataclass
class PIC18F4539(PS30000480):
    name: str = "18F4539"
    CodeSector_endaddr: str = "0x6000"
    devid: str = "0x0420"

@dataclass
class PIC18LF2439(PS30000480):
    name: str = "18LF2439"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0480"

@dataclass
class PIC18LF2539(PS30000480):
    name: str = "18LF2539"
    CodeSector_endaddr: str = "0x6000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0400"

@dataclass
class PIC18LF4439(PS30000480):
    name: str = "18LF4439"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x04A0"

@dataclass
class PIC18LF4539(PS30000480):
    name: str = "18LF4539"
    CodeSector_endaddr: str = "0x6000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0420"


@dataclass
class PS30000492:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "2"
    Latches_userid: str = "2"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "2"
    ProgrammingRowSize_userid: str = "2"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "30000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13500"
    VPP_minvoltage: str = "12500"
    Wait_cfg: str = "2000"
    Wait_erase: str = "30000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "2000"
    architecture: str = "16xxxx"
    erasealgo: str = "1"
    fuses_impl: str = "39FF0043"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    lvpthresh: str = "4750"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "30000492"
    tries: str = "1"

@dataclass
class PIC16F737(PS30000492):
    name: str = "16F737"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x0BA0"

@dataclass
class PIC16F747(PS30000492):
    name: str = "16F747"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x0BE0"

@dataclass
class PIC16F767(PS30000492):
    name: str = "16F767"
    devid: str = "0x0EA0"

@dataclass
class PIC16F777(PS30000492):
    name: str = "16F777"
    devid: str = "0x0DE0"

@dataclass
class PIC16LF747(PS30000492):
    name: str = "16LF747"
    CodeSector_endaddr: str = "0x1000"
    VDD_mindefaultvoltage: str = "2500"
    VDD_minvoltage: str = "2500"
    devid: str = "0x0BE0"

@dataclass
class PIC16LF767(PS30000492):
    name: str = "16LF767"
    VDD_mindefaultvoltage: str = "2500"
    VDD_minvoltage: str = "2500"
    devid: str = "0x0EA0"

@dataclass
class PIC16LF777(PS30000492):
    name: str = "16LF777"
    VDD_mindefaultvoltage: str = "2500"
    VDD_minvoltage: str = "2500"
    devid: str = "0x0DE0"


@dataclass
class PS30000499:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0xc000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "002F0F1F838385000FC00FE00F40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "8192"
    pgmspec: str = "30000499"
    tries: str = "1"

@dataclass
class PIC18F6525(PS30000499):
    name: str = "18F6525"
    devid: str = "0x0AE0"

@dataclass
class PIC18F6621(PS30000499):
    name: str = "18F6621"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x0AA0"

@dataclass
class PIC18F8525(PS30000499):
    name: str = "18F8525"
    devid: str = "0x0AC0"

@dataclass
class PIC18F8621(PS30000499):
    name: str = "18F8621"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x0A80"

@dataclass
class PIC18LF6525(PS30000499):
    name: str = "18LF6525"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0AE0"

@dataclass
class PIC18LF6621(PS30000499):
    name: str = "18LF6621"
    CodeSector_endaddr: str = "0x10000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0AA0"

@dataclass
class PIC18LF8525(PS30000499):
    name: str = "18LF8525"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0AC0"

@dataclass
class PIC18LF8621(PS30000499):
    name: str = "18LF8621"
    CodeSector_endaddr: str = "0x10000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0A80"


@dataclass
class PS30000500:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00CF0F3F3C9D85000FC00FE00F40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "8192"
    pgmspec: str = "30000500"
    tries: str = "1"

@dataclass
class PIC18F2331(PS30000500):
    name: str = "18F2331"
    devid: str = "0x08E0"

@dataclass
class PIC18F2431(PS30000500):
    name: str = "18F2431"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x08C0"

@dataclass
class PIC18F4331(PS30000500):
    name: str = "18F4331"
    devid: str = "0x08A0"

@dataclass
class PIC18F4431(PS30000500):
    name: str = "18F4431"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x0880"

@dataclass
class PIC18LF2331(PS30000500):
    name: str = "18LF2331"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x08E0"
    haslvp2: str = "0"

@dataclass
class PIC18LF2431(PS30000500):
    name: str = "18LF2431"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x08C0"
    haslvp2: str = "0"

@dataclass
class PIC18LF4331(PS30000500):
    name: str = "18LF4331"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x08A0"
    haslvp2: str = "0"

@dataclass
class PIC18LF4431(PS30000500):
    name: str = "18LF4431"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0880"
    haslvp2: str = "0"


@dataclass
class PS30000677:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x7ff0"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    Latches_cfg: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "512"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "512"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "600"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3500"
    VDD_mindefaultvoltage: str = "2125"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "3250"
    VPP_defaultvoltage: str = "3250"
    VPP_maxvoltage: str = "3500"
    VPP_minvoltage: str = "2125"
    WORMHoleSector_beginaddr: str = "0x7ff0"
    WORMHoleSector_endaddr: str = "0x8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "600"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    architecture: str = "18xxxx"
    fuses_impl: str = "E0FFAFFF33F0FFF7FBFFFFFF1DF0F8F3"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "0"
    haslvp2: str = "1"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30000677"
    tries: str = "1"

@dataclass
class PIC18F65J94(PS30000677):
    name: str = "18F65J94"
    devid: str = "0x6400"
    fuses_impl: str = "E0FFAFFF33F0FFF703FFFFFF1DF0F8F3"

@dataclass
class PIC18F66J94(PS30000677):
    name: str = "18F66J94"
    CodeSector_endaddr: str = "0xfff0"
    WORMHoleSector_beginaddr: str = "0xfff0"
    WORMHoleSector_endaddr: str = "0x10000"
    devid: str = "0x63E0"
    fuses_impl: str = "E0FFAFFF33F0FFF703FFFFFF1DF0F8F3"

@dataclass
class PIC18F66J99(PS30000677):
    name: str = "18F66J99"
    CodeSector_endaddr: str = "0x17ff0"
    WORMHoleSector_beginaddr: str = "0x17ff0"
    WORMHoleSector_endaddr: str = "0x18000"
    devid: str = "0x63C0"
    fuses_impl: str = "E0FFAFFF33F0FFF703FFFFFF1DF0F8F3"

@dataclass
class PIC18F67J94(PS30000677):
    name: str = "18F67J94"
    CodeSector_endaddr: str = "0x1fff0"
    WORMHoleSector_beginaddr: str = "0x1fff0"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x63A0"
    fuses_impl: str = "E0FFAFFF33F0FFF703FFFFFF1DF0F8F3"

@dataclass
class PIC18F85J94(PS30000677):
    name: str = "18F85J94"
    devid: str = "0x6380"

@dataclass
class PIC18F86J94(PS30000677):
    name: str = "18F86J94"
    CodeSector_endaddr: str = "0xfff0"
    WORMHoleSector_beginaddr: str = "0xfff0"
    WORMHoleSector_endaddr: str = "0x10000"
    devid: str = "0x6360"

@dataclass
class PIC18F86J99(PS30000677):
    name: str = "18F86J99"
    CodeSector_endaddr: str = "0x17ff0"
    WORMHoleSector_beginaddr: str = "0x17ff0"
    WORMHoleSector_endaddr: str = "0x18000"
    devid: str = "0x6340"

@dataclass
class PIC18F87J94(PS30000677):
    name: str = "18F87J94"
    CodeSector_endaddr: str = "0x1fff0"
    WORMHoleSector_beginaddr: str = "0x1fff0"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x6320"

@dataclass
class PIC18F95J94(PS30000677):
    name: str = "18F95J94"
    devid: str = "0x6300"

@dataclass
class PIC18F96J94(PS30000677):
    name: str = "18F96J94"
    CodeSector_endaddr: str = "0xfff0"
    WORMHoleSector_beginaddr: str = "0xfff0"
    WORMHoleSector_endaddr: str = "0x10000"
    devid: str = "0x62E0"

@dataclass
class PIC18F96J99(PS30000677):
    name: str = "18F96J99"
    CodeSector_endaddr: str = "0x17ff0"
    WORMHoleSector_beginaddr: str = "0x17ff0"
    WORMHoleSector_endaddr: str = "0x18000"
    devid: str = "0x62C0"

@dataclass
class PIC18F97J94(PS30000677):
    name: str = "18F97J94"
    CodeSector_endaddr: str = "0x1fff0"
    WORMHoleSector_beginaddr: str = "0x1fff0"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x62A0"


@dataclass
class PS30009025:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2140"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "4000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "4000"
    ProgrammingWaitTime_lvpgm: str = "8000"
    ProgrammingWaitTime_pgm: str = "4000"
    ProgrammingWaitTime_userid: str = "4000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13500"
    VPP_minvoltage: str = "12500"
    Wait_cfg: str = "4000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "4000"
    Wait_lvpgm: str = "8000"
    Wait_pgm: str = "4000"
    Wait_userid: str = "4000"
    architecture: str = "16xxxx"
    erasealgo: str = "3"
    fuses_impl: str = "3BFF"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "0"
    lvpbit_mask: str = "0x0080"
    lvpbit_offset: str = "0"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "30009025"
    tries: str = "1"

@dataclass
class PIC16F870(PS30009025):
    name: str = "16F870"
    devid: str = "0x0D00"

@dataclass
class PIC16F871(PS30009025):
    name: str = "16F871"
    devid: str = "0x0D20"

@dataclass
class PIC16F872(PS30009025):
    name: str = "16F872"
    devid: str = "0x08E0"

@dataclass
class PIC16F873(PS30009025):
    name: str = "16F873"
    CodeSector_endaddr: str = "0x1000"
    EEDataSector_endaddr: str = "0x2180"
    devid: str = "0x0960"

@dataclass
class PIC16F874(PS30009025):
    name: str = "16F874"
    CodeSector_endaddr: str = "0x1000"
    EEDataSector_endaddr: str = "0x2180"
    devid: str = "0x0920"

@dataclass
class PIC16F876(PS30009025):
    name: str = "16F876"
    CodeSector_endaddr: str = "0x2000"
    EEDataSector_endaddr: str = "0x2200"
    devid: str = "0x09E0"

@dataclass
class PIC16F877(PS30009025):
    name: str = "16F877"
    CodeSector_endaddr: str = "0x2000"
    EEDataSector_endaddr: str = "0x2200"
    devid: str = "0x09A0"

@dataclass
class PIC16LF870(PS30009025):
    name: str = "16LF870"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0D00"

@dataclass
class PIC16LF871(PS30009025):
    name: str = "16LF871"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0D20"

@dataclass
class PIC16LF872(PS30009025):
    name: str = "16LF872"
    VDD_mindefaultvoltage: str = "2200"
    VDD_minvoltage: str = "2200"
    devid: str = "0x08E0"

@dataclass
class PIC16LF873(PS30009025):
    name: str = "16LF873"
    CodeSector_endaddr: str = "0x1000"
    EEDataSector_endaddr: str = "0x2180"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0960"

@dataclass
class PIC16LF874(PS30009025):
    name: str = "16LF874"
    CodeSector_endaddr: str = "0x1000"
    EEDataSector_endaddr: str = "0x2180"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0920"

@dataclass
class PIC16LF876(PS30009025):
    name: str = "16LF876"
    CodeSector_endaddr: str = "0x2000"
    EEDataSector_endaddr: str = "0x2200"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x09E0"

@dataclass
class PIC16LF877(PS30009025):
    name: str = "16LF877"
    CodeSector_endaddr: str = "0x2000"
    EEDataSector_endaddr: str = "0x2200"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x09A0"


@dataclass
class PS30009576:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x4000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "5000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "5000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00270F0F000185000FC00FE00F40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "8192"
    pgmspec: str = "30009576"
    tries: str = "1"

@dataclass
class PIC18F242(PS30009576):
    name: str = "18F242"
    devid: str = "0x0480"

@dataclass
class PIC18F248(PS30009576):
    name: str = "18F248"
    devid: str = "0x0800"
    fuses_impl: str = "00270F0F000085000FC00FE00F40"

@dataclass
class PIC18F252(PS30009576):
    name: str = "18F252"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x0400"

@dataclass
class PIC18F258(PS30009576):
    name: str = "18F258"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x0840"
    fuses_impl: str = "00270F0F000085000FC00FE00F40"

@dataclass
class PIC18F442(PS30009576):
    name: str = "18F442"
    devid: str = "0x04A0"

@dataclass
class PIC18F448(PS30009576):
    name: str = "18F448"
    devid: str = "0x0820"
    fuses_impl: str = "00270F0F000085000FC00FE00F40"

@dataclass
class PIC18F452(PS30009576):
    name: str = "18F452"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x0420"

@dataclass
class PIC18F458(PS30009576):
    name: str = "18F458"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x0860"
    fuses_impl: str = "00270F0F000085000FC00FE00F40"

@dataclass
class PIC18LF242(PS30009576):
    name: str = "18LF242"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0480"

@dataclass
class PIC18LF248(PS30009576):
    name: str = "18LF248"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0800"
    fuses_impl: str = "00270F0F000085000FC00FE00F40"

@dataclass
class PIC18LF252(PS30009576):
    name: str = "18LF252"
    CodeSector_endaddr: str = "0x8000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0400"

@dataclass
class PIC18LF258(PS30009576):
    name: str = "18LF258"
    CodeSector_endaddr: str = "0x8000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0840"
    fuses_impl: str = "00270F0F000085000FC00FE00F40"

@dataclass
class PIC18LF442(PS30009576):
    name: str = "18LF442"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x04A0"

@dataclass
class PIC18LF448(PS30009576):
    name: str = "18LF448"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0820"
    fuses_impl: str = "00270F0F000085000FC00FE00F40"

@dataclass
class PIC18LF452(PS30009576):
    name: str = "18LF452"
    CodeSector_endaddr: str = "0x8000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0420"

@dataclass
class PIC18LF458(PS30009576):
    name: str = "18LF458"
    CodeSector_endaddr: str = "0x8000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0860"
    fuses_impl: str = "00270F0F000085000FC00FE00F40"


@dataclass
class PS30009583:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00270F0F83018500FFC0FFE0FF40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "8192"
    pgmspec: str = "30009583"
    tries: str = "1"

@dataclass
class PIC18F6520(PS30009583):
    name: str = "18F6520"
    devid: str = "0x0B20"
    fuses_impl: str = "00270F0F83038500FFC0FFE0FF40"

@dataclass
class PIC18F6620(PS30009583):
    name: str = "18F6620"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x0660"

@dataclass
class PIC18F6720(PS30009583):
    name: str = "18F6720"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x0620"

@dataclass
class PIC18F8520(PS30009583):
    name: str = "18F8520"
    devid: str = "0x0B00"
    fuses_impl: str = "00270F0F83038500FFC0FFE0FF40"

@dataclass
class PIC18F8620(PS30009583):
    name: str = "18F8620"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x0640"

@dataclass
class PIC18F8720(PS30009583):
    name: str = "18F8720"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x0600"

@dataclass
class PIC18LF6520(PS30009583):
    name: str = "18LF6520"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0B20"
    fuses_impl: str = "00270F0F83038500FFC0FFE0FF40"

@dataclass
class PIC18LF6620(PS30009583):
    name: str = "18LF6620"
    CodeSector_endaddr: str = "0x10000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0660"

@dataclass
class PIC18LF6720(PS30009583):
    name: str = "18LF6720"
    CodeSector_endaddr: str = "0x20000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0620"

@dataclass
class PIC18LF8520(PS30009583):
    name: str = "18LF8520"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0B00"
    fuses_impl: str = "00270F0F83038500FFC0FFE0FF40"

@dataclass
class PIC18LF8620(PS30009583):
    name: str = "18LF8620"
    CodeSector_endaddr: str = "0x10000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0640"

@dataclass
class PIC18LF8720(PS30009583):
    name: str = "18LF8720"
    CodeSector_endaddr: str = "0x20000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0600"


@dataclass
class PS30009588:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "2"
    Latches_userid: str = "2"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "2"
    ProgrammingRowSize_userid: str = "2"
    ProgrammingWaitTime_cfg: str = "3000"
    ProgrammingWaitTime_erase: str = "30000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "3000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "6000"
    VDD_mindefaultvoltage: str = "4000"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "12750"
    Wait_cfg: str = "3000"
    Wait_erase: str = "30000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "3000"
    architecture: str = "16xxxx"
    erasealgo: str = "10"
    fuses_impl: str = "005F"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "30009588"
    tries: str = "1"

@dataclass
class PIC16F72(PS30009588):
    name: str = "16F72"
    devid: str = "0x00A0"


@dataclass
class PS30009589:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2180"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "8"
    Latches_userid: str = "4"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "8"
    ProgrammingRowSize_userid: str = "4"
    ProgrammingWaitTime_cfg: str = "8000"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "8000"
    ProgrammingWaitTime_lvpgm: str = "8000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "8000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13500"
    VPP_minvoltage: str = "12500"
    Wait_cfg: str = "8000"
    Wait_eedata: str = "8000"
    Wait_erase: str = "8000"
    Wait_lvpgm: str = "8000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "8000"
    architecture: str = "16xxxx"
    erasealgo: str = "2"
    fuses_impl: str = "2FCF"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "0"
    lvpbit_mask: str = "0x0080"
    lvpbit_offset: str = "0"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "30009589"
    tries: str = "1"

@dataclass
class PIC16F873A(PS30009589):
    name: str = "16F873A"
    devid: str = "0x0E40"

@dataclass
class PIC16F874A(PS30009589):
    name: str = "16F874A"
    devid: str = "0x0E60"

@dataclass
class PIC16F876A(PS30009589):
    name: str = "16F876A"
    CodeSector_endaddr: str = "0x2000"
    EEDataSector_endaddr: str = "0x2200"
    devid: str = "0x0E00"

@dataclass
class PIC16F877A(PS30009589):
    name: str = "16F877A"
    CodeSector_endaddr: str = "0x2000"
    EEDataSector_endaddr: str = "0x2200"
    devid: str = "0x0E20"

@dataclass
class PIC16LF873A(PS30009589):
    name: str = "16LF873A"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0E40"

@dataclass
class PIC16LF874A(PS30009589):
    name: str = "16LF874A"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0E60"

@dataclass
class PIC16LF876A(PS30009589):
    name: str = "16LF876A"
    CodeSector_endaddr: str = "0x2000"
    EEDataSector_endaddr: str = "0x2200"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0E00"

@dataclass
class PIC16LF877A(PS30009589):
    name: str = "16LF877A"
    CodeSector_endaddr: str = "0x2000"
    EEDataSector_endaddr: str = "0x2200"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0E20"


@dataclass
class PS30009592:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00CF0F1F008385000FC00FE00F40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "4096"
    pgmspec: str = "30009592"
    tries: str = "1"

@dataclass
class PIC18F1220(PS30009592):
    name: str = "18F1220"
    devid: str = "0x07E0"
    fuses_impl: str = "00CF0F1F0080850003C003E00340"

@dataclass
class PIC18F1320(PS30009592):
    name: str = "18F1320"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x07C0"
    fuses_impl: str = "00CF0F1F0080850003C003E00340"
    panelsize: str = "8192"

@dataclass
class PIC18F2220(PS30009592):
    name: str = "18F2220"
    devid: str = "0x0580"

@dataclass
class PIC18F2320(PS30009592):
    name: str = "18F2320"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x0500"
    panelsize: str = "8192"

@dataclass
class PIC18F4220(PS30009592):
    name: str = "18F4220"
    devid: str = "0x05A0"

@dataclass
class PIC18F4320(PS30009592):
    name: str = "18F4320"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x0520"
    panelsize: str = "8192"

@dataclass
class PIC18LF1220(PS30009592):
    name: str = "18LF1220"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x07E0"
    fuses_impl: str = "00CF0F1F0080850003C003E00340"

@dataclass
class PIC18LF1320(PS30009592):
    name: str = "18LF1320"
    CodeSector_endaddr: str = "0x2000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x07C0"
    fuses_impl: str = "00CF0F1F0080850003C003E00340"
    panelsize: str = "8192"

@dataclass
class PIC18LF2220(PS30009592):
    name: str = "18LF2220"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0580"

@dataclass
class PIC18LF2320(PS30009592):
    name: str = "18LF2320"
    CodeSector_endaddr: str = "0x2000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0500"
    panelsize: str = "8192"

@dataclass
class PIC18LF4220(PS30009592):
    name: str = "18LF4220"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x05A0"

@dataclass
class PIC18LF4320(PS30009592):
    name: str = "18LF4320"
    CodeSector_endaddr: str = "0x2000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0520"
    panelsize: str = "8192"


@dataclass
class PS30009603:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2180"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "4"
    Latches_rowerase: str = "32"
    Latches_userid: str = "4"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "4"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_eedata: str = "2000"
    ProgrammingWaitTime_erase: str = "8000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "12000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "2000"
    Wait_eedata: str = "2000"
    Wait_erase: str = "8000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "2000"
    architecture: str = "16xxxx"
    erasealgo: str = "2"
    fuses_impl: str = "3FFF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    lvpbit_mask: str = "0x0080"
    lvpbit_offset: str = "0"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "30009603"
    tries: str = "1"

@dataclass
class PIC16F818(PS30009603):
    name: str = "16F818"
    devid: str = "0x04C0"

@dataclass
class PIC16F819(PS30009603):
    name: str = "16F819"
    CodeSector_endaddr: str = "0x800"
    EEDataSector_endaddr: str = "0x2200"
    devid: str = "0x04E0"

@dataclass
class PIC16LF818(PS30009603):
    name: str = "16LF818"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x04C0"

@dataclass
class PIC16LF819(PS30009603):
    name: str = "16LF819"
    CodeSector_endaddr: str = "0x800"
    EEDataSector_endaddr: str = "0x2200"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x04E0"


@dataclass
class PS30009606:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0xc000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "002F0F1F838385000FC00FE00F40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "8192"
    pgmspec: str = "30009606"
    tries: str = "1"

@dataclass
class PIC18F6585(PS30009606):
    name: str = "18F6585"
    devid: str = "0x0A60"

@dataclass
class PIC18F6680(PS30009606):
    name: str = "18F6680"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x0A20"

@dataclass
class PIC18F8585(PS30009606):
    name: str = "18F8585"
    devid: str = "0x0A40"

@dataclass
class PIC18F8680(PS30009606):
    name: str = "18F8680"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x0A00"

@dataclass
class PIC18LF6585(PS30009606):
    name: str = "18LF6585"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0A60"

@dataclass
class PIC18LF6680(PS30009606):
    name: str = "18LF6680"
    CodeSector_endaddr: str = "0x10000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0A20"

@dataclass
class PIC18LF8585(PS30009606):
    name: str = "18LF8585"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0A40"

@dataclass
class PIC18LF8680(PS30009606):
    name: str = "18LF8680"
    CodeSector_endaddr: str = "0x10000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0A00"


@dataclass
class PS30009607:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2200"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "4"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "1000"
    ProgrammingWaitTime_eedata: str = "1000"
    ProgrammingWaitTime_erase: str = "8000"
    ProgrammingWaitTime_lverase: str = "2000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "1000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13500"
    VPP_minvoltage: str = "12500"
    Wait_cfg: str = "1000"
    Wait_eedata: str = "1000"
    Wait_erase: str = "8000"
    Wait_lverase: str = "2000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "1000"
    architecture: str = "16xxxx"
    erasealgo: str = "2"
    fuses_impl: str = "3FFF0003"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    lvpbit_mask: str = "0x0080"
    lvpbit_offset: str = "0"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "30009607"
    tries: str = "1"

@dataclass
class PIC16F87(PS30009607):
    name: str = "16F87"
    devid: str = "0x0720"

@dataclass
class PIC16F88(PS30009607):
    name: str = "16F88"
    devid: str = "0x0760"

@dataclass
class PIC16LF87(PS30009607):
    name: str = "16LF87"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0720"

@dataclass
class PIC16LF88(PS30009607):
    name: str = "16LF88"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0760"


@dataclass
class PS30009622:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "12000"
    VPP_maxvoltage: str = "12500"
    VPP_minvoltage: str = "9500"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00CF1F1F0087C5000FC00FE00F40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "3000"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30009622"
    tries: str = "1"

@dataclass
class PIC18F2221(PS30009622):
    name: str = "18F2221"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    devid: str = "0x2160"
    fuses_impl: str = "00CF1F1F0087FD0003C003E00340"

@dataclass
class PIC18F2321(PS30009622):
    name: str = "18F2321"
    CodeSector_endaddr: str = "0x2000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    devid: str = "0x2120"
    fuses_impl: str = "00CF1F1F0087FD0003C003E00340"

@dataclass
class PIC18F2410(PS30009622):
    name: str = "18F2410"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x1160"

@dataclass
class PIC18F2420(PS30009622):
    name: str = "18F2420"
    CodeSector_endaddr: str = "0x4000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1140"
    fuses_impl: str = "00CF1F1F0087C50003C003E00340"

@dataclass
class PIC18F2423(PS30009622):
    name: str = "18F2423"
    CodeSector_endaddr: str = "0x4000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1150"
    fuses_impl: str = "00CF1F1F0087C50003C003E00340"
    mask: str = "0xFFF0"

@dataclass
class PIC18F2450(PS30009622):
    name: str = "18F2450"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "16"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x2420"
    fuses_impl: str = "3FCF3F1F0086ED00034003600340"

@dataclass
class PIC18F2455(PS30009622):
    name: str = "18F2455"
    CodeSector_endaddr: str = "0x6000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1260"
    fuses_impl: str = "3FCF3F1F0087E50007C007E00740"

@dataclass
class PIC18F2458(PS30009622):
    name: str = "18F2458"
    CodeSector_endaddr: str = "0x6000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x2A60"
    fuses_impl: str = "3FCF3F1F0087E50007C007E00740"

@dataclass
class PIC18F2480(PS30009622):
    name: str = "18F2480"
    CodeSector_endaddr: str = "0x4000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1AE0"
    fuses_impl: str = "00CF1F1F0086D50003C003E00340"

@dataclass
class PIC18F2510(PS30009622):
    name: str = "18F2510"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x1120"

@dataclass
class PIC18F2515(PS30009622):
    name: str = "18F2515"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "64"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x0CE0"

@dataclass
class PIC18F2520(PS30009622):
    name: str = "18F2520"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1100"

@dataclass
class PIC18F2523(PS30009622):
    name: str = "18F2523"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1110"
    mask: str = "0xFFF0"

@dataclass
class PIC18F2525(PS30009622):
    name: str = "18F2525"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x0CC0"
    fuses_impl: str = "00CF1F1F0087C50007C007E00740"

@dataclass
class PIC18F2550(PS30009622):
    name: str = "18F2550"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1240"
    fuses_impl: str = "3FCF3F1F0087E5000FC00FE00F40"

@dataclass
class PIC18F2553(PS30009622):
    name: str = "18F2553"
    VDD_minvoltage: str = "2125"
    devid: str = "0x2A40"
    fuses_impl: str = "3FCF3F1F0087E5000FC00FE00F40"

@dataclass
class PIC18F2580(PS30009622):
    name: str = "18F2580"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1AC0"
    fuses_impl: str = "00CF1F1F0086D5000FC00FE00F40"

@dataclass
class PIC18F2585(PS30009622):
    name: str = "18F2585"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x0EE0"
    fuses_impl: str = "00CF1F1F0086F5000FC00FE00F40"

@dataclass
class PIC18F2610(PS30009622):
    name: str = "18F2610"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "64"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x0CA0"

@dataclass
class PIC18F2620(PS30009622):
    name: str = "18F2620"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x0C80"

@dataclass
class PIC18F2680(PS30009622):
    name: str = "18F2680"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x0EC0"
    fuses_impl: str = "00CF1F1F0086F5000FC00FE00F40"

@dataclass
class PIC18F2682(PS30009622):
    name: str = "18F2682"
    CodeSector_endaddr: str = "0x14000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x2700"
    fuses_impl: str = "00CF1F1F0086F5003FC03FE03F40"
    panelsize: str = "81919"

@dataclass
class PIC18F2685(PS30009622):
    name: str = "18F2685"
    CodeSector_endaddr: str = "0x18000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x2720"
    fuses_impl: str = "00CF1F1F0086F5003FC03FE03F40"
    panelsize: str = "98303"

@dataclass
class PIC18F4221(PS30009622):
    name: str = "18F4221"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    devid: str = "0x2140"
    fuses_impl: str = "00CF1F1F0087FD0003C003E00340"

@dataclass
class PIC18F4321(PS30009622):
    name: str = "18F4321"
    CodeSector_endaddr: str = "0x2000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    devid: str = "0x2100"
    fuses_impl: str = "00CF1F1F0087FD0003C003E00340"

@dataclass
class PIC18F4410(PS30009622):
    name: str = "18F4410"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x10E0"

@dataclass
class PIC18F4420(PS30009622):
    name: str = "18F4420"
    CodeSector_endaddr: str = "0x4000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x10C0"
    fuses_impl: str = "00CF1F1F0087C50003C003E00340"

@dataclass
class PIC18F4423(PS30009622):
    name: str = "18F4423"
    CodeSector_endaddr: str = "0x4000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x10D0"
    fuses_impl: str = "00CF1F1F0087C50003C003E00340"
    mask: str = "0xFFF0"

@dataclass
class PIC18F4450(PS30009622):
    name: str = "18F4450"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "16"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x2400"
    fuses_impl: str = "3FCF3F1F0086ED00034003600340"

@dataclass
class PIC18F4455(PS30009622):
    name: str = "18F4455"
    CodeSector_endaddr: str = "0x6000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1220"
    fuses_impl: str = "3FCF3F1F0087E50007C007E00740"

@dataclass
class PIC18F4458(PS30009622):
    name: str = "18F4458"
    CodeSector_endaddr: str = "0x6000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x2A20"
    fuses_impl: str = "3FCF3F1F0087E50007C007E00740"

@dataclass
class PIC18F4480(PS30009622):
    name: str = "18F4480"
    CodeSector_endaddr: str = "0x4000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1AA0"
    fuses_impl: str = "00CF1F1F0086D50003C003E00340"

@dataclass
class PIC18F4510(PS30009622):
    name: str = "18F4510"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x10A0"

@dataclass
class PIC18F4515(PS30009622):
    name: str = "18F4515"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "64"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x0C60"

@dataclass
class PIC18F4520(PS30009622):
    name: str = "18F4520"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1080"

@dataclass
class PIC18F4523(PS30009622):
    name: str = "18F4523"
    ProgrammingWaitTime_erase: str = "5000"
    VDD_minvoltage: str = "2125"
    Wait_erase: str = "5000"
    devid: str = "0x1090"
    mask: str = "0xFFF0"

@dataclass
class PIC18F4525(PS30009622):
    name: str = "18F4525"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x0C40"
    fuses_impl: str = "00CF1F1F0087C50007C007E00740"

@dataclass
class PIC18F4550(PS30009622):
    name: str = "18F4550"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1200"
    fuses_impl: str = "3FCF3F1F0087E5000FC00FE00F40"

@dataclass
class PIC18F4553(PS30009622):
    name: str = "18F4553"
    VDD_minvoltage: str = "2125"
    devid: str = "0x2A00"
    fuses_impl: str = "3FCF3F1F0087E5000FC00FE00F40"

@dataclass
class PIC18F4580(PS30009622):
    name: str = "18F4580"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1A80"
    fuses_impl: str = "00CF1F1F0086D5000FC00FE00F40"

@dataclass
class PIC18F4585(PS30009622):
    name: str = "18F4585"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x0EA0"
    fuses_impl: str = "00CF1F1F0086F5000FC00FE00F40"

@dataclass
class PIC18F4610(PS30009622):
    name: str = "18F4610"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "64"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_minvoltage: str = "2125"
    Wait_eedata: str = "0"
    devid: str = "0x0C20"

@dataclass
class PIC18F4620(PS30009622):
    name: str = "18F4620"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x0C00"

@dataclass
class PIC18F4680(PS30009622):
    name: str = "18F4680"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x0E80"
    fuses_impl: str = "00CF1F1F0086F5000FC00FE00F40"

@dataclass
class PIC18F4682(PS30009622):
    name: str = "18F4682"
    CodeSector_endaddr: str = "0x14000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x2740"
    fuses_impl: str = "00CF1F1F0086F5003FC03FE03F40"
    panelsize: str = "81919"

@dataclass
class PIC18F4685(PS30009622):
    name: str = "18F4685"
    CodeSector_endaddr: str = "0x18000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_minvoltage: str = "2125"
    devid: str = "0x2760"
    fuses_impl: str = "00CF1F1F0086F5003FC03FE03F40"
    panelsize: str = "98303"

@dataclass
class PIC18LF2221(PS30009622):
    name: str = "18LF2221"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_mindefaultvoltage: str = "2000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    devid: str = "0x2160"
    fuses_impl: str = "00CF1F1F0087FD0003C003E00340"

@dataclass
class PIC18LF2321(PS30009622):
    name: str = "18LF2321"
    CodeSector_endaddr: str = "0x2000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_mindefaultvoltage: str = "2000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    devid: str = "0x2120"
    fuses_impl: str = "00CF1F1F0087FD0003C003E00340"

@dataclass
class PIC18LF2410(PS30009622):
    name: str = "18LF2410"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x1160"

@dataclass
class PIC18LF2420(PS30009622):
    name: str = "18LF2420"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1140"
    fuses_impl: str = "00CF1F1F0087C50003C003E00340"

@dataclass
class PIC18LF2423(PS30009622):
    name: str = "18LF2423"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1150"
    fuses_impl: str = "00CF1F1F0087C50003C003E00340"
    mask: str = "0xFFF0"

@dataclass
class PIC18LF2450(PS30009622):
    name: str = "18LF2450"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "16"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x2420"
    fuses_impl: str = "3FCF3F1F0086ED00034003600340"

@dataclass
class PIC18LF2455(PS30009622):
    name: str = "18LF2455"
    CodeSector_endaddr: str = "0x6000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1260"
    fuses_impl: str = "3FCF3F1F0087E50007C007E00740"

@dataclass
class PIC18LF2458(PS30009622):
    name: str = "18LF2458"
    CodeSector_endaddr: str = "0x6000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x2A60"
    fuses_impl: str = "3FCF3F1F0087E50007C007E00740"

@dataclass
class PIC18LF2480(PS30009622):
    name: str = "18LF2480"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1AE0"
    fuses_impl: str = "00CF1F1F0086D50003C003E00340"

@dataclass
class PIC18LF2510(PS30009622):
    name: str = "18LF2510"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x1120"

@dataclass
class PIC18LF2515(PS30009622):
    name: str = "18LF2515"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "64"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x0CE0"

@dataclass
class PIC18LF2520(PS30009622):
    name: str = "18LF2520"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1100"

@dataclass
class PIC18LF2523(PS30009622):
    name: str = "18LF2523"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1110"
    mask: str = "0xFFF0"

@dataclass
class PIC18LF2525(PS30009622):
    name: str = "18LF2525"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0CC0"
    fuses_impl: str = "00CF1F1F0087C50007C007E00740"

@dataclass
class PIC18LF2550(PS30009622):
    name: str = "18LF2550"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1240"
    fuses_impl: str = "3FCF3F1F0087E5000FC00FE00F40"

@dataclass
class PIC18LF2553(PS30009622):
    name: str = "18LF2553"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x2A40"
    fuses_impl: str = "3FCF3F1F0087E5000FC00FE00F40"

@dataclass
class PIC18LF2580(PS30009622):
    name: str = "18LF2580"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1AC0"
    fuses_impl: str = "00CF1F1F0086D5000FC00FE00F40"

@dataclass
class PIC18LF2585(PS30009622):
    name: str = "18LF2585"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0EE0"
    fuses_impl: str = "00CF1F1F0086F5000FC00FE00F40"

@dataclass
class PIC18LF2610(PS30009622):
    name: str = "18LF2610"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "64"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x0CA0"

@dataclass
class PIC18LF2620(PS30009622):
    name: str = "18LF2620"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0C80"

@dataclass
class PIC18LF2680(PS30009622):
    name: str = "18LF2680"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0EC0"
    fuses_impl: str = "00CF1F1F0086F5000FC00FE00F40"

@dataclass
class PIC18LF2682(PS30009622):
    name: str = "18LF2682"
    CodeSector_endaddr: str = "0x14000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x2700"
    fuses_impl: str = "00CF1F1F0086F5003FC03FE03F40"
    panelsize: str = "81919"

@dataclass
class PIC18LF2685(PS30009622):
    name: str = "18LF2685"
    CodeSector_endaddr: str = "0x18000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x2720"
    fuses_impl: str = "00CF1F1F0086F5003FC03FE03F40"
    panelsize: str = "98303"

@dataclass
class PIC18LF4221(PS30009622):
    name: str = "18LF4221"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_mindefaultvoltage: str = "2000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    devid: str = "0x2140"
    fuses_impl: str = "00CF1F1F0087FD0003C003E00340"

@dataclass
class PIC18LF4321(PS30009622):
    name: str = "18LF4321"
    CodeSector_endaddr: str = "0x2000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_mindefaultvoltage: str = "2000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    devid: str = "0x2100"
    fuses_impl: str = "00CF1F1F0087FD0003C003E00340"

@dataclass
class PIC18LF4410(PS30009622):
    name: str = "18LF4410"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x10E0"

@dataclass
class PIC18LF4420(PS30009622):
    name: str = "18LF4420"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x10C0"
    fuses_impl: str = "00CF1F1F0087C50003C003E00340"

@dataclass
class PIC18LF4423(PS30009622):
    name: str = "18LF4423"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x10D0"
    fuses_impl: str = "00CF1F1F0087C50003C003E00340"
    mask: str = "0xFFF0"

@dataclass
class PIC18LF4450(PS30009622):
    name: str = "18LF4450"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "16"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x2400"
    fuses_impl: str = "3FCF3F1F0086ED00034003600340"

@dataclass
class PIC18LF4455(PS30009622):
    name: str = "18LF4455"
    CodeSector_endaddr: str = "0x6000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1220"
    fuses_impl: str = "3FCF3F1F0087E50007C007E00740"

@dataclass
class PIC18LF4458(PS30009622):
    name: str = "18LF4458"
    CodeSector_endaddr: str = "0x6000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x2A20"
    fuses_impl: str = "3FCF3F1F0087E50007C007E00740"

@dataclass
class PIC18LF4480(PS30009622):
    name: str = "18LF4480"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1AA0"
    fuses_impl: str = "00CF1F1F0086D50003C003E00340"

@dataclass
class PIC18LF4510(PS30009622):
    name: str = "18LF4510"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x10A0"

@dataclass
class PIC18LF4515(PS30009622):
    name: str = "18LF4515"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "64"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x0C60"

@dataclass
class PIC18LF4520(PS30009622):
    name: str = "18LF4520"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1080"

@dataclass
class PIC18LF4523(PS30009622):
    name: str = "18LF4523"
    ProgrammingWaitTime_erase: str = "5000"
    VDD_mindefaultvoltage: str = "2000"
    Wait_erase: str = "5000"
    devid: str = "0x1090"
    mask: str = "0xFFF0"

@dataclass
class PIC18LF4525(PS30009622):
    name: str = "18LF4525"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0C40"
    fuses_impl: str = "00CF1F1F0087C50007C007E00740"

@dataclass
class PIC18LF4550(PS30009622):
    name: str = "18LF4550"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1200"
    fuses_impl: str = "3FCF3F1F0087E5000FC00FE00F40"

@dataclass
class PIC18LF4553(PS30009622):
    name: str = "18LF4553"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x2A00"
    fuses_impl: str = "3FCF3F1F0087E5000FC00FE00F40"

@dataclass
class PIC18LF4580(PS30009622):
    name: str = "18LF4580"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1A80"
    fuses_impl: str = "00CF1F1F0086D5000FC00FE00F40"

@dataclass
class PIC18LF4585(PS30009622):
    name: str = "18LF4585"
    CodeSector_endaddr: str = "0xc000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0EA0"
    fuses_impl: str = "00CF1F1F0086F5000FC00FE00F40"

@dataclass
class PIC18LF4610(PS30009622):
    name: str = "18LF4610"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    Latches_pgm: str = "64"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingWaitTime_eedata: str = "0"
    VDD_mindefaultvoltage: str = "2000"
    Wait_eedata: str = "0"
    devid: str = "0x0C20"

@dataclass
class PIC18LF4620(PS30009622):
    name: str = "18LF4620"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0C00"

@dataclass
class PIC18LF4680(PS30009622):
    name: str = "18LF4680"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x0E80"
    fuses_impl: str = "00CF1F1F0086F5000FC00FE00F40"

@dataclass
class PIC18LF4682(PS30009622):
    name: str = "18LF4682"
    CodeSector_endaddr: str = "0x14000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x2740"
    fuses_impl: str = "00CF1F1F0086F5003FC03FE03F40"
    panelsize: str = "81919"

@dataclass
class PIC18LF4685(PS30009622):
    name: str = "18LF4685"
    CodeSector_endaddr: str = "0x18000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x2760"
    fuses_impl: str = "00CF1F1F0086F5003FC03FE03F40"
    panelsize: str = "98303"


@dataclass
class PS30009624:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    Latches_cfg: str = "2"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "30000"
    ProgrammingWaitTime_lverase: str = "2000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "2000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "30000"
    Wait_lverase: str = "2000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "2000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00CF1F1FC385C1000100000001"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "2750"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30009624"
    tries: str = "1"

@dataclass
class PIC18F6310(PS30009624):
    name: str = "18F6310"
    devid: str = "0x0BE0"

@dataclass
class PIC18F6390(PS30009624):
    name: str = "18F6390"
    devid: str = "0x0BA0"

@dataclass
class PIC18F6393(PS30009624):
    name: str = "18F6393"
    devid: str = "0x1A00"

@dataclass
class PIC18F6410(PS30009624):
    name: str = "18F6410"
    CodeSector_endaddr: str = "0x4000"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    devid: str = "0x06E0"

@dataclass
class PIC18F6490(PS30009624):
    name: str = "18F6490"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x06A0"

@dataclass
class PIC18F6493(PS30009624):
    name: str = "18F6493"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x0E00"

@dataclass
class PIC18F8310(PS30009624):
    name: str = "18F8310"
    devid: str = "0x0BC0"

@dataclass
class PIC18F8390(PS30009624):
    name: str = "18F8390"
    devid: str = "0x0B80"

@dataclass
class PIC18F8393(PS30009624):
    name: str = "18F8393"
    devid: str = "0x1A20"

@dataclass
class PIC18F8410(PS30009624):
    name: str = "18F8410"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x06C0"

@dataclass
class PIC18F8490(PS30009624):
    name: str = "18F8490"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x0680"

@dataclass
class PIC18F8493(PS30009624):
    name: str = "18F8493"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x0E20"

@dataclass
class PIC18LF6310(PS30009624):
    name: str = "18LF6310"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0BE0"

@dataclass
class PIC18LF6390(PS30009624):
    name: str = "18LF6390"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0BA0"

@dataclass
class PIC18LF6393(PS30009624):
    name: str = "18LF6393"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x1A00"

@dataclass
class PIC18LF6410(PS30009624):
    name: str = "18LF6410"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x06E0"

@dataclass
class PIC18LF6490(PS30009624):
    name: str = "18LF6490"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x06A0"

@dataclass
class PIC18LF6493(PS30009624):
    name: str = "18LF6493"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0E00"

@dataclass
class PIC18LF8310(PS30009624):
    name: str = "18LF8310"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0BC0"

@dataclass
class PIC18LF8390(PS30009624):
    name: str = "18LF8390"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0B80"

@dataclass
class PIC18LF8393(PS30009624):
    name: str = "18LF8393"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x1A20"

@dataclass
class PIC18LF8410(PS30009624):
    name: str = "18LF8410"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x06C0"

@dataclass
class PIC18LF8490(PS30009624):
    name: str = "18LF8490"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0680"

@dataclass
class PIC18LF8493(PS30009624):
    name: str = "18LF8493"
    CodeSector_endaddr: str = "0x4000"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    devid: str = "0x0E20"


@dataclass
class PS30009643:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x18000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "10000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11500"
    VPP_maxvoltage: str = "12500"
    VPP_minvoltage: str = "5500"
    Wait_cfg: str = "10000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "10000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00CF1F1F0385F5003FC03FE03F40"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30009643"
    tries: str = "1"

@dataclass
class PIC18F6527(PS30009643):
    name: str = "18F6527"
    CodeSector_endaddr: str = "0xc000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1340"
    fuses_impl: str = "00CF1F1F0385F50007C007E00740"
    panelsize: str = "131072"

@dataclass
class PIC18F6622(PS30009643):
    name: str = "18F6622"
    CodeSector_endaddr: str = "0x10000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1380"
    fuses_impl: str = "00CF1F1F0385F5000FC00FE00F40"

@dataclass
class PIC18F6627(PS30009643):
    name: str = "18F6627"
    VDD_minvoltage: str = "2125"
    devid: str = "0x13C0"
    panelsize: str = "131072"

@dataclass
class PIC18F6628(PS30009643):
    name: str = "18F6628"
    VDD_minvoltage: str = "2125"
    devid: str = "0x49C0"
    panelsize: str = "131072"

@dataclass
class PIC18F6722(PS30009643):
    name: str = "18F6722"
    CodeSector_endaddr: str = "0x20000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1400"
    fuses_impl: str = "00CF1F1F0385F500FFC0FFE0FF40"

@dataclass
class PIC18F6723(PS30009643):
    name: str = "18F6723"
    CodeSector_endaddr: str = "0x20000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x4A00"
    fuses_impl: str = "00CF1F1F0385F500FFC0FFE0FF40"

@dataclass
class PIC18F8527(PS30009643):
    name: str = "18F8527"
    CodeSector_endaddr: str = "0xc000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1360"
    fuses_impl: str = "00CF1F1FF387F50007C007E00740"

@dataclass
class PIC18F8622(PS30009643):
    name: str = "18F8622"
    CodeSector_endaddr: str = "0x10000"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_userid: str = "5000"
    Wait_cfg: str = "5000"
    Wait_userid: str = "5000"
    devid: str = "0x13A0"
    fuses_impl: str = "00CF1F1FF387F5000FC00FE00F40"
    panelsize: str = "8192"

@dataclass
class PIC18F8627(PS30009643):
    name: str = "18F8627"
    VDD_minvoltage: str = "2125"
    devid: str = "0x13E0"
    fuses_impl: str = "00CF1F1FF387F5003FC03FE03F40"

@dataclass
class PIC18F8628(PS30009643):
    name: str = "18F8628"
    VDD_minvoltage: str = "2125"
    devid: str = "0x49E0"
    fuses_impl: str = "00CF1F1FF387F5003FC03FE03F40"

@dataclass
class PIC18F8722(PS30009643):
    name: str = "18F8722"
    CodeSector_endaddr: str = "0x20000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x1420"
    fuses_impl: str = "00CF1F1FF387F500FFC0FFE0FF40"

@dataclass
class PIC18F8723(PS30009643):
    name: str = "18F8723"
    CodeSector_endaddr: str = "0x20000"
    VDD_minvoltage: str = "2125"
    devid: str = "0x4A20"
    fuses_impl: str = "00CF1F1FF387F500FFC0FFE0FF40"

@dataclass
class PIC18LF6527(PS30009643):
    name: str = "18LF6527"
    CodeSector_endaddr: str = "0xc000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1340"
    fuses_impl: str = "00CF1F1F0385F50007C007E00740"
    panelsize: str = "131072"

@dataclass
class PIC18LF6622(PS30009643):
    name: str = "18LF6622"
    CodeSector_endaddr: str = "0x10000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1380"
    fuses_impl: str = "00CF1F1F0385F5000FC00FE00F40"

@dataclass
class PIC18LF6627(PS30009643):
    name: str = "18LF6627"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x13C0"
    panelsize: str = "131072"

@dataclass
class PIC18LF6628(PS30009643):
    name: str = "18LF6628"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x49C0"
    panelsize: str = "131072"

@dataclass
class PIC18LF6722(PS30009643):
    name: str = "18LF6722"
    CodeSector_endaddr: str = "0x20000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1400"
    fuses_impl: str = "00CF1F1F0385F500FFC0FFE0FF40"

@dataclass
class PIC18LF6723(PS30009643):
    name: str = "18LF6723"
    CodeSector_endaddr: str = "0x20000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x4A00"
    fuses_impl: str = "00CF1F1F0385F500FFC0FFE0FF40"

@dataclass
class PIC18LF8527(PS30009643):
    name: str = "18LF8527"
    CodeSector_endaddr: str = "0xc000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1360"
    fuses_impl: str = "00CF1F1FF387F50007C007E00740"

@dataclass
class PIC18LF8622(PS30009643):
    name: str = "18LF8622"
    CodeSector_endaddr: str = "0x10000"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_userid: str = "5000"
    VDD_mindefaultvoltage: str = "2000"
    Wait_cfg: str = "5000"
    Wait_userid: str = "5000"
    devid: str = "0x13A0"
    fuses_impl: str = "00CF1F1FF387F5000FC00FE00F40"
    panelsize: str = "8192"

@dataclass
class PIC18LF8627(PS30009643):
    name: str = "18LF8627"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x13E0"
    fuses_impl: str = "00CF1F1FF387F5003FC03FE03F40"

@dataclass
class PIC18LF8628(PS30009643):
    name: str = "18LF8628"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x49E0"
    fuses_impl: str = "00CF1F1FF387F5003FC03FE03F40"

@dataclass
class PIC18LF8722(PS30009643):
    name: str = "18LF8722"
    CodeSector_endaddr: str = "0x20000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1420"
    fuses_impl: str = "00CF1F1FF387F500FFC0FFE0FF40"

@dataclass
class PIC18LF8723(PS30009643):
    name: str = "18LF8723"
    CodeSector_endaddr: str = "0x20000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x4A20"
    fuses_impl: str = "00CF1F1FF387F500FFC0FFE0FF40"


@dataclass
class PS30009644:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0xfff8"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    Latches_cfg: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "1024"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "1024"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_erase: str = "600"
    ProgrammingWaitTime_pgm: str = "6000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3500"
    VDD_mindefaultvoltage: str = "2125"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "3250"
    VPP_defaultvoltage: str = "3250"
    VPP_maxvoltage: str = "3500"
    VPP_minvoltage: str = "2125"
    WORMHoleSector_beginaddr: str = "0xfff8"
    WORMHoleSector_endaddr: str = "0x7ffe"
    Wait_cfg: str = "10000"
    Wait_erase: str = "600"
    Wait_pgm: str = "6000"
    architecture: str = "18xxxx"
    fuses_impl: str = "E1FCC7FFF8F1"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "0"
    haslvp2: str = "1"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    pgmspec: str = "30009644"
    pkgthresh: str = "2750"
    tries: str = "1"

@dataclass
class PIC18F63J11(PS30009644):
    name: str = "18F63J11"
    CodeSector_endaddr: str = "0x1ff8"
    WORMHoleSector_beginaddr: str = "0x1ff8"
    WORMHoleSector_endaddr: str = "0x1ffe"
    devid: str = "0x3900"
    haschecksum: str = "0"

@dataclass
class PIC18F63J90(PS30009644):
    name: str = "18F63J90"
    CodeSector_endaddr: str = "0x1ff8"
    WORMHoleSector_beginaddr: str = "0x1ff8"
    WORMHoleSector_endaddr: str = "0x1ffe"
    devid: str = "0x3800"
    haschecksum: str = "0"

@dataclass
class PIC18F64J11(PS30009644):
    name: str = "18F64J11"
    CodeSector_endaddr: str = "0x3ff8"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x3ffe"
    devid: str = "0x3920"
    haschecksum: str = "0"

@dataclass
class PIC18F64J90(PS30009644):
    name: str = "18F64J90"
    CodeSector_endaddr: str = "0x3ff8"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x3ffe"
    devid: str = "0x3820"
    haschecksum: str = "0"

@dataclass
class PIC18F65J10(PS30009644):
    name: str = "18F65J10"
    CodeSector_endaddr: str = "0x7ff8"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    Wait_pgm: str = "10000"
    devid: str = "0x1520"
    haschecksum: str = "0"

@dataclass
class PIC18F65J11(PS30009644):
    name: str = "18F65J11"
    CodeSector_endaddr: str = "0x7ff8"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    devid: str = "0x3960"
    haschecksum: str = "0"

@dataclass
class PIC18F65J15(PS30009644):
    name: str = "18F65J15"
    CodeSector_endaddr: str = "0xbff8"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0xbff8"
    WORMHoleSector_endaddr: str = "0xbffe"
    Wait_pgm: str = "10000"
    devid: str = "0x1540"
    haschecksum: str = "0"

@dataclass
class PIC18F65J50(PS30009644):
    name: str = "18F65J50"
    CodeSector_endaddr: str = "0x7ff8"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    devid: str = "0x4100"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F65J90(PS30009644):
    name: str = "18F65J90"
    CodeSector_endaddr: str = "0x7ff8"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    devid: str = "0x3860"
    haschecksum: str = "0"

@dataclass
class PIC18F66J10(PS30009644):
    name: str = "18F66J10"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_endaddr: str = "0xfffe"
    Wait_pgm: str = "10000"
    devid: str = "0x1560"
    haschecksum: str = "0"

@dataclass
class PIC18F66J11(PS30009644):
    name: str = "18F66J11"
    WORMHoleSector_endaddr: str = "0xfffe"
    devid: str = "0x4440"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F66J15(PS30009644):
    name: str = "18F66J15"
    CodeSector_endaddr: str = "0x17ff8"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    Wait_pgm: str = "10000"
    devid: str = "0x1580"
    haschecksum: str = "0"

@dataclass
class PIC18F66J16(PS30009644):
    name: str = "18F66J16"
    CodeSector_endaddr: str = "0x17ff8"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    devid: str = "0x4460"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F66J50(PS30009644):
    name: str = "18F66J50"
    WORMHoleSector_endaddr: str = "0xfffe"
    devid: str = "0x4140"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F66J55(PS30009644):
    name: str = "18F66J55"
    CodeSector_endaddr: str = "0x17ff8"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    devid: str = "0x4160"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F66J90(PS30009644):
    name: str = "18F66J90"
    WORMHoleSector_endaddr: str = "0x10000"
    devid: str = "0x5000"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F66J93(PS30009644):
    name: str = "18F66J93"
    WORMHoleSector_endaddr: str = "0x10000"
    devid: str = "0x5040"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F67J10(PS30009644):
    name: str = "18F67J10"
    CodeSector_endaddr: str = "0x1fff8"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    Wait_pgm: str = "10000"
    devid: str = "0x15A0"
    haschecksum: str = "0"

@dataclass
class PIC18F67J11(PS30009644):
    name: str = "18F67J11"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    devid: str = "0x4480"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F67J50(PS30009644):
    name: str = "18F67J50"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    devid: str = "0x4180"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F67J90(PS30009644):
    name: str = "18F67J90"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5020"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F67J93(PS30009644):
    name: str = "18F67J93"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5060"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F83J11(PS30009644):
    name: str = "18F83J11"
    CodeSector_endaddr: str = "0x1ff8"
    WORMHoleSector_beginaddr: str = "0x1ff8"
    WORMHoleSector_endaddr: str = "0x1ffe"
    devid: str = "0x3980"
    haschecksum: str = "0"

@dataclass
class PIC18F83J90(PS30009644):
    name: str = "18F83J90"
    CodeSector_endaddr: str = "0x1ff8"
    WORMHoleSector_beginaddr: str = "0x1ff8"
    WORMHoleSector_endaddr: str = "0x1ffe"
    devid: str = "0x3880"
    haschecksum: str = "0"

@dataclass
class PIC18F84J11(PS30009644):
    name: str = "18F84J11"
    CodeSector_endaddr: str = "0x3ff8"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x3ffe"
    devid: str = "0x39A0"
    haschecksum: str = "0"

@dataclass
class PIC18F84J90(PS30009644):
    name: str = "18F84J90"
    CodeSector_endaddr: str = "0x3ff8"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x3ffe"
    devid: str = "0x38A0"
    haschecksum: str = "0"

@dataclass
class PIC18F85J10(PS30009644):
    name: str = "18F85J10"
    CodeSector_endaddr: str = "0x7ff8"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    devid: str = "0x15E0"
    fuses_impl: str = "E1FCC7FFF8F3"
    haschecksum: str = "0"

@dataclass
class PIC18F85J11(PS30009644):
    name: str = "18F85J11"
    CodeSector_endaddr: str = "0x7ff8"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    devid: str = "0x39E0"
    haschecksum: str = "0"

@dataclass
class PIC18F85J15(PS30009644):
    name: str = "18F85J15"
    CodeSector_endaddr: str = "0xbff8"
    WORMHoleSector_beginaddr: str = "0xbff8"
    WORMHoleSector_endaddr: str = "0xbffe"
    devid: str = "0x1700"
    fuses_impl: str = "E1FCC7FFF8F3"
    haschecksum: str = "0"

@dataclass
class PIC18F85J50(PS30009644):
    name: str = "18F85J50"
    CodeSector_endaddr: str = "0x7ff8"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    devid: str = "0x41A0"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F85J90(PS30009644):
    name: str = "18F85J90"
    CodeSector_endaddr: str = "0x7ff8"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    devid: str = "0x38E0"
    haschecksum: str = "0"

@dataclass
class PIC18F86J10(PS30009644):
    name: str = "18F86J10"
    WORMHoleSector_endaddr: str = "0xfffe"
    devid: str = "0x1720"
    fuses_impl: str = "E1FCC7FFF8F3"
    haschecksum: str = "0"

@dataclass
class PIC18F86J11(PS30009644):
    name: str = "18F86J11"
    WORMHoleSector_endaddr: str = "0xfffe"
    devid: str = "0x44E0"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F86J15(PS30009644):
    name: str = "18F86J15"
    CodeSector_endaddr: str = "0x17ff8"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    devid: str = "0x1740"
    fuses_impl: str = "E1FCC7FFF8F3"
    haschecksum: str = "0"

@dataclass
class PIC18F86J16(PS30009644):
    name: str = "18F86J16"
    CodeSector_endaddr: str = "0x17ff8"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    devid: str = "0x4500"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F86J50(PS30009644):
    name: str = "18F86J50"
    WORMHoleSector_endaddr: str = "0xfffe"
    devid: str = "0x41E0"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F86J55(PS30009644):
    name: str = "18F86J55"
    CodeSector_endaddr: str = "0x17ff8"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    devid: str = "0x4200"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F86J72(PS30009644):
    name: str = "18F86J72"
    WORMHoleSector_endaddr: str = "0x10000"
    devid: str = "0x5040"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F86J90(PS30009644):
    name: str = "18F86J90"
    WORMHoleSector_endaddr: str = "0x10000"
    devid: str = "0x5080"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F86J93(PS30009644):
    name: str = "18F86J93"
    WORMHoleSector_endaddr: str = "0x10000"
    devid: str = "0x50C0"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F87J10(PS30009644):
    name: str = "18F87J10"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    devid: str = "0x1760"
    fuses_impl: str = "E1FCC7FFF8F3"
    haschecksum: str = "0"

@dataclass
class PIC18F87J11(PS30009644):
    name: str = "18F87J11"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    devid: str = "0x4520"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F87J50(PS30009644):
    name: str = "18F87J50"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    devid: str = "0x4220"
    fuses_impl: str = "EFFFC7FFF8FF"
    haschecksum: str = "0"

@dataclass
class PIC18F87J72(PS30009644):
    name: str = "18F87J72"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5060"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F87J90(PS30009644):
    name: str = "18F87J90"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x50A0"
    fuses_impl: str = "E10CDF0F02F1"

@dataclass
class PIC18F87J93(PS30009644):
    name: str = "18F87J93"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x50E0"
    fuses_impl: str = "E10CDF0F02F1"


@dataclass
class PS30009687:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0xfff8"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_locsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataMemTraits_wordimpl: str = "0xff"
    EEDataMemTraits_wordinit: str = "0xff"
    EEDataMemTraits_wordsafe: str = "0xff"
    EEDataMemTraits_wordsize: str = "0x1"
    Latches_cfg: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "1024"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "1024"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "600"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3500"
    VDD_mindefaultvoltage: str = "2125"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "3250"
    VPP_defaultvoltage: str = "3250"
    VPP_maxvoltage: str = "3500"
    VPP_minvoltage: str = "2125"
    WORMHoleSector_beginaddr: str = "0xfff8"
    WORMHoleSector_endaddr: str = "0x10000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "600"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    architecture: str = "18xxxx"
    fuses_impl: str = "EF0FDF0FFFF9FF01"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "0"
    haslvp2: str = "1"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30009687"
    tries: str = "1"

@dataclass
class PIC18F24J10(PS30009687):
    name: str = "18F24J10"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "0"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x3ffe"
    Wait_cfg: str = "10000"
    Wait_lvpgm: str = "0"
    Wait_pgm: str = "10000"
    devid: str = "0x1D00"
    fuses_impl: str = "E1FCC7FF00F1"
    haschecksum: str = "0"

@dataclass
class PIC18F24J11(PS30009687):
    name: str = "18F24J11"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x4000"
    devid: str = "0x4D80"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18F24J50(PS30009687):
    name: str = "18F24J50"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x4000"
    devid: str = "0x4C00"

@dataclass
class PIC18F25J10(PS30009687):
    name: str = "18F25J10"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "0"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x7ffe"
    Wait_cfg: str = "10000"
    Wait_lvpgm: str = "0"
    Wait_pgm: str = "10000"
    devid: str = "0x1C00"
    fuses_impl: str = "E1FCC7FF00F1"
    haschecksum: str = "0"

@dataclass
class PIC18F25J11(PS30009687):
    name: str = "18F25J11"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x8000"
    devid: str = "0x4DA0"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18F25J50(PS30009687):
    name: str = "18F25J50"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x8000"
    devid: str = "0x4C20"

@dataclass
class PIC18F26J11(PS30009687):
    name: str = "18F26J11"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    devid: str = "0x4DC0"
    fuses_impl: str = "E104DF0FFFF9FF01"

@dataclass
class PIC18F26J13(PS30009687):
    name: str = "18F26J13"
    devid: str = "0x5920"
    fuses_impl: str = "FF0CFF0FFF0FFF03"

@dataclass
class PIC18F26J50(PS30009687):
    name: str = "18F26J50"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    devid: str = "0x4C40"

@dataclass
class PIC18F26J53(PS30009687):
    name: str = "18F26J53"
    devid: str = "0x5820"
    fuses_impl: str = "FF0FFF0FFF0FFF0B"

@dataclass
class PIC18F27J13(PS30009687):
    name: str = "18F27J13"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5960"
    fuses_impl: str = "FF0CFF0FFF0FFF03"

@dataclass
class PIC18F27J53(PS30009687):
    name: str = "18F27J53"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5860"
    fuses_impl: str = "FF0FFF0FFF0FFF0B"

@dataclass
class PIC18F44J10(PS30009687):
    name: str = "18F44J10"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "0"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x3ffe"
    Wait_cfg: str = "10000"
    Wait_lvpgm: str = "0"
    Wait_pgm: str = "10000"
    devid: str = "0x1D20"
    fuses_impl: str = "E1FCC7FF00F1"
    haschecksum: str = "0"

@dataclass
class PIC18F44J11(PS30009687):
    name: str = "18F44J11"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x4000"
    devid: str = "0x4DE0"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18F44J50(PS30009687):
    name: str = "18F44J50"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x4000"
    devid: str = "0x4C60"

@dataclass
class PIC18F45J10(PS30009687):
    name: str = "18F45J10"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "0"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x7ffe"
    Wait_cfg: str = "10000"
    Wait_lvpgm: str = "0"
    Wait_pgm: str = "10000"
    devid: str = "0x1C20"
    fuses_impl: str = "E1FCC7FF00F1"
    haschecksum: str = "0"

@dataclass
class PIC18F45J11(PS30009687):
    name: str = "18F45J11"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x8000"
    devid: str = "0x4E00"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18F45J50(PS30009687):
    name: str = "18F45J50"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x8000"
    devid: str = "0x4C80"

@dataclass
class PIC18F46J11(PS30009687):
    name: str = "18F46J11"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    devid: str = "0x4E20"
    fuses_impl: str = "E104DF0FFFF9FF01"

@dataclass
class PIC18F46J13(PS30009687):
    name: str = "18F46J13"
    devid: str = "0x59A0"
    fuses_impl: str = "FF0CFF0FFF0FFF03"

@dataclass
class PIC18F46J50(PS30009687):
    name: str = "18F46J50"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    devid: str = "0x4CA0"

@dataclass
class PIC18F46J53(PS30009687):
    name: str = "18F46J53"
    devid: str = "0x58A0"
    fuses_impl: str = "FF0FFF0FFF0FFF0B"

@dataclass
class PIC18F47J13(PS30009687):
    name: str = "18F47J13"
    CodeSector_endaddr: str = "0x1fff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x59E0"
    fuses_impl: str = "FF0CFF0FFF0FFF03"

@dataclass
class PIC18F47J53(PS30009687):
    name: str = "18F47J53"
    CodeSector_endaddr: str = "0x1fff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x58E0"
    fuses_impl: str = "FF0FFF0FFF0FFF0B"

@dataclass
class PIC18LF24J10(PS30009687):
    name: str = "18LF24J10"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "0"
    ProgrammingWaitTime_pgm: str = "10000"
    VDD_maxdefaultvoltage: str = "2750"
    VDD_maxvoltage: str = "2750"
    VDD_nominalvoltage: str = "2500"
    VPP_defaultvoltage: str = "2500"
    VPP_maxvoltage: str = "2750"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x3ffe"
    Wait_cfg: str = "10000"
    Wait_lvpgm: str = "0"
    Wait_pgm: str = "10000"
    devid: str = "0x1D40"
    fuses_impl: str = "E1FCC7FF00F1"
    haschecksum: str = "0"

@dataclass
class PIC18LF24J11(PS30009687):
    name: str = "18LF24J11"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x4000"
    devid: str = "0x4E40"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18LF24J50(PS30009687):
    name: str = "18LF24J50"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x4000"
    devid: str = "0x4CC0"

@dataclass
class PIC18LF25J10(PS30009687):
    name: str = "18LF25J10"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "0"
    ProgrammingWaitTime_pgm: str = "10000"
    VDD_maxdefaultvoltage: str = "2750"
    VDD_maxvoltage: str = "2750"
    VDD_nominalvoltage: str = "2500"
    VPP_defaultvoltage: str = "2500"
    VPP_maxvoltage: str = "2750"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x7ffe"
    Wait_cfg: str = "10000"
    Wait_lvpgm: str = "0"
    Wait_pgm: str = "10000"
    devid: str = "0x1C40"
    fuses_impl: str = "E1FCC7FF00F1"
    haschecksum: str = "0"

@dataclass
class PIC18LF25J11(PS30009687):
    name: str = "18LF25J11"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x8000"
    devid: str = "0x4E60"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18LF25J50(PS30009687):
    name: str = "18LF25J50"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x8000"
    devid: str = "0x4CE0"

@dataclass
class PIC18LF26J11(PS30009687):
    name: str = "18LF26J11"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    devid: str = "0x4E80"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18LF26J13(PS30009687):
    name: str = "18LF26J13"
    devid: str = "0x5B20"
    fuses_impl: str = "FF0CFF0FFF0FFF03"

@dataclass
class PIC18LF26J50(PS30009687):
    name: str = "18LF26J50"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    devid: str = "0x4D00"

@dataclass
class PIC18LF26J53(PS30009687):
    name: str = "18LF26J53"
    devid: str = "0x5A20"
    fuses_impl: str = "FF0FFF0FFF0FFF0B"

@dataclass
class PIC18LF27J13(PS30009687):
    name: str = "18LF27J13"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5B60"
    fuses_impl: str = "FF0CFF0FFF0FFF03"

@dataclass
class PIC18LF27J53(PS30009687):
    name: str = "18LF27J53"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5A60"
    fuses_impl: str = "FF0FFF0FFF0FFF0B"

@dataclass
class PIC18LF44J10(PS30009687):
    name: str = "18LF44J10"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "0"
    ProgrammingWaitTime_pgm: str = "10000"
    VDD_maxdefaultvoltage: str = "2750"
    VDD_maxvoltage: str = "2750"
    VDD_nominalvoltage: str = "2500"
    VPP_defaultvoltage: str = "2500"
    VPP_maxvoltage: str = "2750"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x3ffe"
    Wait_cfg: str = "10000"
    Wait_lvpgm: str = "0"
    Wait_pgm: str = "10000"
    devid: str = "0x1D60"
    fuses_impl: str = "E1FCC7FF00F1"
    haschecksum: str = "0"

@dataclass
class PIC18LF44J11(PS30009687):
    name: str = "18LF44J11"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x4000"
    devid: str = "0x4EA0"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18LF44J50(PS30009687):
    name: str = "18LF44J50"
    CodeSector_endaddr: str = "0x3ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x3ff8"
    WORMHoleSector_endaddr: str = "0x4000"
    devid: str = "0x4D20"

@dataclass
class PIC18LF45J10(PS30009687):
    name: str = "18LF45J10"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "0"
    ProgrammingWaitTime_pgm: str = "10000"
    VDD_maxdefaultvoltage: str = "2750"
    VDD_maxvoltage: str = "2750"
    VDD_nominalvoltage: str = "2500"
    VPP_defaultvoltage: str = "2500"
    VPP_maxvoltage: str = "2750"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x7ffe"
    Wait_cfg: str = "10000"
    Wait_lvpgm: str = "0"
    Wait_pgm: str = "10000"
    devid: str = "0x1C60"
    fuses_impl: str = "E1FCC7FF00F1"
    haschecksum: str = "0"

@dataclass
class PIC18LF45J11(PS30009687):
    name: str = "18LF45J11"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x8000"
    devid: str = "0x4EC0"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18LF45J50(PS30009687):
    name: str = "18LF45J50"
    CodeSector_endaddr: str = "0x7ff8"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    WORMHoleSector_beginaddr: str = "0x7ff8"
    WORMHoleSector_endaddr: str = "0x8000"
    devid: str = "0x4D40"

@dataclass
class PIC18LF46J11(PS30009687):
    name: str = "18LF46J11"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    devid: str = "0x4EE0"
    fuses_impl: str = "E10CDF0FFFF9FF01"

@dataclass
class PIC18LF46J13(PS30009687):
    name: str = "18LF46J13"
    devid: str = "0x5BA0"
    fuses_impl: str = "FF0CFF0FFF0FFF03"

@dataclass
class PIC18LF46J50(PS30009687):
    name: str = "18LF46J50"
    EEDataMemTraits_locsize: str = "0"
    EEDataMemTraits_wordimpl: str = "0"
    EEDataMemTraits_wordinit: str = "0"
    EEDataMemTraits_wordsafe: str = "0"
    EEDataMemTraits_wordsize: str = "0"
    devid: str = "0x4D60"

@dataclass
class PIC18LF46J53(PS30009687):
    name: str = "18LF46J53"
    devid: str = "0x5AA0"
    fuses_impl: str = "FF0FFF0FFF0FFF0B"

@dataclass
class PIC18LF47J13(PS30009687):
    name: str = "18LF47J13"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5BE0"
    fuses_impl: str = "FF0CFF0FFF0FFF03"

@dataclass
class PIC18LF47J53(PS30009687):
    name: str = "18LF47J53"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x20000"
    devid: str = "0x5AE0"
    fuses_impl: str = "FF0FFF0FFF0FFF0B"


@dataclass
class PS30009688:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0xfff8"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    Latches_cfg: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "1024"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "1024"
    ProgrammingWaitTime_cfg: str = "10000"
    ProgrammingWaitTime_erase: str = "600"
    ProgrammingWaitTime_pgm: str = "3400"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3500"
    VDD_mindefaultvoltage: str = "2125"
    VDD_minvoltage: str = "2125"
    VDD_nominalvoltage: str = "3250"
    VPP_defaultvoltage: str = "3250"
    VPP_maxvoltage: str = "3500"
    VPP_minvoltage: str = "2125"
    WORMHoleSector_beginaddr: str = "0xfff8"
    WORMHoleSector_endaddr: str = "0xfffe"
    Wait_cfg: str = "10000"
    Wait_erase: str = "600"
    Wait_pgm: str = "3400"
    architecture: str = "18xxxx"
    fuses_impl: str = "E1F4C7FFF8F7"
    hashighvoltagemclr2: str = "0"
    haslvp2: str = "1"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30009688"
    pkgthresh: str = "2750"
    tries: str = "1"

@dataclass
class PIC18F66J60(PS30009688):
    name: str = "18F66J60"
    ProgrammingWaitTime_pgm: str = "10000"
    Wait_pgm: str = "10000"
    devid: str = "0x1800"

@dataclass
class PIC18F66J65(PS30009688):
    name: str = "18F66J65"
    CodeSector_endaddr: str = "0x17ff8"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    Wait_pgm: str = "10000"
    devid: str = "0x1F00"

@dataclass
class PIC18F67J60(PS30009688):
    name: str = "18F67J60"
    CodeSector_endaddr: str = "0x1fff8"
    ProgrammingWaitTime_pgm: str = "10000"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    Wait_pgm: str = "10000"
    devid: str = "0x1F20"

@dataclass
class PIC18F86J60(PS30009688):
    name: str = "18F86J60"
    devid: str = "0x1820"

@dataclass
class PIC18F86J65(PS30009688):
    name: str = "18F86J65"
    CodeSector_endaddr: str = "0x17ff8"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    devid: str = "0x1F40"

@dataclass
class PIC18F87J60(PS30009688):
    name: str = "18F87J60"
    CodeSector_endaddr: str = "0x1fff8"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    devid: str = "0x1F60"

@dataclass
class PIC18F96J60(PS30009688):
    name: str = "18F96J60"
    devid: str = "0x1840"

@dataclass
class PIC18F96J65(PS30009688):
    name: str = "18F96J65"
    CodeSector_endaddr: str = "0x17ff8"
    WORMHoleSector_beginaddr: str = "0x17ff8"
    WORMHoleSector_endaddr: str = "0x17ffe"
    devid: str = "0x1F80"

@dataclass
class PIC18F97J60(PS30009688):
    name: str = "18F97J60"
    CodeSector_endaddr: str = "0x1fff8"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    WORMHoleSector_beginaddr: str = "0x1fff8"
    WORMHoleSector_endaddr: str = "0x1fffe"
    devid: str = "0x1FA0"


@dataclass
class PS30009752:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00080"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "9000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00CF1F1F0E89F90003C003E00340"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "4500"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30009752"
    tries: str = "1"

@dataclass
class PIC18F1230(PS30009752):
    name: str = "18F1230"
    devid: str = "0x1E00"

@dataclass
class PIC18F1330(PS30009752):
    name: str = "18F1330"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x1E20"

@dataclass
class PIC18LF1230(PS30009752):
    name: str = "18LF1230"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1E00"

@dataclass
class PIC18LF1330(PS30009752):
    name: str = "18LF1330"
    CodeSector_endaddr: str = "0x2000"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1E20"


@dataclass
class PS30009947:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "5000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "5750"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "5000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "5DDF7F7F018991000FC00FE00F40"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30009947"
    tries: str = "1"

@dataclass
class PIC18F65K22(PS30009947):
    name: str = "18F65K22"
    devid: str = "0x5300"

@dataclass
class PIC18F65K90(PS30009947):
    name: str = "18F65K90"
    devid: str = "0x5240"

@dataclass
class PIC18F66K22(PS30009947):
    name: str = "18F66K22"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x52C0"

@dataclass
class PIC18F66K90(PS30009947):
    name: str = "18F66K90"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x5200"

@dataclass
class PIC18F67K22(PS30009947):
    name: str = "18F67K22"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    devid: str = "0x5180"
    fuses_impl: str = "5DDF7F7F01899100FFC0FFE0FF40"

@dataclass
class PIC18F67K90(PS30009947):
    name: str = "18F67K90"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    devid: str = "0x5100"
    fuses_impl: str = "5DDF7F7F01899100FFC0FFE0FF40"

@dataclass
class PIC18F85K22(PS30009947):
    name: str = "18F85K22"
    devid: str = "0x5360"
    fuses_impl: str = "5DDF7F7FF98B91000FC00FE00F40"

@dataclass
class PIC18F85K90(PS30009947):
    name: str = "18F85K90"
    devid: str = "0x52A0"
    fuses_impl: str = "5DDF7F7F018B91000FC00FE00F40"

@dataclass
class PIC18F86K22(PS30009947):
    name: str = "18F86K22"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x5320"
    fuses_impl: str = "5DDF7F7FF98B91000FC00FE00F40"

@dataclass
class PIC18F86K90(PS30009947):
    name: str = "18F86K90"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x5260"
    fuses_impl: str = "5DDF7F7F018B91000FC00FE00F40"

@dataclass
class PIC18F87K22(PS30009947):
    name: str = "18F87K22"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    devid: str = "0x51C0"
    fuses_impl: str = "5DDF7F7FF98B9100FFC0FFE0FF40"

@dataclass
class PIC18F87K90(PS30009947):
    name: str = "18F87K90"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    devid: str = "0x5140"
    fuses_impl: str = "5DDF7F7F018B9100FFC0FFE0FF40"


@dataclass
class PS30009972:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "5000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "5750"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "5000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "5DFF7F7F008991000FC00FE00F40"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    lvpthresh: str = "3000"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "30009972"
    tries: str = "1"

@dataclass
class PIC18F25K80(PS30009972):
    name: str = "18F25K80"
    devid: str = "0x6180"

@dataclass
class PIC18F26K80(PS30009972):
    name: str = "18F26K80"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x6120"
    fuses_impl: str = "5DFF7F7F008D91000FC00FE00F40"

@dataclass
class PIC18F45K80(PS30009972):
    name: str = "18F45K80"
    devid: str = "0x6160"

@dataclass
class PIC18F46K80(PS30009972):
    name: str = "18F46K80"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x6100"

@dataclass
class PIC18F65K80(PS30009972):
    name: str = "18F65K80"
    devid: str = "0x6140"
    fuses_impl: str = "5DFF7F7F008F91000FC00FE00F40"

@dataclass
class PIC18F66K80(PS30009972):
    name: str = "18F66K80"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x60E0"
    fuses_impl: str = "5DFF7F7F008F91000FC00FE00F40"

@dataclass
class PIC18LF25K80(PS30009972):
    name: str = "18LF25K80"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6260"

@dataclass
class PIC18LF26K80(PS30009972):
    name: str = "18LF26K80"
    CodeSector_endaddr: str = "0x10000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6200"

@dataclass
class PIC18LF45K80(PS30009972):
    name: str = "18LF45K80"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6240"

@dataclass
class PIC18LF46K80(PS30009972):
    name: str = "18LF46K80"
    CodeSector_endaddr: str = "0x10000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x61E0"

@dataclass
class PIC18LF65K80(PS30009972):
    name: str = "18LF65K80"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6220"
    fuses_impl: str = "5DFF7F7F008F91000FC00FE00F40"
    haslvp: str = "0"

@dataclass
class PIC18LF66K80(PS30009972):
    name: str = "18LF66K80"
    CodeSector_endaddr: str = "0x10000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x61C0"
    fuses_impl: str = "5DFF7F7F008F91000FC00FE00F40"
    haslvp: str = "0"


@dataclass
class PS40000245:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "4"
    Latches_userid: str = "4"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_userid: str = "4"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_pgm: str = "2000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "6000"
    Wait_pgm: str = "2000"
    Wait_userid: str = "2000"
    architecture: str = "16xxxx"
    erasealgo: str = "1"
    fuses_impl: str = "20CF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40000245"
    tries: str = "1"

@dataclass
class PIC16F716(PS40000245):
    name: str = "16F716"
    devid: str = "0x1140"


@dataclass
class PS40001191:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_unimplval: str = "0"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2180"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2500"
    ProgrammingWaitTime_eedata: str = "6000"
    ProgrammingWaitTime_erase: str = "8000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2000"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "12750"
    Wait_cfg: str = "2500"
    Wait_eedata: str = "6000"
    Wait_erase: str = "8000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2000"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    erasealgo: str = "4"
    fuses_impl: str = "31FF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    hasvppfirst: str = "1"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001191"
    tries: str = "1"

@dataclass
class PIC12F629(PS40001191):
    name: str = "12F629"
    devid: str = "0x0F80"

@dataclass
class PIC12F675(PS40001191):
    name: str = "12F675"
    devid: str = "0x0FC0"

@dataclass
class PIC16F630(PS40001191):
    name: str = "16F630"
    ProgrammingWaitTime_erase: str = "6000"
    Wait_erase: str = "6000"
    devid: str = "0x10C0"

@dataclass
class PIC16F676(PS40001191):
    name: str = "16F676"
    ProgrammingWaitTime_erase: str = "6000"
    Wait_erase: str = "6000"
    devid: str = "0x10E0"
    hasvppfirst: str = "0"


@dataclass
class PS40001196:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2180"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "4000"
    ProgrammingWaitTime_eedata: str = "6000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_pgm: str = "4000"
    ProgrammingWaitTime_userid: str = "4000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "6000"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13250"
    VPP_minvoltage: str = "12750"
    Wait_cfg: str = "4000"
    Wait_eedata: str = "6000"
    Wait_erase: str = "10000"
    Wait_pgm: str = "4000"
    Wait_userid: str = "4000"
    architecture: str = "16xxxx"
    erasealgo: str = "1"
    fuses_impl: str = "21FF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "0"
    lvpbit_mask: str = "0x0080"
    lvpbit_offset: str = "0"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001196"
    tries: str = "1"

@dataclass
class PIC16F627A(PS40001196):
    name: str = "16F627A"
    devid: str = "0x1040"

@dataclass
class PIC16F628A(PS40001196):
    name: str = "16F628A"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x1060"

@dataclass
class PIC16F648A(PS40001196):
    name: str = "16F648A"
    CodeSector_endaddr: str = "0x1000"
    EEDataSector_endaddr: str = "0x2200"
    devid: str = "0x1100"

@dataclass
class PIC16LF627A(PS40001196):
    name: str = "16LF627A"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1040"

@dataclass
class PIC16LF628A(PS40001196):
    name: str = "16LF628A"
    CodeSector_endaddr: str = "0x800"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1060"

@dataclass
class PIC16LF648A(PS40001196):
    name: str = "16LF648A"
    CodeSector_endaddr: str = "0x1000"
    EEDataSector_endaddr: str = "0x2200"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2000"
    devid: str = "0x1100"


@dataclass
class PS40001204:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2200"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "4"
    Latches_rowerase: str = "16"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "6000"
    ProgrammingWaitTime_eedata: str = "6000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "2500"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "6000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "6000"
    Wait_eedata: str = "6000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "2500"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "6000"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "0FFF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001204"
    tries: str = "1"

@dataclass
class PIC12F635(PS40001204):
    name: str = "12F635"
    CodeSector_endaddr: str = "0x400"
    EEDataSector_endaddr: str = "0x2180"
    VDD_mindefaultvoltage: str = "3000"
    boundary: str = "0"
    devid: str = "0x0FA0"
    fuses_impl: str = "1FFF"

@dataclass
class PIC12F683(PS40001204):
    name: str = "12F683"
    VDD_mindefaultvoltage: str = "3000"
    boundary: str = "0"
    devid: str = "0x0460"

@dataclass
class PIC16F631(PS40001204):
    name: str = "16F631"
    CodeSector_endaddr: str = "0x400"
    EEDataSector_endaddr: str = "0x2180"
    devid: str = "0x1420"

@dataclass
class PIC16F636(PS40001204):
    name: str = "16F636"
    boundary: str = "0"
    devid: str = "0x10A0"
    fuses_impl: str = "1FFF"
    hasvppfirst: str = "0"

@dataclass
class PIC16F639(PS40001204):
    name: str = "16F639"
    devid: str = "0x10A0"
    fuses_impl: str = "1FFF"

@dataclass
class PIC16F677(PS40001204):
    name: str = "16F677"
    devid: str = "0x1440"

@dataclass
class PIC16F684(PS40001204):
    name: str = "16F684"
    boundary: str = "0"
    devid: str = "0x1080"

@dataclass
class PIC16F685(PS40001204):
    name: str = "16F685"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x04A0"

@dataclass
class PIC16F687(PS40001204):
    name: str = "16F687"
    devid: str = "0x1320"

@dataclass
class PIC16F688(PS40001204):
    name: str = "16F688"
    CodeSector_endaddr: str = "0x1000"
    boundary: str = "0"
    devid: str = "0x1180"

@dataclass
class PIC16F689(PS40001204):
    name: str = "16F689"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x1340"

@dataclass
class PIC16F690(PS40001204):
    name: str = "16F690"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x1400"


@dataclass
class PS40001207:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x200"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_unimplval: str = "0"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200"
    UserIDSector_endaddr: str = "0x204"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "2"
    fuses_impl: str = "000F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001207"
    tries: str = "1"

@dataclass
class PIC16F54(PS40001207):
    name: str = "16F54"
    devid: str = "0x0000"


@dataclass
class PS40001208:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "4"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x800"
    UserIDSector_endaddr: str = "0x804"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "2"
    fuses_impl: str = "000F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001208"
    tries: str = "1"

@dataclass
class PIC16F57(PS40001208):
    name: str = "16F57"
    devid: str = "0x0000"


@dataclass
class PS40001226:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x400"
    UserIDSector_endaddr: str = "0x404"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "1"
    fuses_impl: str = "003F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001226"
    tries: str = "1"

@dataclass
class PIC16F505(PS40001226):
    name: str = "16F505"
    devid: str = "0x0000"


@dataclass
class PS40001227:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x200"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200"
    UserIDSector_endaddr: str = "0x204"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "1"
    fuses_impl: str = "001F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001227"
    tries: str = "1"

@dataclass
class PIC12F508(PS40001227):
    name: str = "12F508"
    devid: str = "0x0000"

@dataclass
class PIC12F509(PS40001227):
    name: str = "12F509"
    CodeSector_endaddr: str = "0x400"
    UserIDSector_beginaddr: str = "0x400"
    UserIDSector_endaddr: str = "0x404"
    devid: str = "0x0000"


@dataclass
class PS40001228:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x100"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x100"
    UserIDSector_endaddr: str = "0x104"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "12500"
    VPP_maxvoltage: str = "13500"
    VPP_minvoltage: str = "12500"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "1"
    fuses_impl: str = "001C"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001228"
    tries: str = "1"

@dataclass
class PIC10F200(PS40001228):
    name: str = "10F200"
    devid: str = "0x0000"

@dataclass
class PIC10F202(PS40001228):
    name: str = "10F202"
    CodeSector_endaddr: str = "0x200"
    UserIDSector_beginaddr: str = "0x200"
    UserIDSector_endaddr: str = "0x204"
    devid: str = "0x0000"

@dataclass
class PIC10F204(PS40001228):
    name: str = "10F204"
    devid: str = "0x0000"

@dataclass
class PIC10F206(PS40001228):
    name: str = "10F206"
    CodeSector_endaddr: str = "0x200"
    UserIDSector_beginaddr: str = "0x200"
    UserIDSector_endaddr: str = "0x204"
    devid: str = "0x0000"


@dataclass
class PS40001237:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2200"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "4"
    Latches_rowerase: str = "16"
    Latches_userid: str = "4"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "4"
    ProgrammingWaitTime_cfg: str = "6000"
    ProgrammingWaitTime_eedata: str = "6000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "2500"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "6000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "6000"
    Wait_eedata: str = "6000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "2500"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "6000"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "0FFF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001237"
    tries: str = "1"

@dataclass
class PIC16F785(PS40001237):
    name: str = "16F785"
    devid: str = "0x1200"

@dataclass
class PIC16HV785(PS40001237):
    name: str = "16HV785"
    ProgrammingWaitTime_lverase: str = "0"
    ProgrammingWaitTime_lvpgm: str = "0"
    VDD_maxdefaultvoltage: str = "4875"
    VDD_maxvoltage: str = "4875"
    VDD_mindefaultvoltage: str = "2500"
    VDD_minvoltage: str = "2500"
    VDD_nominalvoltage: str = "4500"
    Wait_lverase: str = "0"
    Wait_lvpgm: str = "0"
    devid: str = "0x1220"


@dataclass
class PS40001243:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "4"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x800"
    UserIDSector_endaddr: str = "0x804"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "2"
    fuses_impl: str = "000F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001243"
    tries: str = "1"

@dataclass
class PIC16F59(PS40001243):
    name: str = "16F59"
    devid: str = "0x0000"


@dataclass
class PS40001244:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2200"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "16"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2500"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2500"
    Wait_eedata: str = "8000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "1FFF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001244"
    tries: str = "1"

@dataclass
class PIC16F913(PS40001244):
    name: str = "16F913"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "4"
    ProgrammingRowSize_pgm: str = "4"
    devid: str = "0x13E0"

@dataclass
class PIC16F914(PS40001244):
    name: str = "16F914"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "4"
    ProgrammingRowSize_pgm: str = "4"
    devid: str = "0x13C0"

@dataclass
class PIC16F916(PS40001244):
    name: str = "16F916"
    boundary: str = "8"
    devid: str = "0x13A0"

@dataclass
class PIC16F917(PS40001244):
    name: str = "16F917"
    boundary: str = "8"
    devid: str = "0x1380"

@dataclass
class PIC16F946(PS40001244):
    name: str = "16F946"
    VPP_defaultvoltage: str = "12000"
    boundary: str = "0"
    devid: str = "0x1460"


@dataclass
class PS40001257:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x400"
    UserIDSector_endaddr: str = "0x404"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "1"
    fuses_impl: str = "003F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001257"
    tries: str = "1"

@dataclass
class PIC12F510(PS40001257):
    name: str = "12F510"
    devid: str = "0x0000"


@dataclass
class PS40001258:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x400"
    UserIDSector_endaddr: str = "0x404"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "1"
    fuses_impl: str = "007F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001258"
    tries: str = "1"

@dataclass
class PIC16F506(PS40001258):
    name: str = "16F506"
    devid: str = "0x0000"


@dataclass
class PS40001266:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x100"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x100"
    UserIDSector_endaddr: str = "0x104"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    erasealgo: str = "1"
    fuses_impl: str = "001F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001266"
    tries: str = "1"

@dataclass
class PIC10F220(PS40001266):
    name: str = "10F220"
    devid: str = "0x0000"

@dataclass
class PIC10F222(PS40001266):
    name: str = "10F222"
    CodeSector_endaddr: str = "0x200"
    UserIDSector_beginaddr: str = "0x200"
    UserIDSector_endaddr: str = "0x204"
    devid: str = "0x0000"


@dataclass
class PS40001284:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "16"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "6000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "2500"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "6000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "12000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "6000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "2500"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "6000"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "03FF"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001284"
    tries: str = "1"

@dataclass
class PIC12F609(PS40001284):
    name: str = "12F609"
    devid: str = "0x2240"

@dataclass
class PIC12F615(PS40001284):
    name: str = "12F615"
    devid: str = "0x2180"

@dataclass
class PIC12F617(PS40001284):
    name: str = "12F617"
    CodeSector_endaddr: str = "0x800"
    Latches_pgm: str = "4"
    ProgrammingRowSize_pgm: str = "4"
    devid: str = "0x1360"
    fuses_impl: str = "0FFF"

@dataclass
class PIC12HV609(PS40001284):
    name: str = "12HV609"
    Latches_rowerase: str = "0"
    ProgrammingRowSize_rowerase: str = "0"
    VDD_maxdefaultvoltage: str = "4750"
    VDD_maxvoltage: str = "4750"
    VDD_nominalvoltage: str = "4750"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13000"
    devid: str = "0x2280"

@dataclass
class PIC12HV615(PS40001284):
    name: str = "12HV615"
    Latches_rowerase: str = "0"
    ProgrammingRowSize_rowerase: str = "0"
    VDD_maxdefaultvoltage: str = "4750"
    VDD_maxvoltage: str = "4750"
    VDD_nominalvoltage: str = "4750"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13000"
    devid: str = "0x21A0"

@dataclass
class PIC16F610(PS40001284):
    name: str = "16F610"
    devid: str = "0x2260"

@dataclass
class PIC16F616(PS40001284):
    name: str = "16F616"
    CodeSector_endaddr: str = "0x800"
    Latches_pgm: str = "4"
    ProgrammingRowSize_pgm: str = "4"
    devid: str = "0x1240"

@dataclass
class PIC16HV610(PS40001284):
    name: str = "16HV610"
    Latches_rowerase: str = "0"
    ProgrammingRowSize_rowerase: str = "0"
    VDD_maxdefaultvoltage: str = "4750"
    VDD_maxvoltage: str = "4750"
    VDD_nominalvoltage: str = "4750"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13000"
    devid: str = "0x22A0"

@dataclass
class PIC16HV616(PS40001284):
    name: str = "16HV616"
    CodeSector_endaddr: str = "0x800"
    Latches_pgm: str = "4"
    Latches_rowerase: str = "0"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_rowerase: str = "0"
    VDD_maxdefaultvoltage: str = "4750"
    VDD_maxvoltage: str = "4750"
    VDD_mindefaultvoltage: str = "4000"
    VDD_nominalvoltage: str = "4750"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13000"
    devid: str = "0x1260"


@dataclass
class PS40001287:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    EEDataSector_beginaddr: str = "0x2100"
    EEDataSector_endaddr: str = "0x2200"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "4"
    Latches_rowerase: str = "16"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2500"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2500"
    Wait_eedata: str = "8000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "3FFF0700"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x1000"
    lvpbit_offset: str = "0"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001287"
    tries: str = "1"

@dataclass
class PIC16F882(PS40001287):
    name: str = "16F882"
    CodeSector_endaddr: str = "0x800"
    EEDataSector_endaddr: str = "0x2180"
    devid: str = "0x2000"

@dataclass
class PIC16F883(PS40001287):
    name: str = "16F883"
    devid: str = "0x2020"

@dataclass
class PIC16F884(PS40001287):
    name: str = "16F884"
    devid: str = "0x2040"

@dataclass
class PIC16F886(PS40001287):
    name: str = "16F886"
    CodeSector_endaddr: str = "0x2000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    boundary: str = "8"
    devid: str = "0x2060"

@dataclass
class PIC16F887(PS40001287):
    name: str = "16F887"
    CodeSector_endaddr: str = "0x2000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    boundary: str = "8"
    devid: str = "0x2080"


@dataclass
class PS40001297:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3500"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "5750"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00CF1F1F008FC50003C003E00340"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "40001297"
    tries: str = "1"

@dataclass
class PIC18F23K20(PS40001297):
    name: str = "18F23K20"
    Latches_pgm: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    devid: str = "0x20E0"

@dataclass
class PIC18F24K20(PS40001297):
    name: str = "18F24K20"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x20A0"

@dataclass
class PIC18F25K20(PS40001297):
    name: str = "18F25K20"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x2060"
    fuses_impl: str = "00CF1F1F008FC5000FC00FE00F40"

@dataclass
class PIC18F26K20(PS40001297):
    name: str = "18F26K20"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    devid: str = "0x2020"
    fuses_impl: str = "00CF1F1F008FC5000FC00FE00F40"

@dataclass
class PIC18F43K20(PS40001297):
    name: str = "18F43K20"
    Latches_pgm: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    devid: str = "0x20C0"

@dataclass
class PIC18F44K20(PS40001297):
    name: str = "18F44K20"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x2080"

@dataclass
class PIC18F45K20(PS40001297):
    name: str = "18F45K20"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x2040"
    fuses_impl: str = "00CF1F1F008FC5000FC00FE00F40"

@dataclass
class PIC18F46K20(PS40001297):
    name: str = "18F46K20"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    Latches_pgm: str = "64"
    ProgrammingRowSize_pgm: str = "64"
    devid: str = "0x2000"
    fuses_impl: str = "00CF1F1F008FC5000FC00FE00F40"


@dataclass
class PS40001316:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_flashdata: str = "2000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x440"
    UserIDSector_endaddr: str = "0x444"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_flashdata: str = "2000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16x5x"
    csumadd: str = "100"
    erasealgo: str = "3"
    fuses_impl: str = "007F"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001316"
    tries: str = "1"

@dataclass
class PIC12F519(PS40001316):
    name: str = "12F519"
    devid: str = "0x0000"


@dataclass
class PS40001317:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xfff"
    CodeMemTraits_wordinit: str = "0xfff"
    CodeMemTraits_wordsafe: str = "0xfff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0xfff"
    ConfigFuseMemTraits_wordinit: str = "0xfff"
    ConfigFuseMemTraits_wordsafe: str = "0xfff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0xfff"
    ConfigFuseSector_endaddr: str = "0x1000"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    EEDataMemTraits_magicoffset: str = "0x0"
    Latches_cfg: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "1"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "2000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_flashdata: str = "2000"
    ProgrammingWaitTime_lvpgm: str = "2000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "2000"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xfff"
    UserIDMemTraits_wordinit: str = "0xfff"
    UserIDMemTraits_wordsafe: str = "0xfff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x440"
    UserIDSector_endaddr: str = "0x444"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "3000"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "11000"
    VPP_maxvoltage: str = "12000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "2000"
    Wait_erase: str = "10000"
    Wait_flashdata: str = "2000"
    Wait_lvpgm: str = "2000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "2000"
    architecture: str = "16c5x"
    erasealgo: str = "3"
    fuses_impl: str = "00FF"
    hashighvoltagemclr2: str = "1"
    lvpthresh: str = "0"
    mask: str = "0x0FFF"
    memtech: str = "ee"
    pgmspec: str = "40001317"
    tries: str = "1"

@dataclass
class PIC16F526(PS40001317):
    name: str = "16F526"
    devid: str = "0x0000"


@dataclass
class PS40001332:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "3F7F0030"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001332"
    tries: str = "1"

@dataclass
class PIC16F722(PS40001332):
    name: str = "16F722"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x1880"

@dataclass
class PIC16F722A(PS40001332):
    name: str = "16F722A"
    CodeSector_endaddr: str = "0x800"
    ProgrammingWaitTime_lverase: str = "0"
    Wait_lverase: str = "0"
    devid: str = "0x1B20"

@dataclass
class PIC16F723(PS40001332):
    name: str = "16F723"
    devid: str = "0x1860"

@dataclass
class PIC16F723A(PS40001332):
    name: str = "16F723A"
    ProgrammingWaitTime_lverase: str = "0"
    Wait_lverase: str = "0"
    devid: str = "0x1B00"

@dataclass
class PIC16F724(PS40001332):
    name: str = "16F724"
    devid: str = "0x1840"

@dataclass
class PIC16F726(PS40001332):
    name: str = "16F726"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x1820"

@dataclass
class PIC16F727(PS40001332):
    name: str = "16F727"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x1800"

@dataclass
class PIC16LF722(PS40001332):
    name: str = "16LF722"
    CodeSector_endaddr: str = "0x800"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1980"

@dataclass
class PIC16LF722A(PS40001332):
    name: str = "16LF722A"
    CodeSector_endaddr: str = "0x800"
    ProgrammingWaitTime_lverase: str = "0"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_nominalvoltage: str = "3300"
    Wait_lverase: str = "0"
    devid: str = "0x1B60"

@dataclass
class PIC16LF723(PS40001332):
    name: str = "16LF723"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1960"

@dataclass
class PIC16LF723A(PS40001332):
    name: str = "16LF723A"
    ProgrammingWaitTime_lverase: str = "0"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_nominalvoltage: str = "3300"
    Wait_lverase: str = "0"
    devid: str = "0x1B40"

@dataclass
class PIC16LF724(PS40001332):
    name: str = "16LF724"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1940"

@dataclass
class PIC16LF726(PS40001332):
    name: str = "16LF726"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1920"

@dataclass
class PIC16LF727(PS40001332):
    name: str = "16LF727"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1900"


@dataclass
class PS40001342:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "5500"
    VDD_maxvoltageissuggestion: str = "1"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3300"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "3300"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "38FF1F1F00884D0003C003E00340"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "40001342"
    tries: str = "1"

@dataclass
class PIC18F13K50(PS40001342):
    name: str = "18F13K50"
    devid: str = "0x4740"

@dataclass
class PIC18F14K50(PS40001342):
    name: str = "18F14K50"
    CodeSector_endaddr: str = "0x4000"
    Latches_pgm: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    devid: str = "0x4760"
    fuses_impl: str = "CFFF1F1F00884D0003C003E00340"

@dataclass
class PIC18LF13K50(PS40001342):
    name: str = "18LF13K50"
    VDD_maxvoltage: str = "3600"
    VDD_maxvoltageissuggestion: str = "0"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    devid: str = "0x4700"

@dataclass
class PIC18LF14K50(PS40001342):
    name: str = "18LF14K50"
    CodeSector_endaddr: str = "0x4000"
    Latches_pgm: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    VDD_maxvoltage: str = "3600"
    VDD_maxvoltageissuggestion: str = "0"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    devid: str = "0x4720"
    fuses_impl: str = "CFFF1F1F00884D0003C003E00340"


@dataclass
class PS40001357:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "10000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "5750"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "10000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00FF1F1F0088CD0003C003E00340"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "0"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "40001357"
    tries: str = "1"

@dataclass
class PIC18F13K22(PS40001357):
    name: str = "18F13K22"
    devid: str = "0x4F40"

@dataclass
class PIC18F14K22(PS40001357):
    name: str = "18F14K22"
    CodeSector_endaddr: str = "0x4000"
    Latches_pgm: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    devid: str = "0x4F20"

@dataclass
class PIC18LF13K22(PS40001357):
    name: str = "18LF13K22"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x4F80"

@dataclass
class PIC18LF14K22(PS40001357):
    name: str = "18LF14K22"
    CodeSector_endaddr: str = "0x4000"
    Latches_pgm: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x4F60"


@dataclass
class PS40001390:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "8000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "3FFF3713"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001390"
    tries: str = "1"

@dataclass
class PIC12F1822(PS40001390):
    name: str = "12F1822"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    devid: str = "0x2700"

@dataclass
class PIC12LF1822(PS40001390):
    name: str = "12LF1822"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2800"

@dataclass
class PIC16F1823(PS40001390):
    name: str = "16F1823"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    devid: str = "0x2720"

@dataclass
class PIC16F1824(PS40001390):
    name: str = "16F1824"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x2740"

@dataclass
class PIC16F1825(PS40001390):
    name: str = "16F1825"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x2760"

@dataclass
class PIC16F1826(PS40001390):
    name: str = "16F1826"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    boundary: str = "8"
    devid: str = "0x2780"

@dataclass
class PIC16F1827(PS40001390):
    name: str = "16F1827"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    boundary: str = "8"
    devid: str = "0x27A0"

@dataclass
class PIC16F1828(PS40001390):
    name: str = "16F1828"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x27C0"

@dataclass
class PIC16F1829(PS40001390):
    name: str = "16F1829"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x27E0"

@dataclass
class PIC16F1829LIN(PS40001390):
    name: str = "16F1829LIN"
    CodeSector_endaddr: str = "0x2000"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordsafe: str = "0x7f"
    devid: str = "0x27E0"

@dataclass
class PIC16LF1823(PS40001390):
    name: str = "16LF1823"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2820"

@dataclass
class PIC16LF1824(PS40001390):
    name: str = "16LF1824"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2840"

@dataclass
class PIC16LF1825(PS40001390):
    name: str = "16LF1825"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2860"

@dataclass
class PIC16LF1826(PS40001390):
    name: str = "16LF1826"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    boundary: str = "8"
    devid: str = "0x2880"
    fuses_impl: str = "3FFF3703"

@dataclass
class PIC16LF1827(PS40001390):
    name: str = "16LF1827"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    boundary: str = "8"
    devid: str = "0x28A0"
    fuses_impl: str = "3FFF3703"

@dataclass
class PIC16LF1828(PS40001390):
    name: str = "16LF1828"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x28C0"

@dataclass
class PIC16LF1829(PS40001390):
    name: str = "16LF1829"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x28E0"


@dataclass
class PS40001397:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "8000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "3FFF3703"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001397"
    tries: str = "1"

@dataclass
class PIC16F1933(PS40001397):
    name: str = "16F1933"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_nominalvoltage: str = "5000"
    devid: str = "0x2300"
    fuses_impl: str = "3FFF3733"

@dataclass
class PIC16F1934(PS40001397):
    name: str = "16F1934"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_nominalvoltage: str = "5000"
    devid: str = "0x2340"
    fuses_impl: str = "3FFF3733"

@dataclass
class PIC16F1936(PS40001397):
    name: str = "16F1936"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_nominalvoltage: str = "5000"
    devid: str = "0x2360"
    fuses_impl: str = "3FFF3733"

@dataclass
class PIC16F1937(PS40001397):
    name: str = "16F1937"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_nominalvoltage: str = "5000"
    devid: str = "0x2380"
    fuses_impl: str = "3FFF3733"

@dataclass
class PIC16F1938(PS40001397):
    name: str = "16F1938"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_nominalvoltage: str = "5000"
    devid: str = "0x23A0"
    fuses_impl: str = "3FFF3733"

@dataclass
class PIC16F1939(PS40001397):
    name: str = "16F1939"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_nominalvoltage: str = "5000"
    devid: str = "0x23C0"
    fuses_impl: str = "3FFF3733"

@dataclass
class PIC16F1946(PS40001397):
    name: str = "16F1946"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_nominalvoltage: str = "5000"
    devid: str = "0x2500"
    fuses_impl: str = "3FFF3713"

@dataclass
class PIC16F1947(PS40001397):
    name: str = "16F1947"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_nominalvoltage: str = "5000"
    devid: str = "0x2520"
    fuses_impl: str = "3FFF3713"

@dataclass
class PIC16LF1902(PS40001397):
    name: str = "16LF1902"
    CodeSector_endaddr: str = "0x800"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    Wait_eedata: str = "0"
    devid: str = "0x2C20"
    fuses_impl: str = "0EFB3E03"
    lvpthresh: str = "2600"

@dataclass
class PIC16LF1903(PS40001397):
    name: str = "16LF1903"
    CodeSector_endaddr: str = "0x1000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    Wait_eedata: str = "0"
    devid: str = "0x2C00"
    fuses_impl: str = "0EFB3E03"
    lvpthresh: str = "2600"

@dataclass
class PIC16LF1904(PS40001397):
    name: str = "16LF1904"
    CodeSector_endaddr: str = "0x1000"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    Wait_eedata: str = "0"
    devid: str = "0x2C80"
    fuses_impl: str = "0EFB3E03"
    lvpthresh: str = "2600"

@dataclass
class PIC16LF1906(PS40001397):
    name: str = "16LF1906"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    Wait_eedata: str = "0"
    devid: str = "0x2C60"
    fuses_impl: str = "0EFB3E03"
    lvpthresh: str = "2600"

@dataclass
class PIC16LF1907(PS40001397):
    name: str = "16LF1907"
    EEDataSector_beginaddr: str = "0"
    EEDataSector_endaddr: str = "0"
    Latches_eedata: str = "0"
    ProgrammingRowSize_eedata: str = "0"
    ProgrammingWaitTime_eedata: str = "0"
    Wait_eedata: str = "0"
    devid: str = "0x2C40"
    fuses_impl: str = "0EFB3E03"
    lvpthresh: str = "2600"

@dataclass
class PIC16LF1933(PS40001397):
    name: str = "16LF1933"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    devid: str = "0x2400"

@dataclass
class PIC16LF1934(PS40001397):
    name: str = "16LF1934"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    devid: str = "0x2440"

@dataclass
class PIC16LF1936(PS40001397):
    name: str = "16LF1936"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    devid: str = "0x2460"

@dataclass
class PIC16LF1937(PS40001397):
    name: str = "16LF1937"
    Latches_pgm: str = "8"
    ProgrammingRowSize_pgm: str = "8"
    devid: str = "0x2480"

@dataclass
class PIC16LF1938(PS40001397):
    name: str = "16LF1938"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x24A0"

@dataclass
class PIC16LF1939(PS40001397):
    name: str = "16LF1939"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x24C0"

@dataclass
class PIC16LF1946(PS40001397):
    name: str = "16LF1946"
    devid: str = "0x2580"

@dataclass
class PIC16LF1947(PS40001397):
    name: str = "16LF1947"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x25A0"


@dataclass
class PS40001398:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "12000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7500"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "12000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "00FF1F3F00BFC50003C003E00340"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "40001398"
    tries: str = "1"

@dataclass
class PIC18F23K22(PS40001398):
    name: str = "18F23K22"
    devid: str = "0x5740"

@dataclass
class PIC18F24K22(PS40001398):
    name: str = "18F24K22"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x5640"

@dataclass
class PIC18F25K22(PS40001398):
    name: str = "18F25K22"
    CodeSector_endaddr: str = "0x8000"
    ProgrammingWaitTime_erase: str = "15000"
    Wait_erase: str = "15000"
    devid: str = "0x5540"
    fuses_impl: str = "00FF1F3F00BFC5000FC00FE00F40"

@dataclass
class PIC18F26K22(PS40001398):
    name: str = "18F26K22"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    ProgrammingWaitTime_erase: str = "15000"
    Wait_erase: str = "15000"
    devid: str = "0x5440"
    fuses_impl: str = "00FF1F3F00BFC5000FC00FE00F40"

@dataclass
class PIC18F43K22(PS40001398):
    name: str = "18F43K22"
    devid: str = "0x5700"

@dataclass
class PIC18F44K22(PS40001398):
    name: str = "18F44K22"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x5600"

@dataclass
class PIC18F45K22(PS40001398):
    name: str = "18F45K22"
    CodeSector_endaddr: str = "0x8000"
    ProgrammingWaitTime_erase: str = "15000"
    Wait_erase: str = "15000"
    devid: str = "0x5500"
    fuses_impl: str = "00FF1F3F00BFC5000FC00FE00F40"

@dataclass
class PIC18F46K22(PS40001398):
    name: str = "18F46K22"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    ProgrammingWaitTime_erase: str = "15000"
    Wait_erase: str = "15000"
    devid: str = "0x5400"
    fuses_impl: str = "00FF1F3F00BFC5000FC00FE00F40"

@dataclass
class PIC18LF23K22(PS40001398):
    name: str = "18LF23K22"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "5750"
    devid: str = "0x5760"

@dataclass
class PIC18LF24K22(PS40001398):
    name: str = "18LF24K22"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "5750"
    devid: str = "0x5660"

@dataclass
class PIC18LF25K22(PS40001398):
    name: str = "18LF25K22"
    CodeSector_endaddr: str = "0x8000"
    ProgrammingWaitTime_erase: str = "15000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "5750"
    Wait_erase: str = "15000"
    devid: str = "0x5560"
    fuses_impl: str = "00FF1F3F00BFC5000FC00FE00F40"

@dataclass
class PIC18LF26K22(PS40001398):
    name: str = "18LF26K22"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    ProgrammingWaitTime_erase: str = "15000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "5570"
    Wait_erase: str = "15000"
    devid: str = "0x5460"
    fuses_impl: str = "00FF1F3F00BFC5000FC00FE00F40"

@dataclass
class PIC18LF43K22(PS40001398):
    name: str = "18LF43K22"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "5750"
    devid: str = "0x5720"

@dataclass
class PIC18LF44K22(PS40001398):
    name: str = "18LF44K22"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "5750"
    devid: str = "0x5620"

@dataclass
class PIC18LF45K22(PS40001398):
    name: str = "18LF45K22"
    CodeSector_endaddr: str = "0x8000"
    ProgrammingWaitTime_erase: str = "15000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "5750"
    Wait_erase: str = "15000"
    devid: str = "0x5520"
    fuses_impl: str = "00FF1F3F00BFC5000FC00FE00F40"

@dataclass
class PIC18LF46K22(PS40001398):
    name: str = "18LF46K22"
    CodeSector_endaddr: str = "0x10000"
    EEDataSector_endaddr: str = "0xf00400"
    ProgrammingWaitTime_erase: str = "15000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "5750"
    Wait_erase: str = "15000"
    devid: str = "0x5420"
    fuses_impl: str = "00FF1F3F00BFC5000FC00FE00F40"


@dataclass
class PS40001405:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "8"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "8"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "377F0030"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001405"
    tries: str = "1"

@dataclass
class PIC16F707(PS40001405):
    name: str = "16F707"
    devid: str = "0x1AC0"

@dataclass
class PIC16LF707(PS40001405):
    name: str = "16LF707"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1AE0"
    fuses_impl: str = "377F0000"


@dataclass
class PS40001409:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2500"
    VDD_minvoltage: str = "2100"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "337B0013"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001409"
    tries: str = "1"

@dataclass
class PIC16F720(PS40001409):
    name: str = "16F720"
    devid: str = "0x1C00"

@dataclass
class PIC16F721(PS40001409):
    name: str = "16F721"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x1C20"

@dataclass
class PIC16LF720(PS40001409):
    name: str = "16LF720"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1C40"
    fuses_impl: str = "337B0003"

@dataclass
class PIC16LF721(PS40001409):
    name: str = "16LF721"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3500"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2000"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1C60"
    fuses_impl: str = "337B0003"


@dataclass
class PS40001439:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "8000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "3FFF3713"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001439"
    tries: str = "1"

@dataclass
class PIC12F1840(PS40001439):
    name: str = "12F1840"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    devid: str = "0x1B80"

@dataclass
class PIC12LF1840(PS40001439):
    name: str = "12LF1840"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1BC0"

@dataclass
class PIC16F1847(PS40001439):
    name: str = "16F1847"
    CodeSector_endaddr: str = "0x2000"
    boundary: str = "8"
    devid: str = "0x1480"

@dataclass
class PIC16LF1847(PS40001439):
    name: str = "16LF1847"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    boundary: str = "8"
    devid: str = "0x14A0"


@dataclass
class PS40001442:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "3EFF3E13"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001442"
    tries: str = "1"

@dataclass
class PIC16F1512(PS40001442):
    name: str = "16F1512"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x1700"

@dataclass
class PIC16F1513(PS40001442):
    name: str = "16F1513"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x1640"

@dataclass
class PIC16F1516(PS40001442):
    name: str = "16F1516"
    devid: str = "0x1680"

@dataclass
class PIC16F1517(PS40001442):
    name: str = "16F1517"
    devid: str = "0x16A0"

@dataclass
class PIC16F1518(PS40001442):
    name: str = "16F1518"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x16C0"

@dataclass
class PIC16F1519(PS40001442):
    name: str = "16F1519"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x16E0"

@dataclass
class PIC16F1526(PS40001442):
    name: str = "16F1526"
    devid: str = "0x1580"

@dataclass
class PIC16F1527(PS40001442):
    name: str = "16F1527"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x15A0"

@dataclass
class PIC16LF1512(PS40001442):
    name: str = "16LF1512"
    CodeSector_endaddr: str = "0x800"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1720"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1513(PS40001442):
    name: str = "16LF1513"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1740"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1516(PS40001442):
    name: str = "16LF1516"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x1780"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1517(PS40001442):
    name: str = "16LF1517"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x17A0"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1518(PS40001442):
    name: str = "16LF1518"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x17C0"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1519(PS40001442):
    name: str = "16LF1519"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x17E0"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1526(PS40001442):
    name: str = "16LF1526"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x15C0"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1527(PS40001442):
    name: str = "16LF1527"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x15E0"
    fuses_impl: str = "3EFF3E03"


@dataclass
class PS40001457:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "5000"
    ProgrammingWaitTime_erase: str = "5000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "5000"
    Wait_erase: str = "5000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "3FFF3F23"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001457"
    tries: str = "1"

@dataclass
class PIC16F1782(PS40001457):
    name: str = "16F1782"
    CodeSector_endaddr: str = "0x800"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    devid: str = "0x2A00"

@dataclass
class PIC16F1783(PS40001457):
    name: str = "16F1783"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    devid: str = "0x2A20"

@dataclass
class PIC16F1784(PS40001457):
    name: str = "16F1784"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    devid: str = "0x2A40"

@dataclass
class PIC16F1786(PS40001457):
    name: str = "16F1786"
    CodeSector_endaddr: str = "0x2000"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    devid: str = "0x2A60"

@dataclass
class PIC16F1787(PS40001457):
    name: str = "16F1787"
    CodeSector_endaddr: str = "0x2000"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    devid: str = "0x2A80"

@dataclass
class PIC16F1788(PS40001457):
    name: str = "16F1788"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x302B"
    mask: str = "0x3FFF"

@dataclass
class PIC16F1789(PS40001457):
    name: str = "16F1789"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x302A"
    mask: str = "0x3FFF"

@dataclass
class PIC16LF1782(PS40001457):
    name: str = "16LF1782"
    CodeSector_endaddr: str = "0x800"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2AA0"
    fuses_impl: str = "3FFF3F03"

@dataclass
class PIC16LF1783(PS40001457):
    name: str = "16LF1783"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2AC0"
    fuses_impl: str = "3FFF3F03"

@dataclass
class PIC16LF1784(PS40001457):
    name: str = "16LF1784"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2AE0"
    fuses_impl: str = "3FFF3F03"

@dataclass
class PIC16LF1786(PS40001457):
    name: str = "16LF1786"
    CodeSector_endaddr: str = "0x2000"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2B00"
    fuses_impl: str = "3FFF3F03"

@dataclass
class PIC16LF1787(PS40001457):
    name: str = "16LF1787"
    CodeSector_endaddr: str = "0x2000"
    RevisionIDSector_beginaddr: str = "0"
    RevisionIDSector_endaddr: str = "0"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2B20"
    fuses_impl: str = "3FFF3F03"

@dataclass
class PIC16LF1788(PS40001457):
    name: str = "16LF1788"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x302D"
    fuses_impl: str = "3FFF3F03"
    mask: str = "0x3FFF"

@dataclass
class PIC16LF1789(PS40001457):
    name: str = "16LF1789"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x302C"
    fuses_impl: str = "3FFF3F03"
    mask: str = "0x3FFF"


@dataclass
class PS40001561:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "4"
    Latches_rowerase: str = "16"
    Latches_userid: str = "4"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "4"
    ProgrammingWaitTime_cfg: str = "6000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "2500"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "6000"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "6000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "2500"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "6000"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "3F79"
    hashighvoltagemclr2: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "4500"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001561"
    tries: str = "1"

@dataclass
class PIC12F752(PS40001561):
    name: str = "12F752"
    devid: str = "0x1500"

@dataclass
class PIC12HV752(PS40001561):
    name: str = "12HV752"
    devid: str = "0x1520"


@dataclass
class PS40001572:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x100"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    EEDataMemTraits_magicoffset: str = "0x2100"
    Latches_cfg: str = "1"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "1FFF"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0100"
    lvpbit_offset: str = "0"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001572"
    tries: str = "1"

@dataclass
class PIC10F320(PS40001572):
    name: str = "10F320"
    devid: str = "0x29A0"

@dataclass
class PIC10F322(PS40001572):
    name: str = "10F322"
    CodeSector_endaddr: str = "0x200"
    devid: str = "0x2980"

@dataclass
class PIC10LF320(PS40001572):
    name: str = "10LF320"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x29E0"

@dataclass
class PIC10LF322(PS40001572):
    name: str = "10LF322"
    CodeSector_endaddr: str = "0x200"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x29C0"


@dataclass
class PS40001573:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "0EFB2E03"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001573"
    tries: str = "1"

@dataclass
class PIC12F1501(PS40001573):
    name: str = "12F1501"
    CodeSector_endaddr: str = "0x400"
    devid: str = "0x2CC0"

@dataclass
class PIC12LF1501(PS40001573):
    name: str = "12LF1501"
    CodeSector_endaddr: str = "0x400"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2D80"

@dataclass
class PIC16F1503(PS40001573):
    name: str = "16F1503"
    devid: str = "0x2CE0"

@dataclass
class PIC16F1507(PS40001573):
    name: str = "16F1507"
    devid: str = "0x2D00"

@dataclass
class PIC16F1508(PS40001573):
    name: str = "16F1508"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    devid: str = "0x2D20"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16F1509(PS40001573):
    name: str = "16F1509"
    CodeSector_endaddr: str = "0x2000"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    devid: str = "0x2D40"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1503(PS40001573):
    name: str = "16LF1503"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2DA0"

@dataclass
class PIC16LF1507(PS40001573):
    name: str = "16LF1507"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2DC0"

@dataclass
class PIC16LF1508(PS40001573):
    name: str = "16LF1508"
    CodeSector_endaddr: str = "0x1000"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2DE0"
    fuses_impl: str = "3EFF3E03"

@dataclass
class PIC16LF1509(PS40001573):
    name: str = "16LF1509"
    CodeSector_endaddr: str = "0x2000"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x2E00"
    fuses_impl: str = "3EFF3E03"


@dataclass
class PS40001595:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "8000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "3FFF3713"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001595"
    tries: str = "1"

@dataclass
class PIC12LF1840T39A(PS40001595):
    name: str = "12LF1840T39A"
    devid: str = "0x1BC0"

@dataclass
class PIC12LF1840T48A(PS40001595):
    name: str = "12LF1840T48A"
    devid: str = "0x1BC0"


@dataclass
class PS40001620:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "3EFF3FF3"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pgmspec: str = "40001620"
    tries: str = "1"

@dataclass
class PIC16F1454(PS40001620):
    name: str = "16F1454"
    devid: str = "0x3020"

@dataclass
class PIC16F1455(PS40001620):
    name: str = "16F1455"
    devid: str = "0x3021"

@dataclass
class PIC16F1459(PS40001620):
    name: str = "16F1459"
    devid: str = "0x3023"

@dataclass
class PIC16LF1454(PS40001620):
    name: str = "16LF1454"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3024"

@dataclass
class PIC16LF1455(PS40001620):
    name: str = "16LF1455"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3025"

@dataclass
class PIC16LF1459(PS40001620):
    name: str = "16LF1459"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3027"


@dataclass
class PS40001630:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000e"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0xf00000"
    EEDataSector_beginaddr: str = "0xf00000"
    EEDataSector_endaddr: str = "0xf00100"
    Latches_cfg: str = "2"
    Latches_eedata: str = "2"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "8"
    ProgrammingRowSize_cfg: str = "2"
    ProgrammingRowSize_eedata: str = "2"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "8"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "4000"
    ProgrammingWaitTime_erase: str = "15000"
    ProgrammingWaitTime_lverase: str = "1000"
    ProgrammingWaitTime_lvpgm: str = "1000"
    ProgrammingWaitTime_pgm: str = "1000"
    ProgrammingWaitTime_userid: str = "5000"
    UserIDMemTraits_addrinc: str = "0x1"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0xff"
    UserIDMemTraits_wordinit: str = "0xff"
    UserIDMemTraits_wordsafe: str = "0xff"
    UserIDMemTraits_wordsize: str = "0x1"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200008"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4250"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "4000"
    Wait_erase: str = "15000"
    Wait_lverase: str = "1000"
    Wait_lvpgm: str = "1000"
    Wait_pgm: str = "1000"
    Wait_userid: str = "5000"
    architecture: str = "18xxxx"
    fuses_impl: str = "3BEF5F3F00D3C50003C003E00340"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    lvpbit_mask: str = "0x0004"
    lvpbit_offset: str = "6"
    lvpthresh: str = "2700"
    mask: str = "0xFFE0"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "40001630"
    tries: str = "1"

@dataclass
class PIC18F24K50(PS40001630):
    name: str = "18F24K50"
    CodeSector_endaddr: str = "0x4000"
    ProgrammingWaitTime_erase: str = "12000"
    Wait_erase: str = "12000"
    devid: str = "0x5C60"

@dataclass
class PIC18F25K50(PS40001630):
    name: str = "18F25K50"
    devid: str = "0x5C20"
    fuses_impl: str = "3BEF5F3F00D3C5000FC00FE00F40"

@dataclass
class PIC18F45K50(PS40001630):
    name: str = "18F45K50"
    devid: str = "0x5C00"
    fuses_impl: str = "3BEF5F3F00D3E5000FC00FE00F40"

@dataclass
class PIC18LF24K50(PS40001630):
    name: str = "18LF24K50"
    CodeSector_endaddr: str = "0x4000"
    ProgrammingWaitTime_erase: str = "12000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2700"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    Wait_erase: str = "12000"
    devid: str = "0x5CE0"

@dataclass
class PIC18LF25K50(PS40001630):
    name: str = "18LF25K50"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2700"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x5CA0"
    fuses_impl: str = "3BEF5F3F00D3C5000FC00FE00F40"

@dataclass
class PIC18LF45K50(PS40001630):
    name: str = "18LF45K50"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "2700"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x5C80"
    fuses_impl: str = "3BEF5F3F00D3E5000FC00FE00F40"


@dataclass
class PS40001642:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "0EFB2E03"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001642"
    tries: str = "1"

@dataclass
class PIC12LF1552(PS40001642):
    name: str = "12LF1552"
    devid: str = "0x2BC0"


@dataclass
class PS40001654:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "8000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "8000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "3FFF3713"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001654"
    tries: str = "1"

@dataclass
class PIC16LF1824T39A(PS40001654):
    name: str = "16LF1824T39A"
    devid: str = "0x2840"


@dataclass
class PS40001683:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_rowerase: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_rowerase: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "3EFF3F87"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pgmspec: str = "40001683"
    tries: str = "1"

@dataclass
class PIC16F1703(PS40001683):
    name: str = "16F1703"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    devid: str = "0x3061"
    fuses_impl: str = "0EFB3F87"

@dataclass
class PIC16F1704(PS40001683):
    name: str = "16F1704"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x3043"

@dataclass
class PIC16F1705(PS40001683):
    name: str = "16F1705"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3055"

@dataclass
class PIC16F1707(PS40001683):
    name: str = "16F1707"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    devid: str = "0x3060"
    fuses_impl: str = "0EFB3F87"

@dataclass
class PIC16F1708(PS40001683):
    name: str = "16F1708"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x3042"

@dataclass
class PIC16F1709(PS40001683):
    name: str = "16F1709"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3054"

@dataclass
class PIC16LF1703(PS40001683):
    name: str = "16LF1703"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3063"
    fuses_impl: str = "0EFB3F87"

@dataclass
class PIC16LF1704(PS40001683):
    name: str = "16LF1704"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3045"

@dataclass
class PIC16LF1705(PS40001683):
    name: str = "16LF1705"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3057"

@dataclass
class PIC16LF1707(PS40001683):
    name: str = "16LF1707"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3062"
    fuses_impl: str = "0EFB3F87"

@dataclass
class PIC16LF1708(PS40001683):
    name: str = "16LF1708"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3044"

@dataclass
class PIC16LF1709(PS40001683):
    name: str = "16LF1709"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3056"


@dataclass
class PS40001686:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x2007"
    ConfigFuseSector_endaddr: str = "0x2008"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x2006"
    DeviceIDSector_endaddr: str = "0x2007"
    Latches_cfg: str = "1"
    Latches_pgm: str = "4"
    Latches_rowerase: str = "16"
    Latches_userid: str = "4"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "4"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "4"
    ProgrammingWaitTime_cfg: str = "6000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "3000"
    ProgrammingWaitTime_lvpgm: str = "3000"
    ProgrammingWaitTime_pgm: str = "3000"
    ProgrammingWaitTime_userid: str = "6000"
    RevisionIDSector_beginaddr: str = "0x2005"
    RevisionIDSector_endaddr: str = "0x2006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x2000"
    UserIDSector_endaddr: str = "0x2004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "4500"
    VDD_minvoltage: str = "2000"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "13000"
    VPP_maxvoltage: str = "13000"
    VPP_minvoltage: str = "10000"
    Wait_cfg: str = "6000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "3000"
    Wait_lvpgm: str = "3000"
    Wait_pgm: str = "3000"
    Wait_userid: str = "6000"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "3F79"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpthresh: str = "4500"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pgmspec: str = "40001686"
    tries: str = "1"

@dataclass
class PIC16F753(PS40001686):
    name: str = "16F753"
    devid: str = "0x3030"

@dataclass
class PIC16HV753(PS40001686):
    name: str = "16HV753"
    devid: str = "0x3031"


@dataclass
class PS40001713:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x400"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    fuses_impl: str = "0EFB3F03"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pgmspec: str = "40001713"
    tries: str = "1"

@dataclass
class PIC12F1571(PS40001713):
    name: str = "12F1571"
    devid: str = "0x3051"

@dataclass
class PIC12F1572(PS40001713):
    name: str = "12F1572"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x3050"

@dataclass
class PIC12LF1571(PS40001713):
    name: str = "12LF1571"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "7900"
    devid: str = "0x3053"

@dataclass
class PIC12LF1572(PS40001713):
    name: str = "12LF1572"
    CodeSector_endaddr: str = "0x800"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_minvoltage: str = "7900"
    devid: str = "0x3052"


@dataclass
class PS40001714:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "3EFF3F87"
    haschecksum: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pgmspec: str = "40001714"
    tries: str = "1"

@dataclass
class PIC16F1713(PS40001714):
    name: str = "16F1713"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x3049"

@dataclass
class PIC16F1716(PS40001714):
    name: str = "16F1716"
    devid: str = "0x3048"

@dataclass
class PIC16F1717(PS40001714):
    name: str = "16F1717"
    devid: str = "0x305C"

@dataclass
class PIC16F1718(PS40001714):
    name: str = "16F1718"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x305B"

@dataclass
class PIC16F1719(PS40001714):
    name: str = "16F1719"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x305A"

@dataclass
class PIC16LF1713(PS40001714):
    name: str = "16LF1713"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x304B"

@dataclass
class PIC16LF1716(PS40001714):
    name: str = "16LF1716"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x304A"

@dataclass
class PIC16LF1717(PS40001714):
    name: str = "16LF1717"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x305F"

@dataclass
class PIC16LF1718(PS40001714):
    name: str = "16LF1718"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x305E"

@dataclass
class PIC16LF1719(PS40001714):
    name: str = "16LF1719"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x305D"


@dataclass
class PS40001720:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800a"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "0EE33F833F7F"
    haschecksum: str = "1"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pgmspec: str = "40001720"
    tries: str = "1"

@dataclass
class PIC12F1612(PS40001720):
    name: str = "12F1612"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    devid: str = "0x3058"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC12LF1612(PS40001720):
    name: str = "12LF1612"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3059"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16F1613(PS40001720):
    name: str = "16F1613"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    devid: str = "0x304C"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16F1614(PS40001720):
    name: str = "16F1614"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x3078"
    fuses_impl: str = "0EE33F873F7F"
    haschecksum: str = "0"

@dataclass
class PIC16F1615(PS40001720):
    name: str = "16F1615"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x307C"
    fuses_impl: str = "3EE73F873F7F"
    haschecksum: str = "0"

@dataclass
class PIC16F1618(PS40001720):
    name: str = "16F1618"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x3079"
    fuses_impl: str = "0EE33F873F7F"
    haschecksum: str = "0"

@dataclass
class PIC16F1619(PS40001720):
    name: str = "16F1619"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x307D"
    fuses_impl: str = "3EE73F873F7F"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1613(PS40001720):
    name: str = "16LF1613"
    Latches_pgm: str = "16"
    Latches_rowerase: str = "16"
    ProgrammingRowSize_pgm: str = "16"
    ProgrammingRowSize_rowerase: str = "16"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x304D"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1614(PS40001720):
    name: str = "16LF1614"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x307A"
    fuses_impl: str = "0EE33F873F7F"
    haschecksum: str = "0"

@dataclass
class PIC16LF1615(PS40001720):
    name: str = "16LF1615"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3000"
    devid: str = "0x307E"
    fuses_impl: str = "3EE73F873F7F"
    haschecksum: str = "0"

@dataclass
class PIC16LF1618(PS40001720):
    name: str = "16LF1618"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x307B"
    fuses_impl: str = "0EE33F873F7F"
    haschecksum: str = "0"

@dataclass
class PIC16LF1619(PS40001720):
    name: str = "16LF1619"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x307F"
    fuses_impl: str = "3EE73F873F7F"
    haschecksum: str = "0"


@dataclass
class PS40001738:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800b"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_eedata: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5000"
    Wait_eedata: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "512"
    fuses_impl: str = "29773AEF20030003"
    haschecksum: str = "1"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "2"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40001738"
    tries: str = "1"

@dataclass
class PIC16F18313(PS40001738):
    name: str = "16F18313"
    devid: str = "0x3066"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16F18323(PS40001738):
    name: str = "16F18323"
    devid: str = "0x3067"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16F18324(PS40001738):
    name: str = "16F18324"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x303A"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16F18325(PS40001738):
    name: str = "16F18325"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x303E"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16F18326(PS40001738):
    name: str = "16F18326"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30A4"
    haschecksum: str = "0"

@dataclass
class PIC16F18344(PS40001738):
    name: str = "16F18344"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x303B"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16F18345(PS40001738):
    name: str = "16F18345"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x303F"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16F18346(PS40001738):
    name: str = "16F18346"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30A5"
    haschecksum: str = "0"

@dataclass
class PIC16LF18313(PS40001738):
    name: str = "16LF18313"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3068"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16LF18323(PS40001738):
    name: str = "16LF18323"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3069"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16LF18324(PS40001738):
    name: str = "16LF18324"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x303C"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16LF18325(PS40001738):
    name: str = "16LF18325"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3040"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16LF18326(PS40001738):
    name: str = "16LF18326"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x30A6"
    haschecksum: str = "0"

@dataclass
class PIC16LF18344(PS40001738):
    name: str = "16LF18344"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x303D"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16LF18345(PS40001738):
    name: str = "16LF18345"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3041"
    erasepagesize: str = "0"
    pagesize: str = "0"

@dataclass
class PIC16LF18346(PS40001738):
    name: str = "16LF18346"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x30A7"
    haschecksum: str = "0"


@dataclass
class PS40001743:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "0EFB3E03"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FE0"
    memtech: str = "ee"
    pgmspec: str = "40001743"
    tries: str = "1"

@dataclass
class PIC16LF1554(PS40001743):
    name: str = "16LF1554"
    devid: str = "0x2F00"

@dataclass
class PIC16LF1559(PS40001743):
    name: str = "16LF1559"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x2F20"


@dataclass
class PS40001753:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "5600"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2375"
    VDD_minvoltage: str = "2375"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "5600"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "32"
    fuses_impl: str = "29773EE33F7F30030003"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40001753"
    tries: str = "1"

@dataclass
class PIC16F18854(PS40001753):
    name: str = "16F18854"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x306A"

@dataclass
class PIC16F18855(PS40001753):
    name: str = "16F18855"
    devid: str = "0x306C"

@dataclass
class PIC16F18856(PS40001753):
    name: str = "16F18856"
    CodeSector_endaddr: str = "0x4000"
    ProgrammingWaitTime_erase: str = "8400"
    Wait_erase: str = "8400"
    devid: str = "0x3070"

@dataclass
class PIC16F18857(PS40001753):
    name: str = "16F18857"
    CodeSector_endaddr: str = "0x8000"
    ProgrammingWaitTime_erase: str = "14000"
    Wait_erase: str = "14000"
    devid: str = "0x3074"

@dataclass
class PIC16F18875(PS40001753):
    name: str = "16F18875"
    devid: str = "0x306D"

@dataclass
class PIC16F18876(PS40001753):
    name: str = "16F18876"
    CodeSector_endaddr: str = "0x4000"
    ProgrammingWaitTime_erase: str = "8400"
    Wait_erase: str = "8400"
    devid: str = "0x3071"

@dataclass
class PIC16F18877(PS40001753):
    name: str = "16F18877"
    CodeSector_endaddr: str = "0x8000"
    ProgrammingWaitTime_erase: str = "14000"
    Wait_erase: str = "14000"
    devid: str = "0x3075"

@dataclass
class PIC16LF18854(PS40001753):
    name: str = "16LF18854"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x306B"

@dataclass
class PIC16LF18855(PS40001753):
    name: str = "16LF18855"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x306E"

@dataclass
class PIC16LF18856(PS40001753):
    name: str = "16LF18856"
    CodeSector_endaddr: str = "0x4000"
    ProgrammingWaitTime_erase: str = "8400"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    Wait_erase: str = "8400"
    devid: str = "0x3072"

@dataclass
class PIC16LF18857(PS40001753):
    name: str = "16LF18857"
    CodeSector_endaddr: str = "0x8000"
    ProgrammingWaitTime_erase: str = "14000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    Wait_erase: str = "14000"
    devid: str = "0x3076"

@dataclass
class PIC16LF18875(PS40001753):
    name: str = "16LF18875"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x306F"

@dataclass
class PIC16LF18876(PS40001753):
    name: str = "16LF18876"
    CodeSector_endaddr: str = "0x4000"
    ProgrammingWaitTime_erase: str = "8400"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    Wait_erase: str = "8400"
    devid: str = "0x3073"

@dataclass
class PIC16LF18877(PS40001753):
    name: str = "16LF18877"
    CodeSector_endaddr: str = "0x8000"
    ProgrammingWaitTime_erase: str = "14000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    Wait_erase: str = "14000"
    devid: str = "0x3077"


@dataclass
class PS40001754:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_rowerase: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_rowerase: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "32"
    fuses_impl: str = "3EFF3F87"
    haschecksum: str = "1"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40001754"
    tries: str = "1"

@dataclass
class PIC16F1764(PS40001754):
    name: str = "16F1764"
    devid: str = "0x3080"
    haschecksum: str = "0"

@dataclass
class PIC16F1765(PS40001754):
    name: str = "16F1765"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3081"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16F1768(PS40001754):
    name: str = "16F1768"
    devid: str = "0x3084"
    haschecksum: str = "0"

@dataclass
class PIC16F1769(PS40001754):
    name: str = "16F1769"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3085"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1764(PS40001754):
    name: str = "16LF1764"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3082"

@dataclass
class PIC16LF1765(PS40001754):
    name: str = "16LF1765"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3083"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1768(PS40001754):
    name: str = "16LF1768"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3086"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1769(PS40001754):
    name: str = "16LF1769"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3087"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "0"


@dataclass
class PS40001766:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "4"
    erasealgo: str = "1"
    erasepagesize: str = "32"
    fuses_impl: str = "0EFB3F07"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40001766"
    tries: str = "1"

@dataclass
class PIC16F1574(PS40001766):
    name: str = "16F1574"
    devid: str = "0x3000"

@dataclass
class PIC16F1575(PS40001766):
    name: str = "16F1575"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3001"

@dataclass
class PIC16F1578(PS40001766):
    name: str = "16F1578"
    devid: str = "0x3002"

@dataclass
class PIC16F1579(PS40001766):
    name: str = "16F1579"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3003"

@dataclass
class PIC16LF1574(PS40001766):
    name: str = "16LF1574"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3004"

@dataclass
class PIC16LF1575(PS40001766):
    name: str = "16LF1575"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3005"

@dataclass
class PIC16LF1578(PS40001766):
    name: str = "16LF1578"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3006"

@dataclass
class PIC16LF1579(PS40001766):
    name: str = "16LF1579"
    CodeSector_endaddr: str = "0x2000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3007"


@dataclass
class PS40001772:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x310000"
    EEDataSector_endaddr: str = "0x310400"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "25200"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200010"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "25200"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "18xxxx"
    fuses_impl: str = "7729E3BF7F3F0F3703000F02"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "7"
    lvpthresh: str = "2700"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "40001772"
    tries: str = "1"

@dataclass
class PIC18F24K40(PS40001772):
    name: str = "18F24K40"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0x310100"
    devid: str = "0x69C0"
    fuses_impl: str = "7729E3BF7F3F033703000302"

@dataclass
class PIC18F25K40(PS40001772):
    name: str = "18F25K40"
    EEDataSector_endaddr: str = "0x310100"
    devid: str = "0x69A0"

@dataclass
class PIC18F26K40(PS40001772):
    name: str = "18F26K40"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x6980"

@dataclass
class PIC18F27K40(PS40001772):
    name: str = "18F27K40"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    devid: str = "0x6960"
    fuses_impl: str = "7729E3BF7F3FFF370300FF02"

@dataclass
class PIC18F45K40(PS40001772):
    name: str = "18F45K40"
    EEDataSector_endaddr: str = "0x310100"
    devid: str = "0x6940"

@dataclass
class PIC18F46K40(PS40001772):
    name: str = "18F46K40"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x6920"

@dataclass
class PIC18F47K40(PS40001772):
    name: str = "18F47K40"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    devid: str = "0x6900"
    fuses_impl: str = "7729E3BF7F3FFF370300FF02"

@dataclass
class PIC18LF24K40(PS40001772):
    name: str = "18LF24K40"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0x310100"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6AA0"
    fuses_impl: str = "7729E3BF7F3F033703000302"

@dataclass
class PIC18LF25K40(PS40001772):
    name: str = "18LF25K40"
    EEDataSector_endaddr: str = "0x310100"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6A80"

@dataclass
class PIC18LF26K40(PS40001772):
    name: str = "18LF26K40"
    CodeSector_endaddr: str = "0x10000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6A60"

@dataclass
class PIC18LF27K40(PS40001772):
    name: str = "18LF27K40"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6A40"
    fuses_impl: str = "7729E3BF7F3FFF370300FF02"

@dataclass
class PIC18LF45K40(PS40001772):
    name: str = "18LF45K40"
    EEDataSector_endaddr: str = "0x310100"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6A20"

@dataclass
class PIC18LF46K40(PS40001772):
    name: str = "18LF46K40"
    CodeSector_endaddr: str = "0x10000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6A00"

@dataclass
class PIC18LF47K40(PS40001772):
    name: str = "18LF47K40"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x69E0"
    fuses_impl: str = "7729E3BF7F3FFF370300FF02"


@dataclass
class PS40001792:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lverase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_rowerase: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lverase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_rowerase: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "32"
    fuses_impl: str = "3EFF3F87"
    haschecksum: str = "1"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40001792"
    tries: str = "1"

@dataclass
class PIC16F1773(PS40001792):
    name: str = "16F1773"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x308A"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16F1776(PS40001792):
    name: str = "16F1776"
    devid: str = "0x308B"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16F1777(PS40001792):
    name: str = "16F1777"
    devid: str = "0x308E"
    haschecksum: str = "0"

@dataclass
class PIC16F1778(PS40001792):
    name: str = "16F1778"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x308F"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16F1779(PS40001792):
    name: str = "16F1779"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x3090"
    haschecksum: str = "0"

@dataclass
class PIC16LF1773(PS40001792):
    name: str = "16LF1773"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x308C"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1776(PS40001792):
    name: str = "16LF1776"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x308D"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1777(PS40001792):
    name: str = "16LF1777"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3091"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1778(PS40001792):
    name: str = "16LF1778"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3092"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "0"

@dataclass
class PIC16LF1779(PS40001792):
    name: str = "16LF1779"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x3093"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "0"


@dataclass
class PS40001796:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x8009"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5000"
    ProgrammingWaitTime_erase: str = "6000"
    ProgrammingWaitTime_lvpgm: str = "2500"
    ProgrammingWaitTime_pgm: str = "2500"
    ProgrammingWaitTime_userid: str = "2500"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x7f"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x7f"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5000"
    Wait_erase: str = "6000"
    Wait_lvpgm: str = "2500"
    Wait_pgm: str = "2500"
    Wait_userid: str = "2500"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    fuses_impl: str = "0EFB3E03"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "1"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pgmspec: str = "40001796"
    tries: str = "1"

@dataclass
class PIC16LF1566(PS40001796):
    name: str = "16LF1566"
    devid: str = "0x3046"

@dataclass
class PIC16LF1567(PS40001796):
    name: str = "16LF1567"
    devid: str = "0x3047"


@dataclass
class PS40001822:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x310000"
    EEDataSector_endaddr: str = "0x310400"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "25200"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200010"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "25200"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "18xxxx"
    fuses_impl: str = "7729E3BF7F3F0F3703000F02"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "7"
    lvpthresh: str = "2700"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "40001822"
    tries: str = "1"

@dataclass
class PIC18F65K40(PS40001822):
    name: str = "18F65K40"
    devid: str = "0x6B00"

@dataclass
class PIC18F66K40(PS40001822):
    name: str = "18F66K40"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x6AE0"

@dataclass
class PIC18F67K40(PS40001822):
    name: str = "18F67K40"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    devid: str = "0x6AC0"
    fuses_impl: str = "7729E3BF7F3FFF370300FF02"

@dataclass
class PIC18LF65K40(PS40001822):
    name: str = "18LF65K40"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6B60"

@dataclass
class PIC18LF66K40(PS40001822):
    name: str = "18LF66K40"
    CodeSector_endaddr: str = "0x10000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6B40"

@dataclass
class PIC18LF67K40(PS40001822):
    name: str = "18LF67K40"
    CodeSector_endaddr: str = "0x20000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6B20"
    fuses_impl: str = "7729E3BF7F3FFF370300FF02"


@dataclass
class PS40001836:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x4000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000a"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x310000"
    EEDataSector_endaddr: str = "0x310100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200010"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "8400"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "18xxxx"
    erasepagesize: str = "64"
    fuses_impl: str = "772BFFBF7F3F9F2F0100"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "7"
    lvpthresh: str = "2700"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "64"
    panelsize: str = "0"
    pgmspec: str = "40001836"
    tries: str = "1"

@dataclass
class PIC18F24K42(PS40001836):
    name: str = "18F24K42"
    devid: str = "0x6CA0"

@dataclass
class PIC18F25K42(PS40001836):
    name: str = "18F25K42"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x6C80"

@dataclass
class PIC18LF24K42(PS40001836):
    name: str = "18LF24K42"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6DE0"

@dataclass
class PIC18LF25K42(PS40001836):
    name: str = "18LF25K42"
    CodeSector_endaddr: str = "0x8000"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6DC0"


@dataclass
class PS40001838:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataMemTraits_magicoffset: str = "0xF000"
    Latches_cfg: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2375"
    VDD_minvoltage: str = "2375"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5600"
    Wait_erase: str = "8400"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "32"
    fuses_impl: str = "29773EE33F7F2B9F0001"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40001838"
    tries: str = "1"

@dataclass
class PIC16F15313(PS40001838):
    name: str = "16F15313"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x30BE"

@dataclass
class PIC16F15323(PS40001838):
    name: str = "16F15323"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x30C0"

@dataclass
class PIC16F15324(PS40001838):
    name: str = "16F15324"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x30C2"

@dataclass
class PIC16F15325(PS40001838):
    name: str = "16F15325"
    devid: str = "0x30C6"

@dataclass
class PIC16F15344(PS40001838):
    name: str = "16F15344"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x30C4"

@dataclass
class PIC16F15345(PS40001838):
    name: str = "16F15345"
    devid: str = "0x30C8"

@dataclass
class PIC16F15354(PS40001838):
    name: str = "16F15354"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x30AC"

@dataclass
class PIC16F15355(PS40001838):
    name: str = "16F15355"
    devid: str = "0x30AE"

@dataclass
class PIC16F15356(PS40001838):
    name: str = "16F15356"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30B0"

@dataclass
class PIC16F15375(PS40001838):
    name: str = "16F15375"
    devid: str = "0x30B2"

@dataclass
class PIC16F15376(PS40001838):
    name: str = "16F15376"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30B4"

@dataclass
class PIC16F15385(PS40001838):
    name: str = "16F15385"
    devid: str = "0x30B6"

@dataclass
class PIC16F15386(PS40001838):
    name: str = "16F15386"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30B8"

@dataclass
class PIC16LF15313(PS40001838):
    name: str = "16LF15313"
    CodeSector_endaddr: str = "0x800"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30BF"

@dataclass
class PIC16LF15323(PS40001838):
    name: str = "16LF15323"
    CodeSector_endaddr: str = "0x800"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30C1"

@dataclass
class PIC16LF15324(PS40001838):
    name: str = "16LF15324"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30C3"

@dataclass
class PIC16LF15325(PS40001838):
    name: str = "16LF15325"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30C7"

@dataclass
class PIC16LF15344(PS40001838):
    name: str = "16LF15344"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30C5"

@dataclass
class PIC16LF15345(PS40001838):
    name: str = "16LF15345"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30C9"

@dataclass
class PIC16LF15354(PS40001838):
    name: str = "16LF15354"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30AD"

@dataclass
class PIC16LF15355(PS40001838):
    name: str = "16LF15355"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30AF"

@dataclass
class PIC16LF15356(PS40001838):
    name: str = "16LF15356"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30B1"

@dataclass
class PIC16LF15375(PS40001838):
    name: str = "16LF15375"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x30B3"

@dataclass
class PIC16LF15376(PS40001838):
    name: str = "16LF15376"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x30B5"

@dataclass
class PIC16LF15385(PS40001838):
    name: str = "16LF15385"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3330"
    devid: str = "0x30B7"

@dataclass
class PIC16LF15386(PS40001838):
    name: str = "16LF15386"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3330"
    devid: str = "0x30B9"


@dataclass
class PS40001846:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "14000"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2375"
    VDD_minvoltage: str = "2375"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "14000"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "64"
    fuses_impl: str = "29773EE13F7F2F9F0001"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "64"
    pgmspec: str = "40001846"
    tries: str = "1"

@dataclass
class PIC16F19195(PS40001846):
    name: str = "16F19195"
    devid: str = "0x309E"
    fuses_impl: str = "2F773EE73F7F2F9F0001"

@dataclass
class PIC16F19196(PS40001846):
    name: str = "16F19196"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30A0"

@dataclass
class PIC16F19197(PS40001846):
    name: str = "16F19197"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x30A2"

@dataclass
class PIC16LF19195(PS40001846):
    name: str = "16LF19195"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x309F"

@dataclass
class PIC16LF19196(PS40001846):
    name: str = "16LF19196"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x30A1"

@dataclass
class PIC16LF19197(PS40001846):
    name: str = "16LF19197"
    CodeSector_endaddr: str = "0x8000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x30A3"


@dataclass
class PS40001874:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x310000"
    EEDataSector_endaddr: str = "0x310400"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "128"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "128"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "65"
    ProgrammingWaitTime_eedata: str = "11000"
    ProgrammingWaitTime_erase: str = "65535"
    ProgrammingWaitTime_lverase: str = "11000"
    ProgrammingWaitTime_lvpgm: str = "65"
    ProgrammingWaitTime_pgm: str = "65"
    ProgrammingWaitTime_rowerase: str = "11000"
    ProgrammingWaitTime_userid: str = "65"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200100"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "65"
    Wait_eedata: str = "11000"
    Wait_erase: str = "65535"
    Wait_lverase: str = "11000"
    Wait_lvpgm: str = "65"
    Wait_pgm: str = "65"
    Wait_rowerase: str = "11000"
    Wait_userid: str = "65"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "7729E3BF7F3F033703000302"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "7"
    lvpthresh: str = "2100"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "1"
    panelsize: str = "0"
    pgmspec: str = "40001874"
    tries: str = "1"

@dataclass
class PIC18F24Q10(PS40001874):
    name: str = "18F24Q10"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0x310100"
    devid: str = "0x71C0"

@dataclass
class PIC18F25Q10(PS40001874):
    name: str = "18F25Q10"
    EEDataSector_endaddr: str = "0x310100"
    devid: str = "0x71A0"

@dataclass
class PIC18F26Q10(PS40001874):
    name: str = "18F26Q10"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7180"

@dataclass
class PIC18F27Q10(PS40001874):
    name: str = "18F27Q10"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x7100"
    fuses_impl: str = "7729E3BF7F3FF3370300F302"

@dataclass
class PIC18F45Q10(PS40001874):
    name: str = "18F45Q10"
    EEDataSector_endaddr: str = "0x310100"
    devid: str = "0x7140"

@dataclass
class PIC18F46Q10(PS40001874):
    name: str = "18F46Q10"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7120"

@dataclass
class PIC18F47Q10(PS40001874):
    name: str = "18F47Q10"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x70E0"
    fuses_impl: str = "7729E3BF7F3FF3370300F302"


@dataclass
class PS40001880:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "14000"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2375"
    VDD_minvoltage: str = "2375"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "14000"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "32"
    fuses_impl: str = "29773EE13F7F2F9F0001"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40001880"
    tries: str = "1"

@dataclass
class PIC16F19155(PS40001880):
    name: str = "16F19155"
    devid: str = "0x3096"

@dataclass
class PIC16F19156(PS40001880):
    name: str = "16F19156"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x3098"

@dataclass
class PIC16F19175(PS40001880):
    name: str = "16F19175"
    devid: str = "0x309A"

@dataclass
class PIC16F19176(PS40001880):
    name: str = "16F19176"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x309C"

@dataclass
class PIC16F19185(PS40001880):
    name: str = "16F19185"
    devid: str = "0x30BA"

@dataclass
class PIC16F19186(PS40001880):
    name: str = "16F19186"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30BC"

@dataclass
class PIC16LF19155(PS40001880):
    name: str = "16LF19155"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x3097"

@dataclass
class PIC16LF19156(PS40001880):
    name: str = "16LF19156"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x3099"

@dataclass
class PIC16LF19175(PS40001880):
    name: str = "16LF19175"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x309B"

@dataclass
class PIC16LF19176(PS40001880):
    name: str = "16LF19176"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x309D"

@dataclass
class PIC16LF19185(PS40001880):
    name: str = "16LF19185"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x30BB"

@dataclass
class PIC16LF19186(PS40001880):
    name: str = "16LF19186"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    VPP_minvoltage: str = "8000"
    devid: str = "0x30BD"


@dataclass
class PS40001886:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x10000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000a"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x310000"
    EEDataSector_endaddr: str = "0x310400"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200010"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "8400"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "772BFFBF7F3F9F2F0100"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "7"
    lvpthresh: str = "2700"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "128"
    panelsize: str = "0"
    pgmspec: str = "40001886"
    tries: str = "1"

@dataclass
class PIC18F26K42(PS40001886):
    name: str = "18F26K42"
    devid: str = "0x6C60"

@dataclass
class PIC18F27K42(PS40001886):
    name: str = "18F27K42"
    CodeSector_endaddr: str = "0x20000"
    ProgrammingWaitTime_erase: str = "16800"
    Wait_erase: str = "16800"
    devid: str = "0x6C40"

@dataclass
class PIC18F45K42(PS40001886):
    name: str = "18F45K42"
    CodeSector_endaddr: str = "0x8000"
    EEDataSector_endaddr: str = "0x310100"
    devid: str = "0x6C20"

@dataclass
class PIC18F46K42(PS40001886):
    name: str = "18F46K42"
    devid: str = "0x6C00"

@dataclass
class PIC18F47K42(PS40001886):
    name: str = "18F47K42"
    CodeSector_endaddr: str = "0x20000"
    ProgrammingWaitTime_erase: str = "16800"
    Wait_erase: str = "16800"
    devid: str = "0x6BE0"

@dataclass
class PIC18F55K42(PS40001886):
    name: str = "18F55K42"
    CodeSector_endaddr: str = "0x8000"
    EEDataSector_endaddr: str = "0x310100"
    devid: str = "0x6BC0"

@dataclass
class PIC18F56K42(PS40001886):
    name: str = "18F56K42"
    devid: str = "0x6BA0"

@dataclass
class PIC18F57K42(PS40001886):
    name: str = "18F57K42"
    CodeSector_endaddr: str = "0x20000"
    ProgrammingWaitTime_erase: str = "16800"
    Wait_erase: str = "16800"
    devid: str = "0x6B80"

@dataclass
class PIC18LF26K42(PS40001886):
    name: str = "18LF26K42"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6DA0"

@dataclass
class PIC18LF27K42(PS40001886):
    name: str = "18LF27K42"
    CodeSector_endaddr: str = "0x20000"
    ProgrammingWaitTime_erase: str = "16800"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    Wait_erase: str = "16800"
    devid: str = "0x6D80"

@dataclass
class PIC18LF45K42(PS40001886):
    name: str = "18LF45K42"
    CodeSector_endaddr: str = "0x8000"
    EEDataSector_endaddr: str = "0x310100"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6D60"

@dataclass
class PIC18LF46K42(PS40001886):
    name: str = "18LF46K42"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6D40"

@dataclass
class PIC18LF47K42(PS40001886):
    name: str = "18LF47K42"
    CodeSector_endaddr: str = "0x20000"
    ProgrammingWaitTime_erase: str = "16800"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    Wait_erase: str = "16800"
    devid: str = "0x6D20"

@dataclass
class PIC18LF55K42(PS40001886):
    name: str = "18LF55K42"
    CodeSector_endaddr: str = "0x8000"
    EEDataSector_endaddr: str = "0x310100"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6D00"

@dataclass
class PIC18LF56K42(PS40001886):
    name: str = "18LF56K42"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6CE0"

@dataclass
class PIC18LF57K42(PS40001886):
    name: str = "18LF57K42"
    CodeSector_endaddr: str = "0x20000"
    ProgrammingWaitTime_erase: str = "16800"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    Wait_erase: str = "16800"
    devid: str = "0x6CC0"


@dataclass
class PS40001927:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000a"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x310000"
    EEDataSector_endaddr: str = "0x310400"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "64"
    Latches_rowerase: str = "64"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "64"
    ProgrammingRowSize_rowerase: str = "64"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "25200"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200010"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2300"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "25200"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "18xxxx"
    fuses_impl: str = "772BFFBF7F3F9F2F0100"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "7"
    lvpthresh: str = "2700"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    panelsize: str = "0"
    pgmspec: str = "40001927"
    tries: str = "1"

@dataclass
class PIC18F25K83(PS40001927):
    name: str = "18F25K83"
    devid: str = "0x6EE0"

@dataclass
class PIC18F26K83(PS40001927):
    name: str = "18F26K83"
    CodeSector_endaddr: str = "0x10000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    devid: str = "0x6EC0"

@dataclass
class PIC18LF25K83(PS40001927):
    name: str = "18LF25K83"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6F20"

@dataclass
class PIC18LF26K83(PS40001927):
    name: str = "18LF26K83"
    CodeSector_endaddr: str = "0x10000"
    Latches_pgm: str = "128"
    Latches_rowerase: str = "128"
    ProgrammingRowSize_pgm: str = "128"
    ProgrammingRowSize_rowerase: str = "128"
    VDD_maxdefaultvoltage: str = "3600"
    VDD_maxvoltage: str = "3600"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "3300"
    devid: str = "0x6F00"


@dataclass
class PS40001970:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x2000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2375"
    VDD_minvoltage: str = "2375"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "8000"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "8400"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "32"
    fuses_impl: str = "29773EE73F7F2F9F0001"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40001970"
    tries: str = "1"

@dataclass
class PIC16F18424(PS40001970):
    name: str = "16F18424"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x30CA"

@dataclass
class PIC16F18425(PS40001970):
    name: str = "16F18425"
    devid: str = "0x30CC"

@dataclass
class PIC16F18426(PS40001970):
    name: str = "16F18426"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30D2"

@dataclass
class PIC16F18444(PS40001970):
    name: str = "16F18444"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x30CE"

@dataclass
class PIC16F18445(PS40001970):
    name: str = "16F18445"
    devid: str = "0x30D0"

@dataclass
class PIC16F18446(PS40001970):
    name: str = "16F18446"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30D4"

@dataclass
class PIC16F18455(PS40001970):
    name: str = "16F18455"
    devid: str = "0x30D7"

@dataclass
class PIC16F18456(PS40001970):
    name: str = "16F18456"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30D9"

@dataclass
class PIC16LF18424(PS40001970):
    name: str = "16LF18424"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30CB"

@dataclass
class PIC16LF18425(PS40001970):
    name: str = "16LF18425"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30CD"

@dataclass
class PIC16LF18426(PS40001970):
    name: str = "16LF18426"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30D3"

@dataclass
class PIC16LF18444(PS40001970):
    name: str = "16LF18444"
    CodeSector_endaddr: str = "0x1000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30CF"

@dataclass
class PIC16LF18445(PS40001970):
    name: str = "16LF18445"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30D1"

@dataclass
class PIC16LF18446(PS40001970):
    name: str = "16LF18446"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30D5"

@dataclass
class PIC16LF18455(PS40001970):
    name: str = "16LF18455"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30D8"

@dataclass
class PIC16LF18456(PS40001970):
    name: str = "16LF18456"
    CodeSector_endaddr: str = "0x4000"
    VDD_maxdefaultvoltage: str = "3625"
    VDD_maxvoltage: str = "3625"
    VDD_mindefaultvoltage: str = "1875"
    VDD_minvoltage: str = "1875"
    VDD_nominalvoltage: str = "3250"
    devid: str = "0x30DA"


@dataclass
class PS40002079:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x300012"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x380000"
    EEDataSector_endaddr: str = "0x380400"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "128"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "128"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "11000"
    ProgrammingWaitTime_eedata: str = "11000"
    ProgrammingWaitTime_erase: str = "11000"
    ProgrammingWaitTime_lverase: str = "11000"
    ProgrammingWaitTime_lvpgm: str = "75"
    ProgrammingWaitTime_pgm: str = "75"
    ProgrammingWaitTime_rowerase: str = "11000"
    ProgrammingWaitTime_userid: str = "75"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200040"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2100"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "11000"
    Wait_eedata: str = "11000"
    Wait_erase: str = "11000"
    Wait_lverase: str = "11000"
    Wait_lvpgm: str = "75"
    Wait_pgm: str = "75"
    Wait_rowerase: str = "11000"
    Wait_userid: str = "75"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "772BFFBF7F3F3F8F0001"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2100"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "128"
    panelsize: str = "0"
    pgmspec: str = "40002079"
    tries: str = "1"

@dataclass
class PIC18F25Q43(PS40002079):
    name: str = "18F25Q43"
    devid: str = "0x73C0"

@dataclass
class PIC18F26Q43(PS40002079):
    name: str = "18F26Q43"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7420"

@dataclass
class PIC18F27Q43(PS40002079):
    name: str = "18F27Q43"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x7480"

@dataclass
class PIC18F45Q43(PS40002079):
    name: str = "18F45Q43"
    devid: str = "0x73E0"

@dataclass
class PIC18F46Q43(PS40002079):
    name: str = "18F46Q43"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7440"

@dataclass
class PIC18F47Q43(PS40002079):
    name: str = "18F47Q43"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x74A0"

@dataclass
class PIC18F55Q43(PS40002079):
    name: str = "18F55Q43"
    devid: str = "0x7400"

@dataclass
class PIC18F56Q43(PS40002079):
    name: str = "18F56Q43"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7460"

@dataclass
class PIC18F57Q43(PS40002079):
    name: str = "18F57Q43"
    CodeSector_endaddr: str = "0x20000"
    VDD_mindefaultvoltage: str = "1800"
    devid: str = "0x74C0"


@dataclass
class PS40002137:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x10000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x300024"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x380000"
    EEDataSector_endaddr: str = "0x380400"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "128"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "128"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "16800"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200040"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2100"
    VDD_minvoltage: str = "2300"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "16800"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "77FBFFBF7F3F1F0F3301FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2100"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "128"
    panelsize: str = "0"
    pgmspec: str = "40002137"
    tries: str = "1"

@dataclass
class PIC18F26Q83(PS40002137):
    name: str = "18F26Q83"
    devid: str = "0xA306"

@dataclass
class PIC18F26Q84(PS40002137):
    name: str = "18F26Q84"
    devid: str = "0xA300"

@dataclass
class PIC18F27Q83(PS40002137):
    name: str = "18F27Q83"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x9909"

@dataclass
class PIC18F27Q84(PS40002137):
    name: str = "18F27Q84"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x9903"

@dataclass
class PIC18F46Q83(PS40002137):
    name: str = "18F46Q83"
    devid: str = "0xA307"

@dataclass
class PIC18F46Q84(PS40002137):
    name: str = "18F46Q84"
    devid: str = "0xA301"

@dataclass
class PIC18F47Q83(PS40002137):
    name: str = "18F47Q83"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x990A"

@dataclass
class PIC18F47Q84(PS40002137):
    name: str = "18F47Q84"
    CodeSector_endaddr: str = "0x20000"
    devid: str = "0x9904"

@dataclass
class PIC18F56Q83(PS40002137):
    name: str = "18F56Q83"
    VDD_minvoltage: str = "1800"
    devid: str = "0xA308"

@dataclass
class PIC18F56Q84(PS40002137):
    name: str = "18F56Q84"
    VDD_minvoltage: str = "1800"
    devid: str = "0xA302"
    haschecksum: str = "0"

@dataclass
class PIC18F57Q83(PS40002137):
    name: str = "18F57Q83"
    CodeSector_endaddr: str = "0x20000"
    VDD_minvoltage: str = "1800"
    devid: str = "0x990B"

@dataclass
class PIC18F57Q84(PS40002137):
    name: str = "18F57Q84"
    CodeSector_endaddr: str = "0x20000"
    VDD_minvoltage: str = "1800"
    devid: str = "0x9905"
    haschecksum: str = "0"


@dataclass
class PS40002143:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x4000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x300012"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x380000"
    EEDataSector_endaddr: str = "0x380200"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "256"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "256"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "11000"
    ProgrammingWaitTime_eedata: str = "11000"
    ProgrammingWaitTime_erase: str = "11000"
    ProgrammingWaitTime_lverase: str = "11000"
    ProgrammingWaitTime_lvpgm: str = "75"
    ProgrammingWaitTime_pgm: str = "75"
    ProgrammingWaitTime_rowerase: str = "11000"
    ProgrammingWaitTime_userid: str = "75"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200040"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2100"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "11000"
    Wait_eedata: str = "11000"
    Wait_erase: str = "11000"
    Wait_lverase: str = "11000"
    Wait_lvpgm: str = "75"
    Wait_pgm: str = "75"
    Wait_rowerase: str = "11000"
    Wait_userid: str = "75"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "77EBFFBF7F3F1F0F01"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2100"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "128"
    panelsize: str = "0"
    pgmspec: str = "40002143"
    tries: str = "1"

@dataclass
class PIC18F04Q41(PS40002143):
    name: str = "18F04Q41"
    devid: str = "0x7540"

@dataclass
class PIC18F05Q41(PS40002143):
    name: str = "18F05Q41"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x7500"

@dataclass
class PIC18F06Q41(PS40002143):
    name: str = "18F06Q41"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7580"

@dataclass
class PIC18F14Q41(PS40002143):
    name: str = "18F14Q41"
    devid: str = "0x7520"

@dataclass
class PIC18F15Q41(PS40002143):
    name: str = "18F15Q41"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x74E0"

@dataclass
class PIC18F16Q41(PS40002143):
    name: str = "18F16Q41"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7560"


@dataclass
class PS40002149:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "8400"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "8192"
    fuses_impl: str = "11333ADF00002B9F0001"
    haschecksum: str = "0"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40002149"
    tries: str = "1"

@dataclass
class PIC16F15213(PS40002149):
    name: str = "16F15213"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x30E3"
    erasepagesize: str = "2048"

@dataclass
class PIC16F15214(PS40002149):
    name: str = "16F15214"
    devid: str = "0x30E6"
    erasepagesize: str = "4096"

@dataclass
class PIC16F15223(PS40002149):
    name: str = "16F15223"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x30E4"
    erasepagesize: str = "2048"

@dataclass
class PIC16F15224(PS40002149):
    name: str = "16F15224"
    devid: str = "0x30E7"
    erasepagesize: str = "4096"

@dataclass
class PIC16F15225(PS40002149):
    name: str = "16F15225"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30E9"

@dataclass
class PIC16F15243(PS40002149):
    name: str = "16F15243"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x30E5"
    erasepagesize: str = "2048"
    haslvp2: str = "0"

@dataclass
class PIC16F15244(PS40002149):
    name: str = "16F15244"
    devid: str = "0x30E8"
    erasepagesize: str = "4096"

@dataclass
class PIC16F15245(PS40002149):
    name: str = "16F15245"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30EA"

@dataclass
class PIC16F15254(PS40002149):
    name: str = "16F15254"
    devid: str = "0x30F0"

@dataclass
class PIC16F15255(PS40002149):
    name: str = "16F15255"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30EF"

@dataclass
class PIC16F15256(PS40002149):
    name: str = "16F15256"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30EB"

@dataclass
class PIC16F15274(PS40002149):
    name: str = "16F15274"
    devid: str = "0x30EE"
    erasepagesize: str = "4096"

@dataclass
class PIC16F15275(PS40002149):
    name: str = "16F15275"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30ED"

@dataclass
class PIC16F15276(PS40002149):
    name: str = "16F15276"
    CodeSector_endaddr: str = "0x4000"
    devid: str = "0x30EC"
    erasepagesize: str = "16384"


@dataclass
class PS40002185:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x4000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x300012"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x380000"
    EEDataSector_endaddr: str = "0x380200"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "256"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "256"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "11000"
    ProgrammingWaitTime_eedata: str = "11000"
    ProgrammingWaitTime_erase: str = "11000"
    ProgrammingWaitTime_lverase: str = "11000"
    ProgrammingWaitTime_lvpgm: str = "75"
    ProgrammingWaitTime_pgm: str = "75"
    ProgrammingWaitTime_rowerase: str = "11000"
    ProgrammingWaitTime_userid: str = "75"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200040"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "2100"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "11000"
    Wait_eedata: str = "11000"
    Wait_erase: str = "11000"
    Wait_lverase: str = "11000"
    Wait_lvpgm: str = "75"
    Wait_pgm: str = "75"
    Wait_rowerase: str = "11000"
    Wait_userid: str = "75"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "77EBFFBF7F3F1F0F01"
    haschecksum: str = "0"
    hashighvoltagemclr: str = "1"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2100"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "128"
    panelsize: str = "0"
    pgmspec: str = "40002185"
    tries: str = "1"

@dataclass
class PIC18F04Q40(PS40002185):
    name: str = "18F04Q40"
    devid: str = "0x7640"

@dataclass
class PIC18F05Q40(PS40002185):
    name: str = "18F05Q40"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x7600"

@dataclass
class PIC18F06Q40(PS40002185):
    name: str = "18F06Q40"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x75C0"

@dataclass
class PIC18F14Q40(PS40002185):
    name: str = "18F14Q40"
    devid: str = "0x7620"

@dataclass
class PIC18F15Q40(PS40002185):
    name: str = "18F15Q40"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x75E0"

@dataclass
class PIC18F16Q40(PS40002185):
    name: str = "18F16Q40"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x75A0"


@dataclass
class PS40002266:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf080"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "8400"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "4096"
    fuses_impl: str = "39773FE73F7F2F9F0003"
    haschecksum: str = "0"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40002266"
    tries: str = "1"

@dataclass
class PIC16F17114(PS40002266):
    name: str = "16F17114"
    devid: str = "0x30DB"

@dataclass
class PIC16F17115(PS40002266):
    name: str = "16F17115"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30E2"
    erasepagesize: str = "8192"

@dataclass
class PIC16F17124(PS40002266):
    name: str = "16F17124"
    devid: str = "0x30DC"

@dataclass
class PIC16F17125(PS40002266):
    name: str = "16F17125"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30DE"
    erasepagesize: str = "8192"

@dataclass
class PIC16F17126(PS40002266):
    name: str = "16F17126"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x30E0"
    erasepagesize: str = "16384"

@dataclass
class PIC16F17144(PS40002266):
    name: str = "16F17144"
    devid: str = "0x30DD"

@dataclass
class PIC16F17145(PS40002266):
    name: str = "16F17145"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30DF"
    erasepagesize: str = "8192"

@dataclass
class PIC16F17146(PS40002266):
    name: str = "16F17146"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x30E1"
    erasepagesize: str = "16384"

@dataclass
class PIC16F17154(PS40002266):
    name: str = "16F17154"
    devid: str = "0x3101"

@dataclass
class PIC16F17155(PS40002266):
    name: str = "16F17155"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3103"
    erasepagesize: str = "8192"

@dataclass
class PIC16F17156(PS40002266):
    name: str = "16F17156"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x3105"
    erasepagesize: str = "16384"

@dataclass
class PIC16F17174(PS40002266):
    name: str = "16F17174"
    devid: str = "0x3102"

@dataclass
class PIC16F17175(PS40002266):
    name: str = "16F17175"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3104"
    erasepagesize: str = "8192"

@dataclass
class PIC16F17176(PS40002266):
    name: str = "16F17176"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x3106"
    erasepagesize: str = "16384"


@dataclass
class PS40002276:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf080"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "8400"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "4096"
    fuses_impl: str = "39773FE73F7F2F9F0003"
    haschecksum: str = "0"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40002276"
    tries: str = "1"

@dataclass
class PIC16F18114(PS40002276):
    name: str = "16F18114"
    devid: str = "0x3107"

@dataclass
class PIC16F18115(PS40002276):
    name: str = "16F18115"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x310C"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18124(PS40002276):
    name: str = "16F18124"
    devid: str = "0x3108"

@dataclass
class PIC16F18125(PS40002276):
    name: str = "16F18125"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x310D"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18126(PS40002276):
    name: str = "16F18126"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x3111"
    erasepagesize: str = "16384"

@dataclass
class PIC16F18144(PS40002276):
    name: str = "16F18144"
    devid: str = "0x3109"

@dataclass
class PIC16F18145(PS40002276):
    name: str = "16F18145"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x310E"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18146(PS40002276):
    name: str = "16F18146"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x3112"
    erasepagesize: str = "16384"

@dataclass
class PIC16F18154(PS40002276):
    name: str = "16F18154"
    devid: str = "0x310A"

@dataclass
class PIC16F18155(PS40002276):
    name: str = "16F18155"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x310F"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18156(PS40002276):
    name: str = "16F18156"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x3113"
    erasepagesize: str = "16384"

@dataclass
class PIC16F18174(PS40002276):
    name: str = "16F18174"
    devid: str = "0x310B"

@dataclass
class PIC16F18175(PS40002276):
    name: str = "16F18175"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3110"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18176(PS40002276):
    name: str = "16F18176"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x3114"
    erasepagesize: str = "16384"


@dataclass
class PS40002306:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x4000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30000b"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x380000"
    EEDataSector_endaddr: str = "0x380100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "256"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "256"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "11000"
    ProgrammingWaitTime_eedata: str = "11000"
    ProgrammingWaitTime_erase: str = "11000"
    ProgrammingWaitTime_lverase: str = "11000"
    ProgrammingWaitTime_lvpgm: str = "75"
    ProgrammingWaitTime_pgm: str = "75"
    ProgrammingWaitTime_rowerase: str = "11000"
    ProgrammingWaitTime_userid: str = "75"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200040"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "11000"
    Wait_eedata: str = "11000"
    Wait_erase: str = "11000"
    Wait_lverase: str = "11000"
    Wait_lvpgm: str = "75"
    Wait_pgm: str = "75"
    Wait_rowerase: str = "11000"
    Wait_userid: str = "75"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "77EBFFBF7F3FFFFF8F0101"
    haschecksum: str = "0"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "3"
    lvpthresh: str = "1800"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "128"
    panelsize: str = "0"
    pgmspec: str = "40002306"
    tries: str = "1"

@dataclass
class PIC18F24Q71(PS40002306):
    name: str = "18F24Q71"
    devid: str = "0x7660"

@dataclass
class PIC18F25Q71(PS40002306):
    name: str = "18F25Q71"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x76A0"

@dataclass
class PIC18F26Q71(PS40002306):
    name: str = "18F26Q71"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x76E0"

@dataclass
class PIC18F44Q71(PS40002306):
    name: str = "18F44Q71"
    devid: str = "0x77A0"

@dataclass
class PIC18F45Q71(PS40002306):
    name: str = "18F45Q71"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x77E0"

@dataclass
class PIC18F46Q71(PS40002306):
    name: str = "18F46Q71"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7720"

@dataclass
class PIC18F54Q71(PS40002306):
    name: str = "18F54Q71"
    devid: str = "0x7820"
    haslvp2: str = "0"

@dataclass
class PIC18F55Q71(PS40002306):
    name: str = "18F55Q71"
    CodeSector_endaddr: str = "0x8000"
    devid: str = "0x7860"

@dataclass
class PIC18F56Q71(PS40002306):
    name: str = "18F56Q71"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7760"


@dataclass
class PS40002317:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x1000"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    EEDataSector_beginaddr: str = "0xf000"
    EEDataSector_endaddr: str = "0xf080"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "8400"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "4096"
    fuses_impl: str = "11733FDF00002F9F0003"
    haschecksum: str = "0"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40002317"
    tries: str = "1"

@dataclass
class PIC16F18013(PS40002317):
    name: str = "16F18013"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x30F1"
    erasepagesize: str = "2048"

@dataclass
class PIC16F18014(PS40002317):
    name: str = "16F18014"
    devid: str = "0x30F2"

@dataclass
class PIC16F18015(PS40002317):
    name: str = "16F18015"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30F5"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18023(PS40002317):
    name: str = "16F18023"
    CodeSector_endaddr: str = "0x800"
    devid: str = "0x30F3"
    erasepagesize: str = "2048"

@dataclass
class PIC16F18024(PS40002317):
    name: str = "16F18024"
    devid: str = "0x30F4"

@dataclass
class PIC16F18025(PS40002317):
    name: str = "16F18025"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30F6"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18026(PS40002317):
    name: str = "16F18026"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x30F9"
    erasepagesize: str = "16384"

@dataclass
class PIC16F18044(PS40002317):
    name: str = "16F18044"
    devid: str = "0x30F7"

@dataclass
class PIC16F18045(PS40002317):
    name: str = "16F18045"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30F8"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18046(PS40002317):
    name: str = "16F18046"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x30FA"
    erasepagesize: str = "16384"

@dataclass
class PIC16F18054(PS40002317):
    name: str = "16F18054"
    devid: str = "0x30FB"

@dataclass
class PIC16F18055(PS40002317):
    name: str = "16F18055"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30FC"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18056(PS40002317):
    name: str = "16F18056"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x30FF"
    erasepagesize: str = "16384"

@dataclass
class PIC16F18074(PS40002317):
    name: str = "16F18074"
    devid: str = "0x30FD"

@dataclass
class PIC16F18075(PS40002317):
    name: str = "16F18075"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x30FE"
    erasepagesize: str = "8192"

@dataclass
class PIC16F18076(PS40002317):
    name: str = "16F18076"
    CodeSector_endaddr: str = "0x4000"
    EEDataSector_endaddr: str = "0xf100"
    devid: str = "0x3100"
    erasepagesize: str = "16384"


@dataclass
class PS40002327:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x10000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30001a"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x380000"
    EEDataSector_endaddr: str = "0x380100"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "256"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "256"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "11000"
    ProgrammingWaitTime_eedata: str = "11000"
    ProgrammingWaitTime_erase: str = "11000"
    ProgrammingWaitTime_lverase: str = "11000"
    ProgrammingWaitTime_lvpgm: str = "75"
    ProgrammingWaitTime_pgm: str = "75"
    ProgrammingWaitTime_rowerase: str = "11000"
    ProgrammingWaitTime_userid: str = "75"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200040"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "11000"
    Wait_eedata: str = "11000"
    Wait_erase: str = "11000"
    Wait_lverase: str = "11000"
    Wait_lvpgm: str = "75"
    Wait_pgm: str = "75"
    Wait_rowerase: str = "11000"
    Wait_userid: str = "75"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "77EFFFFB7F3F01FF8F01010000000000000000000000000001FF"
    haschecksum: str = "0"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "3"
    lvpthresh: str = "1800"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "128"
    panelsize: str = "0"
    pgmspec: str = "40002327"
    tries: str = "1"

@dataclass
class PIC18F06Q20(PS40002327):
    name: str = "18F06Q20"
    devid: str = "0x7A60"

@dataclass
class PIC18F16Q20(PS40002327):
    name: str = "18F16Q20"
    devid: str = "0x7A40"
    fuses_impl: str = "77EFFFFB7F3F03FF8F01010000000000000000000000000001FF"


@dataclass
class PS40002414:
    CodeMemTraits_locsize: str = "0x2"
    CodeMemTraits_wordimpl: str = "0xffff"
    CodeMemTraits_wordinit: str = "0xffff"
    CodeMemTraits_wordsafe: str = "0xffff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x8000"
    ConfigFuseMemTraits_locsize: str = "0x1"
    ConfigFuseMemTraits_wordimpl: str = "0xff"
    ConfigFuseMemTraits_wordinit: str = "0xff"
    ConfigFuseMemTraits_wordsafe: str = "0xff"
    ConfigFuseMemTraits_wordsize: str = "0x1"
    ConfigFuseSector_beginaddr: str = "0x300000"
    ConfigFuseSector_endaddr: str = "0x30001d"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0xff"
    DeviceIDMemTraits_wordinit: str = "0xff"
    DeviceIDMemTraits_wordsafe: str = "0xff"
    DeviceIDMemTraits_wordsize: str = "0x1"
    DeviceIDSector_beginaddr: str = "0x3ffffe"
    DeviceIDSector_endaddr: str = "0x400000"
    EEDataMemTraits_magicoffset: str = "0x000000"
    EEDataSector_beginaddr: str = "0x380000"
    EEDataSector_endaddr: str = "0x380200"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_pgm: str = "1"
    Latches_rowerase: str = "256"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_pgm: str = "1"
    ProgrammingRowSize_rowerase: str = "256"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "11000"
    ProgrammingWaitTime_eedata: str = "11000"
    ProgrammingWaitTime_erase: str = "11000"
    ProgrammingWaitTime_lverase: str = "11000"
    ProgrammingWaitTime_lvpgm: str = "75"
    ProgrammingWaitTime_pgm: str = "75"
    ProgrammingWaitTime_rowerase: str = "11000"
    ProgrammingWaitTime_userid: str = "75"
    RevisionIDSector_beginaddr: str = "0x3ffffc"
    RevisionIDSector_endaddr: str = "0x3ffffe"
    UserIDMemTraits_addrinc: str = "0x2"
    UserIDMemTraits_locsize: str = "0x2"
    UserIDMemTraits_wordimpl: str = "0xffff"
    UserIDMemTraits_wordinit: str = "0xffff"
    UserIDMemTraits_wordsafe: str = "0xffff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x200000"
    UserIDSector_endaddr: str = "0x200040"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "9000"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "11000"
    Wait_eedata: str = "11000"
    Wait_erase: str = "11000"
    Wait_lverase: str = "11000"
    Wait_lvpgm: str = "75"
    Wait_pgm: str = "75"
    Wait_rowerase: str = "11000"
    Wait_userid: str = "75"
    architecture: str = "18xxxx"
    erasepagesize: str = "128"
    fuses_impl: str = "77FBFFBF7F3FFF008F00010000000000000000000000000001FF000001"
    haschecksum: str = "0"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x0020"
    lvpbit_offset: str = "3"
    lvpthresh: str = "1900"
    mask: str = "0xFFFF"
    memtech: str = "ee"
    pagesize: str = "128"
    panelsize: str = "0"
    pgmspec: str = "40002414"
    tries: str = "1"

@dataclass
class PIC18F24Q24(PS40002414):
    name: str = "18F24Q24"
    devid: str = "0x78A0"
    haslvp2: str = "0"

@dataclass
class PIC18F25Q24(PS40002414):
    name: str = "18F25Q24"
    devid: str = "0x78C0"

@dataclass
class PIC18F26Q24(PS40002414):
    name: str = "18F26Q24"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x78E0"

@dataclass
class PIC18F45Q24(PS40002414):
    name: str = "18F45Q24"
    devid: str = "0x7940"
    haslvp2: str = "0"

@dataclass
class PIC18F46Q24(PS40002414):
    name: str = "18F46Q24"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7900"

@dataclass
class PIC18F55Q24(PS40002414):
    name: str = "18F55Q24"
    devid: str = "0x7960"

@dataclass
class PIC18F56Q24(PS40002414):
    name: str = "18F56Q24"
    CodeSector_endaddr: str = "0x10000"
    devid: str = "0x7920"


@dataclass
class PS40002500:
    CodeMemTraits_locsize: str = "0x1"
    CodeMemTraits_wordimpl: str = "0x3fff"
    CodeMemTraits_wordinit: str = "0x3fff"
    CodeMemTraits_wordsafe: str = "0x3fff"
    CodeMemTraits_wordsize: str = "0x2"
    CodeSector_beginaddr: str = "0x0"
    CodeSector_endaddr: str = "0x800"
    ConfigFuseMemTraits_locsize: str = "0x2"
    ConfigFuseMemTraits_wordimpl: str = "0x3fff"
    ConfigFuseMemTraits_wordinit: str = "0x3fff"
    ConfigFuseMemTraits_wordsafe: str = "0x3fff"
    ConfigFuseMemTraits_wordsize: str = "0x2"
    ConfigFuseSector_beginaddr: str = "0x8007"
    ConfigFuseSector_endaddr: str = "0x800c"
    DataMemTraits_locsize: str = "0x1"
    DataMemTraits_wordimpl: str = "0xff"
    DataMemTraits_wordinit: str = "0x0"
    DataMemTraits_wordsafe: str = "0xff"
    DataMemTraits_wordsize: str = "0x1"
    DeviceIDMemTraits_locsize: str = "0x1"
    DeviceIDMemTraits_wordimpl: str = "0x3fff"
    DeviceIDMemTraits_wordinit: str = "0x3fff"
    DeviceIDMemTraits_wordsafe: str = "0x3fff"
    DeviceIDMemTraits_wordsize: str = "0x2"
    DeviceIDSector_beginaddr: str = "0x8006"
    DeviceIDSector_endaddr: str = "0x8007"
    Latches_cfg: str = "1"
    Latches_eedata: str = "1"
    Latches_flashdata: str = "1"
    Latches_pgm: str = "32"
    Latches_rowerase: str = "32"
    Latches_userid: str = "1"
    ProgrammingRowSize_cfg: str = "1"
    ProgrammingRowSize_eedata: str = "1"
    ProgrammingRowSize_flashdata: str = "1"
    ProgrammingRowSize_pgm: str = "32"
    ProgrammingRowSize_rowerase: str = "32"
    ProgrammingRowSize_userid: str = "1"
    ProgrammingWaitTime_cfg: str = "5600"
    ProgrammingWaitTime_eedata: str = "5600"
    ProgrammingWaitTime_erase: str = "8400"
    ProgrammingWaitTime_flashdata: str = "5600"
    ProgrammingWaitTime_lverase: str = "2800"
    ProgrammingWaitTime_lvpgm: str = "2800"
    ProgrammingWaitTime_pgm: str = "2800"
    ProgrammingWaitTime_rowerase: str = "2800"
    ProgrammingWaitTime_userid: str = "5600"
    RevisionIDSector_beginaddr: str = "0x8005"
    RevisionIDSector_endaddr: str = "0x8006"
    UserIDMemTraits_locsize: str = "0x1"
    UserIDMemTraits_wordimpl: str = "0x3fff"
    UserIDMemTraits_wordinit: str = "0x3fff"
    UserIDMemTraits_wordsafe: str = "0x3fff"
    UserIDMemTraits_wordsize: str = "0x2"
    UserIDSector_beginaddr: str = "0x8000"
    UserIDSector_endaddr: str = "0x8004"
    VDD_maxdefaultvoltage: str = "5500"
    VDD_maxvoltage: str = "5500"
    VDD_mindefaultvoltage: str = "1800"
    VDD_minvoltage: str = "1800"
    VDD_nominalvoltage: str = "5000"
    VPP_defaultvoltage: str = "8500"
    VPP_maxvoltage: str = "9000"
    VPP_minvoltage: str = "7900"
    Wait_cfg: str = "5600"
    Wait_eedata: str = "5600"
    Wait_erase: str = "8400"
    Wait_flashdata: str = "5600"
    Wait_lverase: str = "2800"
    Wait_lvpgm: str = "2800"
    Wait_pgm: str = "2800"
    Wait_rowerase: str = "2800"
    Wait_userid: str = "5600"
    architecture: str = "16xxxx"
    boundary: str = "8"
    erasealgo: str = "1"
    erasepagesize: str = "2048"
    fuses_impl: str = "39773BE73F7F2B9F0001"
    haschecksum: str = "0"
    hashighvoltagemclr2: str = "1"
    haslvp2: str = "1"
    hasrowerasecmd: str = "1"
    hasvppfirst: str = "1"
    lvpbit_mask: str = "0x2000"
    lvpbit_offset: str = "3"
    lvpthresh: str = "2700"
    mask: str = "0x3FFF"
    memtech: str = "ee"
    pagesize: str = "32"
    pgmspec: str = "40002500"
    tries: str = "1"

@dataclass
class PIC16F13113(PS40002500):
    name: str = "16F13113"
    devid: str = "0x3121"

@dataclass
class PIC16F13114(PS40002500):
    name: str = "16F13114"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x3124"
    erasepagesize: str = "4096"

@dataclass
class PIC16F13115(PS40002500):
    name: str = "16F13115"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3127"
    erasepagesize: str = "8192"

@dataclass
class PIC16F13123(PS40002500):
    name: str = "16F13123"
    devid: str = "0x3122"

@dataclass
class PIC16F13124(PS40002500):
    name: str = "16F13124"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x3125"
    erasepagesize: str = "4096"

@dataclass
class PIC16F13125(PS40002500):
    name: str = "16F13125"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3128"
    erasepagesize: str = "8192"

@dataclass
class PIC16F13143(PS40002500):
    name: str = "16F13143"
    devid: str = "0x3123"

@dataclass
class PIC16F13144(PS40002500):
    name: str = "16F13144"
    CodeSector_endaddr: str = "0x1000"
    devid: str = "0x3126"
    erasepagesize: str = "4096"

@dataclass
class PIC16F13145(PS40002500):
    name: str = "16F13145"
    CodeSector_endaddr: str = "0x2000"
    devid: str = "0x3129"
    erasepagesize: str = "8192"


