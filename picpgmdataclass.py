#!/usr/bin/env python3
"""
Title: Create file with PIC programming attributes

Author: Rob Hamerling, Copyright (c) 2024..2024. All rights reserved.

Description:
     - collect programming attributes from MPLABX XML files as
       selected with the generation of Jallib device files
     - build a class per common Program Specifications
       with all attributes and default value of its PICs
     - build a sub-class per PIC with attributes which
       are unique for this PIC like 'devid', or of which the
       value deviates from the pgmspec class (default) value
"""

user_guide = ("""
# User guide:
# The pic programming information file can be loaded as follows:
#     import picpgmdataclassinfo
# This will import a dataclass for every PIC.
# Obtain access to the attributes of a target PIC with e.g.:
#     target = picpgmdataclassinfo.PIC16F18857
# When pictype is a variable like "16F18857" you may need to specify:
#     target = eval("picpgmdataclassinfo.PIC" + pictype)
# The programming attributes of the target are then available
# by name like for example the device ID with:
#     devid = target.devid
# or with:
#     devid = getattr(target, "devid")

""")

"""
    Notes
        1. For PICs without DeviceID a dummy value 0x0000 is provided
        2. When an attribute has no value (might be an error in MPLABX):
           Latch value will be 1
           Rowsize value will be 1 (word)
        3  Simple Wait attributes will be omitted (currently inactive code!)

"""

import os
import time
import xml.etree.ElementTree as et
from concurrent import futures

# determining Jallib devices base and MPLABX version
try:
    from pic2jal_environment import check_and_set_environment
except:
    print('Required "pic2jal_environment" script missing')
    exit(1)
base, mplabxversion = check_and_set_environment()  # obtain environment variables
if (base == ""):
    exit(1)


def isdecimal(str):
    """ check if <str> contains a (decimal) number """
    if (str is None) or (str == 'none'):
        return False
    try:
        if str.isdigit():
            return True
    except:
        pass
    return False

def isfloat(str):
    """ check if <str> contains a floating point number """
    if (str is None) or (str == 'none'):
        return False
    if str.count('.') == 1:             # check for one and only one decimal point
        try:
            float(str)
            return True
        except:
            pass
    return False

def ishex(str):
    """ check if <str> contains a valid hexadecimal value """
    if (str is None) or (str == 'none'):
        return False
    if str.startswith('0x'):
        try:
            int(str, 16)
            return True
        except:
            pass
    return False

def isnumeric(str):
    """ any form of numeric data """
    return isdecimal(str) or isfloat(str) or ishex(str)

def collect_pgminfo_pic(xmlfile):
    """ collect programming attributes of this PIC,
        return these in a dictionary
    """
    # print(xmlfile)
    with open(xmlfile, 'r') as fp:
        xmlstr = fp.read()                              # complete xml content
        # remove 'edc:' and 'xsi:' namespace prefixes in xml file
        xmlstr = xmlstr.replace('edc:', '')
        xmlstr = xmlstr.replace('xsi:', '')
    root = et.fromstring(xmlstr)                        # parse xml content
    picname = os.path.splitext(os.path.basename(xmlfile))[0][3:].upper()  # isolate pic type
    picpgmdict = {}                                     # pgminfo dictionary for this PIC

    # Header
    if (pgmspec := root.get('psid', '00000000')).startswith('DS'):
        pgmspec = ''.join([c for c in pgmspec if '0' <= c <= '9'])  # remove possible DS- prefix

    # correction or addition of some PgmSpec number specifications
    def altspec(old, new):
        if old != new:
            print(f'{picname:12} PgmSpec {old} replaced by {new}')
        else:
            print(f'{picname:12s} NO REPLACE NEEDED!')
        return new                                      # even when new==old!

    if pgmspec == 'none':
        pgmspec = '00000000'
    if picname.endswith(('F1571', 'F1572')):
        pgmspec = altspec(pgmspec, '40001713')
    elif picname.endswith(('F1612', 'F1613')):
        pgmspec = altspec(pgmspec, '40001720')
    elif picname.endswith(('F1713', 'F1716')):
        pgmspec = altspec(pgmspec, '40001714')
    elif picname.endswith(('F18154', 'F18155', 'F18156', 'F18174', 'F18175', 'F18176')):
        pgmspec = altspec(pgmspec, '40002276')
    elif picname.endswith(('F877')):
        pgmspec = altspec(pgmspec, '30009025')
    elif picname.endswith(('F882', 'F883', 'F884', 'F886', 'F887')):
        pgmspec = altspec(pgmspec, '40001287')
    picpgmdict['pgmspec'] = pgmspec

    # Architecture
    archnode = root.find('ArchDef')                     # select architecture node
    picpgmdict['architecture'] = archnode.get('name')

    # Memory
    memsections = ('CodeMemTraits', 'UserIDMemTraits', 'ConfigFuseMemTraits',
                   'DeviceIDMemTraits', 'EEDataMemTraits', 'DataMemTraits')
    memtraits = archnode.find('MemTraits')
    for memsection in memsections:
        trait = memtraits.find(memsection)
        if not trait is None:                           # trait found
            picpgmdict[memsection] = {}
            for item in trait.items():
                picpgmdict[memsection][item[0]] = item[1]

    # Power
    powernode = root.find('Power')                       # select power node
    for item in powernode.items():
        picpgmdict[item[0]] = item[1]

    powersections = ('VDD', 'VPP')
    for powersection in powersections:
        powerelement = powernode.find(powersection)
        picpgmdict[powersection] = {}
        for name, value in powerelement.items():
            if isnumeric(value):
                picpgmdict[powersection][name] = int(1000 * eval(value))    # millivolts
            else:
                picpgmdict[powersection][name] = value

    # Programming
    pgmnode = root.find('Programming')                  # select programmming node
    for name, value in pgmnode.items():
        if isfloat(value):
            value = f'{int(1000 * eval(value))}'        # (probably) Volts -> milliVolts
        picpgmdict[name] = value

    pgmelements = pgmnode.findall('ProgrammingWaitTime')
    picpgmdict['ProgrammingWaitTime'] = {}
    # note: timeunits are not considered, supposed to be 'us' (microsecond)
    for pgmelement in pgmelements:
        attrib = pgmelement.attrib
        if not (progop := attrib.get('progop')) is None:
            if not (time := attrib.get('time')) is None:
                picpgmdict['ProgrammingWaitTime'][progop] = time
                # print(f'{picname} {progop=} {time=}')
                if not (timeunits := attrib['timeunits']) in ('', 'us'):
                    print(f'{picname:12s}: timeunits <{timeunits}> is not "us"!')

    pgmelements = pgmnode.findall('ProgrammingRowSize')
    picpgmdict['ProgrammingRowSize'] = {}
    for pgmelement in pgmelements:
        attrib = pgmelement.attrib
        if not (progop := attrib.get('progop')) is None:
            if not (size := attrib.get('nzsize')) is None:
                picpgmdict["ProgrammingRowSize"][progop] = 1 if size in ('', None) else size
                sizeunits = attrib.get('sizeunits')
                if not sizeunits in ('', 'bytes', None):
                    print(f'{picname:12s} {progop}: sizeunits <{sizeunits}> not "bytes"!')

    pgmelement = pgmnode.find('Wait')
    picpgmdict['Wait'] = {}
    for name, value in pgmelement.items():
        if not value in ('', 'None'):
            picpgmdict["Wait"][name] = value

    pgmelement = pgmnode.find('Latches')
    picpgmdict['Latches'] = {}
    for name, value in pgmelement.items():
        if not value in ('', 'None'):
            picpgmdict["Latches"][name] = value

    # ProgramSpace
    pgmspace = root.find('ProgramSpace')            # select program space
    pgmsectors = ('CodeSector', 'UserIDSector', 'EEDataSector',
                  'ConfigFuseSector', 'WORMHoleSector',
                  'RevisionIDSector', 'DeviceIDSector')
    picpgmdict['fuses_impl'] = ''                   # init string of impl. config bytes
    for pgmsector in pgmsectors:

        if pgmsector == 'DeviceIDSector':
            picpgmdict['devid'] = '0x0000'          # default (dummy) values
            picpgmdict['mask'] = '0xFFFF'           # default 16-bits wide
            tempdict = picpgmdict.get("CodeMemTraits")
            if tempdict is not None:                # size of word in code memory
                picpgmdict['mask'] = tempdict.get("wordimpl", 0xFFFF)
        pgmelements = pgmspace.findall(pgmsector)
        if len(pgmelements):
            addr_low = 0xFFFFFF                     # start value lower boundary
            addr_high = 0x0                         # start value upper boundary
            for pgmelement in pgmelements:          # all pages
                attr = pgmelement.attrib
                begin = attr.get('beginaddr')
                addr_low = min(addr_low, eval(begin))   # new lower boundary
                end = attr.get('endaddr')
                addr_high = max(addr_high, eval(end))   # new upper boundary
                picpgmdict[pgmsector] = {"beginaddr" : f'0x{addr_low:x}',
                                         "endaddr" : f'0x{addr_high:x}'}
                if pgmsector == 'DeviceIDSector':
                    if not (devid := attr.get('value')) in ('', None):
                        picpgmdict['devid'] = devid
                    if not (mask := attr.get('mask')) in ('', None):
                        picpgmdict['mask'] = mask
                elif pgmsector in ('ConfigFuseSector', 'WORMHoleSector'):
                    dcraddr = addr_low                  # begin of config memory
                    for dcrdef in list(pgmelement):     # (DCRDefs and AdjustPoints)
                        if dcrdef.tag == 'AdjustPoint':
                            offset = eval(dcrdef.get("offset", 1))
                            dcraddr += offset
                            if picname.startswith('18'):
                                picpgmdict['fuses_impl'] += ('00' * offset)
                            else:
                                picpgmdict['fuses_impl'] += ('0000' * offset)
                        elif dcrdef.tag == 'DCRDef':
                            if picname.startswith('18'):
                                picpgmdict['fuses_impl'] += f'{eval(dcrdef.get("impl", "0")):02X}'
                            else:
                                picpgmdict['fuses_impl'] += f'{eval(dcrdef.get("impl", "0")):04X}'
                            if not (dcrmodelist := dcrdef.find('DCRModeList')) is None:
                                for dcrmode in dcrmodelist.findall('DCRMode'):
                                    offset = 0          # bit position start
                                    for dcrfielddef in list(dcrmode):
                                        if dcrfielddef.tag == "AdjustPoint":
                                            offset += eval(dcrfielddef.get("offset", 1))
                                        elif dcrfielddef.tag == "DCRFieldDef":
                                            if (name := dcrfielddef.get("cname").upper()) == 'LVP':
                                                picpgmdict['lvpbit_offset'] = f'{(dcraddr-addr_low):X}'
                                                picpgmdict['lvpbit_mask'] = f'0x{(1 << offset):04X}'
                                                # print(f'{picpgmdict["lvpbit_offset"]} {picpgmdict["lvpbit_mask"]}')
                                                break       # (breaks only inner loop, doesn't care)
                                            offset += eval(dcrfielddef.get("nzwidth", 1))
                            dcraddr +=1
    """
    # check for duplicate attributes: 'ProgrammingWaitTime' <--> 'Wait'
    # this piece of code may be activated later.
    for waitkey in picpgmdict.get("Wait").keys():
        wait = picpgmdict["Wait"].get(waitkey)
        time = picpgmdict["ProgrammingWaitTime"].get(waitkey)
        if not wait == time:
            print(f'{picname} Wait_{waitkey} ({wait}) != ProgrammingWaitTime_{waitkey} ({time})')
    """
    return picname, picpgmdict

def collect_pgminfo(picdir):
    """ create and return a dictionary with programming attributes of all PICs
        {picname : attributes}
        note: the dictionary is collected with parallel processes
              (a process for collecting info of each individual PIC)
    """
    picfiles = os.listdir(picdir)
    with futures.ProcessPoolExecutor() as executor:
        results = executor.map(collect_pgminfo_pic, [os.path.join(picdir, f) for f in picfiles])
    # from results: create dictionary with pgminfo per PIC
    return {r[0] : r[1] for r in results}       # {picname : pgminfo} dictionary

def format_pic_attrs(pic, picpgmdict):
    """ build a new dictionary with formatted {key : value} pairs for one PIC.
        keys with a dictionary of values {'subkey : subvalue, ... }
        will be replaced by a number of {key_subkey : sub_value} items
    """
    keylist = sorted(picpgmdict[pic].keys())
    # simple attributes first, dictionaries later
    fmtdict = {}
    for key in keylist:
        if not type(picpgmdict[pic][key]) == type({}):   # value is not of type dictionary
            value = picpgmdict[pic][key]
            if isnumeric(value):
                fmtvalue = eval(value)                  # convert string to number
                if key in ('devid', 'mask', 'lvpbit_mask'):     # hex string preferred
                    fmtvalue = f'0x{fmtvalue:04X}'      # '0x' + 4 hex digits (no quotes)
            elif value in ('true', 'false'):            # simplify
                fmtvalue = 1 if value=='true' else 0    # True is 1, False = 0
            else:
                fmtvalue = f'{value}'
            fmtdict[f'{key}'] = f'{fmtvalue}'
    # other (non dictionary) attributes
    for key in keylist:
        if type(picpgmdict[pic][key]) == type({}):      # dictionary type
            for sub in picpgmdict[pic][key]:
                value = picpgmdict[pic][key][sub]
                if value in ('true', 'false'):
                   value = 1 if value=='true' else 0    # True is 1, False = 0
                fmtdict[f'{key}_{sub}'] = f'{value}'
    return fmtdict

def build_format_dict(picpgmdict):
    """ build dictionary of formatted programming attrs for all PICs
        { picname : (attrs) }
    """
    # PICs to be excluded:
    excluded_pics = ('18F14K22LIN', )       # (no Programming Specifications?)
    picfmtdict = {}
    for pic in picpgmdict:
        if pic not in excluded_pics:
            picfmtdict[pic] = format_pic_attrs(pic, picpgmdict)
    return picfmtdict

def mostfrequent(attr, piclist, picattr):
    """ select best default value for attribute in pgmspec class
        <attr> attribute (name)
        <piclist> list of PICs (with same pgmspec) with this attribute
        <picfmtdict> dictionary with attributes of all PICs
        return attribute value which occurs most frequently
    """
    countdict = {}                          # dictionary (attrvalue : occurences}
    for pic in piclist:                     # all PICs this PgmSpec
        if not (attrvalue := picattr[pic].get(attr)) is None:
            countdict[attrvalue] = countdict.get(attrvalue, 0) + 1  # occurrences of each value
    maxcount = 0                            # start value highest count
    maxvalue = None                         # corresponding value
    for value, count in countdict.items():  # occurrences of each value
        if count > maxcount:                # higher number of occurences
            maxvalue = value                # new attribute with highest occurrence
            maxcount = count                # new highest number of occurences
    return maxvalue                         # most frequently used value

def invert_dict(picfmtdict):
    # invert {pic : pgmspec) dictionary
    pgmspec2picname = {}                    # inverted dictionary: {pgmspec : [picname,]}
    for picname in picfmtdict:
        pgmspec = picfmtdict[picname]['pgmspec']
        pgmspec2picname.setdefault(pgmspec, []).append(picname)
    return pgmspec2picname

def build_picpgmdataclassinfo(picfmtdict, picpgmdataclassfile):
    """ build picpgmdataclassinfo.py with a class for each PgmSpec
        and a subclass for each of the PICs in this PgmSpec
        - collect all <key:value> pairs of all PICs of this PgmSpec
        - create a class for this PgmSpec with all these attributes
          (excluding 'devid')
        - create a class for every PIC of this PgmSpec with
          'devid' and with <key:value> pairs of which the value
          differs from the value specified with the PgmSpec.
        - Since the pgmspec class specifies all keywords of all
          its PICs there might be occasions that a keyword
          in the pgmspec class does not apply to a specific PIC!
    """
    pgmspec2picname = {}                        # inverted dictionary: {pgmspec : [picname,]}
    maxwait = {}                                # dictionary with max wait-times
    for picname in picfmtdict:
        pgmspec = picfmtdict[picname]['pgmspec']
        pgmspec2picname.setdefault(pgmspec, []).append(picname)

    with open(picpgmdataclassfile, 'w') as fp:
        fp.write('#\n'
                 '# PIC programming attributes\n'
                 '#  - all attribute values are presented as string\n'
                 '#  - times are in microseconds\n'
                 '#  - voltages are in millivolts\n\n'
                f'# Data collected from MPLABX v{mplabxversion} dd {time.asctime()}\n')
        fp.write(user_guide)                                        # for text: see top of script
        fp.write('from dataclasses import dataclass\n\n')
        for pgmspec in sorted(pgmspec2picname.keys()):              # per pgmspec
            # collect attributes of all PICs in this PgmSpec
            attrdict = {}                                           # {attr : [list of PICs]}
            for pic in sorted(pgmspec2picname[pgmspec]):
                for attr in picfmtdict[pic]:
                    attrdict.setdefault(attr, []).append(pic)       # list ofPICs with this attribute
            # create class for the PgmSpec with keywords common to all PICs of the PgmSpec
            fp.write(f'@dataclass\n')                               # decorator
            fp.write(f'class PS{pgmspec}:\n')                       # start class for PgmSpec
            pgmspecdefault = {}                                     # {key:value} defaults
            for attr in sorted(list(attrdict.keys())):              # all keywords this PgmSpec
                if attr == 'devid':                                 # devid only in pic class
                    continue
                val = mostfrequent(attr, attrdict[attr], picfmtdict)  # select most frequently used value
                fp.write(f'    {attr}: str = "{val}"\n')            # add to class specification
                pgmspecdefault[attr] = val                          # to dict with pgmspec settings
            fp.write('\n')
            # create class for every PIC of this PgmSpec
            for pic in sorted(pgmspec2picname[pgmspec]):            # all PICs of this pgmspec
                fp.write(f'@dataclass\n')                           # decorator
                fp.write(f'class PIC{pic}(PS{pgmspec}):\n')         # inherit pgmspec class
                fp.write(f'    name: str = "{pic}"\n')              # specify PIC type
                if not 'devid' in attrdict:                         # no devid in attributes
                    fp.write(f'    {devid}: str = "0x0000"\n')      # add dummy
                for attr in sorted(list(attrdict.keys())):
                    if attr == 'devid':                             # devid: always
                        fp.write(f'    {attr}: str = "{picfmtdict[pic].get(attr,0)}"\n')
                    else:                                           # other: only when different value
                        if (val := picfmtdict[pic].get(attr,0)) != pgmspecdefault[attr]:    # unequal
                            fp.write(f'    {attr}: str = "{picfmtdict[pic].get(attr,0)}"\n')
                    if attr.startswith('Wait_'):
                        if (wait := int(val)) != 65535:             # exclude MPLABX faults
                            if attr in maxwait:
                                if wait > maxwait[attr][0]:
                                    maxwait[attr] = (wait, pic)
                            else:
                                maxwait[attr] = (wait, pic)
                fp.write('\n')
            fp.write('\n')
    print('Maximum wait values:')
    for w in maxwait:
        print(f'{w} = {maxwait[w]}')
    return len(picfmtdict)


# ================ mainline =======================

if __name__ == "__main__":
    print(f'Collecting attributes for PIC programmer ...')

    # pathspec of pic programming info file to be created (or replaced)
    picpgmdataclassfile = os.path.join('./', 'picpgmdataclassinfo.py')
    picpgmcompactfile = os.path.join('./', 'picpgmcompactinfo.py')

    # locate .PIC files extracted from MPLABX IDE (for Jallib!)
    picdir = os.path.join(base, 'mplabx')       # with all platforms!
    if not os.path.exists(picdir):              # directory with XML files not found
        print(f'Could not find {picdir}')
        exit(1)
    if not len(os.listdir(picdir)):             # only xml files expected
        print(f'No files found in {picdir}')
        exit(1)

    # collect 'raw' programming attributes from MPLABX xml files
    picpgmdict = collect_pgminfo(picdir)        # collect programming attributes
    if not len(picpgmdict):
        exit(1)

    # build a dictionary with single {key : value} pairs
    # of which 'key' may be a combined key_subkey name
    picfmtdict = build_format_dict(picpgmdict)

    print(f'Building file with programming attributes: {picpgmdataclassfile} ...')
    if (pic_count := build_picpgmdataclassinfo(picfmtdict, picpgmdataclassfile)):
        print(f'Generated {picpgmdataclassfile} with {pic_count} PICs')
    else:
        print(f'Failed to create {picpgmdataclassfile}')
        exit(1)


#
